<?php
/**
 * Created by PhpStorm.
 * User: laxman
 * Date: 8/28/19
 * Time: 11:33 AM
 */

/**
 * https://codex.wordpress.org/Customizing_the_Login_Form
 */
add_action( 'login_enqueue_scripts', function(){
    wp_enqueue_style( 'custom-login', plugin_dir_url(__FILE__) . '/assets/admin/login.css' );
} );

add_filter( 'login_headerurl', function(){
    return site_url();
} );