<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Edox
 * @since Edox 1.0
 */


$footerPostId = my_icl_object_id(50, 'page');

$footerPost = get_posts('page_id='.$footerPostId.'&post_per_page=1&post_type=page&suppress_filters=0');
$post = $footerPost[0];
setup_postdata($post);

?>
<!-- special -->
<?php if(ICL_LANGUAGE_CODE !='fr'):?>
<div class="special">
<div class="wrapper">
  <div class="row">
    <?php
	//$id = (ICL_LANGUAGE_CODE == 'en') ? '50' : '478';
	//$id=50;
  	//$query = new wp_query('page_id='.my_icl_object_id(50, 'page'));
	//while ($query->have_posts()):$query->the_post();
	//foreach( $footerPost as $post ): setup_postdata($post);	?>
    <div class="col-sm-3"><img src="<?php the_field('picto_1'); ?>"> <?php the_field('picto_text_1'); ?></div>
    <div class="col-sm-3"><img src="<?php the_field('picto_2'); ?>"> <?php the_field('picto_text_2'); ?></div>
    <div class="col-sm-3 text-center"><img src="<?php the_field('picto_3') ?>"> <?php the_field('picto_text_3'); ?></div>
    <div class="col-sm-3 text-center"><img src="<?php the_field('picto_4') ?>"> <?php the_field('picto_text_4'); ?></div>
    <?php //endforeach;?>
  </div>
</div>
</div>
<?php endif;?>
<!-- special -->
<!-- footer -->
<div class="footer">
<div class="wrapper">
  <div class="row">
    <?php
		
		$blockAItems = get_field('block_a_page_listing');
		if(!empty($blockAItems)):
			$blockAID = lang_object_ids($blockAItems,'page');
		 ?>
		    <div class="col-sm-3"><h5><?php the_field('block_a_title');?></h5>
		     <ul class="list-unstyled">
		    <?php 	 foreach( $blockAID as $postItem): // variable must be called $post (IMPORTANT) ?>
		  
		        <li><a href="<?=get_permalink($postItem); ?>"><?=get_the_title($postItem);?></a></li>
		  	<?php  endforeach; ?>
		    </ul>
		    </div>
	<?php endif; //endforeach;?>
    <div class="col-sm-3">
		<?php
		/*
            if(ICL_LANGUAGE_CODE == "fr") {
                $collectionTitle = "Collection";
            } else if(ICL_LANGUAGE_CODE == "es") {
                $collectionTitle = "COLECCIÓN";
            } else if(ICL_LANGUAGE_CODE == "bs") {
                $collectionTitle = "KOLEKCIJA";	
            } else if(ICL_LANGUAGE_CODE == "zh-hant") {
                $collectionTitle = "採集";	
            } else {
                $collectionTitle = "Collection";
            }
            <h5><?php echo $collectionTitle; ?></h5>
            */
        ?>
    	<h5><?php the_field('collection_list_title');?></h5>
    <ul class="list-unstyled" id="footer-coll"></ul>
    
     <script>
     jQuery('#mega-menu .collection .sub-menu:first > li > a').each(function(index){
  		if(index < 3){
  			var $list = jQuery('<li />').appendTo(jQuery('#footer-coll'));
  			jQuery(this).clone().appendTo($list);
  		}
  	});
	</script>
    </div>
    
	<?php 
		
		$blockBItems = get_field('block_b_page_listing');
	//foreach( $footerPost as $post ): setup_postdata($post);
		if(!empty($blockBItems)):
			$blockBID = lang_object_ids($blockBItems,'page');

		?>
		    <div class="col-sm-3"><h5><?php the_field('block_b_title');?></h5>
		     <ul class="list-unstyled">
		    <?php 	$count=1; foreach( $blockBID as $postItem): // variable must be called $post (IMPORTANT) ?>
		       <?php //if($count==4) break;?>
		        <li><a href="<?=get_permalink($postItem); ?>"><?=get_the_title($postItem);?></a></li>
		    <?php //$count++;
			endforeach; ?>
		    </ul>
		    </div>
	<?php endif; //endforeach;?>
	
	<div class="col-sm-3">
	<?php //foreach( $footerPost as $post ): setup_postdata($post);	?>
      <h5><?php the_field('home_contact_title');?></h5>
     <?php the_field('home_contact_details');?>
    <?php //endforeach;?>
    </div>
	
  </div>
  

  <div class="row">
    <div id="footer-map" class="col-sm-6 footer-map">
    	<h4><a href="#" data-toggle="modal" data-target="#languageModal"><?php the_field('map_title', $footerPost->ID);?></a></h4>
    </div>  




    <?php if(!isset($_GET['action'])): ?>
    	<!-- popup -->
    	<div class="modal fade pop-map" id="languageModal" tabindex="-1" role="dialog">
    		<div class="modal-dialog">
    		<div class="modal-content">
    		<h4><?php the_field('country_lang_title', $footerPost->ID);?> </h4>
    		<div class="row map-section">
                <div role="tabpanel">                       
                         <div class="col-sm-6 list-small" id="selcountry">
                            <!-- Nav tabs -->
                            <ul class="list-unstyled" role="tablist">
                                <?php $countryListTitle = get_field('country_list_title',$footerPost->ID);
                                    if(!empty($countryListTitle)):
                                ?>
								    <li><strong><?php echo $countryListTitle; ?></strong></li>
                                <?php endif; ?>
                                <?php
                                global $currCountryCode;
                                /*$currCountryCode = kd_detect_country();*/


                                $countryList = get_field('country_and_language',$footerPost->ID);
                                $countryCount = 1;
                                //$currCountryCode = 'AT';
                                $isLocationAustria = ($currCountryCode == 'AT');
                                foreach ($countryList as $countryName):
                               // $active = ($countryCount==1)?'active':'';
                                    $showCountryItem = true;
                                    if(!$isLocationAustria && $countryName['country'] == 'Austria'){
                                        $showCountryItem = false;
                                    }
                                    if($showCountryItem):
                                ?>
                                <li>
                                    <a href="javascript:void(0)" onclick="jQuery('.tab-pane.active').removeClass('active');jQuery('#tab-content-<?=$countryCount?>').addClass('active');jQuery('div#selcountry ul li').removeClass('active');jQuery(this).parent('li').addClass('active');">
                                        <?=$countryName['country']?></a>
                                </li>
                                <?php
                                    endif;
                                    $countryCount++;

                                endforeach; ?>
                            </ul>
                        </div>

                       
                        <div class="col-sm-6 list-big">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <?php
                                $countryIP = get_client_ip();
                                $langCount=1;
                                foreach ($countryList as $countryName):
                                    $countryLanguage = $countryName['add_language_and_code'];
                                    $showCountryItem = true;
                                    if(!$isLocationAustria && $countryName['country'] == 'Austria'){
                                        $showCountryItem = false;
                                    }
                                    if($showCountryItem):
                                    ?>

                                        <div role="tabpanel" class="tab-pane" id="tab-content-<?=$langCount?>">
                                            <ul class="list-unstyled" role="tablist">
                                                <li class="firstc"><strong>Language</strong></li>
                                                <?php foreach ($countryLanguage as $countryLang): ?>
                                                    <li class="lastc"><a href="<?=site_url('/')?><?=($countryLang['langauge_code']=='en')? '' : $countryLang['langauge_code']?>"><?=$countryLang['language']?></a></li>
                                                <?php endforeach;?>
                                            </ul>
                                        </div>


                                  <?php
                                    endif;
                                    $langCount++;
                                endforeach;
                                ?>
                            </div>
                        </div>

                </div>
    		</div>
    		</div>
    		</div>
    	</div>
    	<!-- popup -->
	<?php endif; ?>

      <script>
          $(".col-sm-6 .list-unstyled li:eq(1)").addClass("active");
          $(".tab-content .tab-pane").first().addClass("active");
      </script>

	
    <div class="col-sm-3 point-de"><h5><?php the_field('block_point_title', $footerPostId)?></h5>
    <ul class="list-unstyled">
      <li><a href="<?php the_field('block_d_page_1_link', $footerPostId)?>"><?php the_field('block_d_page_1', $footerPostId);?></a></li>
      <li><a href="<?php the_field('block_d_page_2_link', $footerPostId)?>"><?php the_field('block_d_page_2', $footerPostId
	  );?></a></li>      
    </ul>
    
    
    </div>
    
    <div class="col-sm-3 forms">
		<div class="input-group">
			<div id="newsletter">
            	<div id="nl-mess"></div>
                    <form method="post" id="form_mailchimp">
	                    <input type="email" name='email' placeholder="<?php _e('Newsletter') ?>" class="form-control emailid">
	                    <span class="input-group-btn">
                        <input type="hidden" name='lang' value='<?=ICL_LANGUAGE_CODE?>' class="btn btn-default">
	                    <input type="submit" name='submit' value="OK" class="btn btn-default">
	                    </span>
                    </form>
                </div>
            </div>
		<div class="button-click">
			<a class="btn btn-default demande" href="<?php the_field('catalogue_request_link', $footerPostId);?>"><?php the_field('catalogue_request_title', $footerPostId);?></a>
			<a class="btn btn-default" href="<?php the_field('general_information_link', $footerPostId);?>"><?php the_field('general_information_title', $footerPostId);?></a>
		</div>
		</div>
        <div class="col-sm-3 footer-social" style="text-align:left">
     <a href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819" target="_blank"><i class="fa fa-facebook"></i></a>
    <a href="https://twitter.com/edoxwatches"><i class="fa fa-twitter" target="_blank"></i></a> 
    <a href="https://instagram.com/edoxswisswatches" target="_blank"><i class="fa fa-instagram"></i></a> 
    <a href="https://www.youtube.com/user/EdoxWatches"><i class="fa fa-youtube" target="_blank"></i></a> 
    <a href="https://www.pinterest.com/edoxwatches/" target="_blank"><i class="fa fa-pinterest-square"></i></a> 
   
    </div>
    </div>
  </div>
 <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
			 	<a href="" class="page-scroll" >
					<button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
				</a>
			</div>
</div>

<!-- footer -->
<!-- copyright -->
<div class="text-center copyright"><?php _e("Copyright")?> <?php echo date("Y");?> <?php the_field('copyright_information',$footerPost->ID);?> <span>/</span>Website by <a target="_blank" href="https://www.procab.ch">Procabstudio</a></div>

<script>
(function($){
	var ajaxURL = "<?=home_url('/');?>wp-admin/admin-ajax.php";
	//MailChimp error and success message displayed
	jQuery('#form_mailchimp').submit(function(e) {
			e.preventDefault();
			var data = jQuery(this).serialize();
			//$("#newsletter input[type='submit']").addClass('submitteed');
			jQuery.ajax({
				url: ajaxURL,
				type: 'POST',
				data: data+"&action=mailchimp_subscribe",
				success: function(result) {
					//console.log(result.error, result.success,result);
					if(typeof result.error !== 'undefined'){
						$("#newsletter #nl-mess").attr('class','error').text(result.error);
						//$('#newsletter input[type="email"]').attr('class','error');
						return;
					}
					if(typeof result.success !== 'undefined'){
						$("#newsletter #nl-mess").attr('class','success').text(result.success);
						 $('#newsletter input[type="email"]').val('');
						// $('#newsletter input[type="email"]').removeClass('error');
						return;
					}				
				},
				dataType:"json"
			});
		});
		 /* ========================================================================
     back to top
   ========================================================================== */

 $(document).ready(function () {
      $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
              $('.back-to-top').fadeIn();
          } else {
              $('.back-to-top').fadeOut();
          }
      });
      $('.back-to-top').click(function () {
          $("html, body").animate({
              scrollTop: 0
          }, 600);
          return false;
      });
  });

})(jQuery);
</script>

<?php wp_reset_postdata(); ?>
<!-- copyright -->
</div>