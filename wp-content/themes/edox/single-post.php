<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage EDOX
 * @since EDOX 1.0
 */
get_header();
?>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55b078a961e25d4d" async="async"></script>

<?php
/*$cache = get_procab_file_cache();
$cacheSingleNewsKey = $cache->buildKey('singlenews');
$cacheSingleNewsData = $cache->restore($cacheSingleNewsKey);
if($cacheSingleNewsData): echo $cacheSingleNewsData;
else:
    $cache->captureStart($cacheSingleNewsKey);*/
?>

<div class="main-wrapper">
  <div class="">
  

    <div class="newdetailpage">
      <?php while (have_posts()): the_post(); ?>
      <div class="header-section bg-blue">
        <div class="container news-wrapper cms-content">
          <?php the_title('<h1>','</h1>'); ?>
        </div>
      </div>
      <div id="banner"> <img src="<?php the_field('banner_image'); ?>" alt="<?php the_title();  ?>" class="img-responsive"></div>
          <?php if(post_password_required() ) { ?>
                        <div id="col-text" class="news-wrapper cms-content text-center">
                            <?php the_content(); ?>
                        </div>
                        <?php
                    } 
      if(!post_password_required()) { ?>
    	  <div id="col-text" class="news-wrapper cms-content">
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
    		<div class="addthis_native_toolbox"></div>
    		
    			<div class="blockspace"><?php the_field('column_text'); ?></div>
    </div>
          <div id="career-achievements">
            <?php 
        		
        		/*
        		// loop through the rows of data
        		while ( have_rows('repeater_field_name') ) : the_row();
        		
        		// display a sub field value
        		the_sub_field('sub_field_name');
        		
        		endwhile;
        		*/
        			
        		?>
          </div>
          <div id="galley" class="news-wrapper  cms-content newsPage-gallery gallery">
          <div class="blockspace">
            <h2><?php the_field('gallery_title')?></h2>
            <?php 
        		
        		$images = get_field('images_for_gallery',$post->ID);
        		
        		if( $images ): ?>
            <ul>
              <?php foreach( $images as $image ): ?>
              <li class='gallery-item '>
    		  <span class='gallery-icon'><a href="<?php echo $image['url']; ?>" rel='gallery'>
    			<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" />
    		  </a>
    		  </span>
    		  </li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            </div>
          </div>
      <?php //For video
    		
    	$videoLink = get_field('home_video_link');
    	if(!empty($videoLink)):
    	?>
	      <div class="video" id="video">
	        <h2><?php _e('The Film'); ?></h2>
			
			<?php $videoImage = get_field("background_image");
					if(!empty($videoImage)):
			?>
	        <img src="<?php echo $videoImage; ?>" class="img-responsive">
			<?php endif; ?>
	        <div class="overlay">
	          <div class="caption">
	            <div class="caption-text">
	              <h3 class="wow animated fadeInDown">
	                <?php the_field('home_video_title');?>
	              </h3>
	              <h2 class="wow animated fadeInDown">
	                <?php the_field('home_video_subtitle');?>
	              </h2>
	              <p class="wow animated fadeInDown">
	                <?php the_field('home_video_play_button_title');?>
	              </p>
	              <a href="#video" data-video-href="<?php the_field('home_video_link');?>" onclick="loadVideo('<?php the_field('home_video_link');?>')" class="play wow animated fadeInDown animated scroll"><i class="fa fa-play"></i></a> </div>
	          </div>
	        </div>
	        <!-- video popup -->
	        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	          <div class="modal-dialog">
	            <button type="button" class="close-btn pull-right" data-dismiss="modal" aria-label="Close"><img src="<?php echo get_template_directory_uri();?>/images/cross.png"></button>
	            <div class="embed-responsive embed-responsive-16by9">
	              <iframe class="embed-responsive-item" src="<?php echo $videoLink; ?>" frameborder="0" allowfullscreen></iframe>
	            </div>
	          </div>
	        </div>
	        <!-- video popup -->
	        <div class="closeVideo" id="ytplayerClose" onclick="closeVideo();" style="display:none">Close Video</div>
	        <div id="ytplayer"></div>
	      </div>
      <?php endif;?>
      
      <?php } endwhile;?>
      
      <div class="pull-right blockspace btn-wrap">
      		<a href="<?php echo site_url('/news');?>" class="btn-cms"><?php _e("Back to News"); ?></a>
          <?php if(post_password_required() ) { ?>
              <a href="http://eepurl.com/gxrqgz" class="btn-cms" style=""><?php _e("Subscribe to Newsletter"); ?></a>
          <?php } ?>
	   </div>
	   
    </div>
  </div>
</div>
<?php
 /*   echo $cache->captureEnd($cacheSingleNewsKey);
endif;*/
?>
<script src="<?php echo get_template_directory_uri();  ?>/js/yt_video.js"></script>
<?php get_footer();?>