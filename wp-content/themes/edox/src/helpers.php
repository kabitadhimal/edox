<?php

namespace App;

// //https://github.com/bradvin/social-share-urls
use Intervention\Image\ImageManager;
use Procab\Media\Manager;
use Procab\Wp\Wrapper;

function isRequestAjax()
{
    return Wrapper::$isRequestAjax;
}




function get_main_template(){
    return Wrapper::$mainTemplate;
}


const IMAGE_SIZE_NEWS_THUMB= 'news-thumb';
const IMAGE_SIZE_PARTNER_THUMB= 'partner-thumb';



/**
 * @return Manager
 */
function getImageManager(){
    static $imageManager;
    if(!$imageManager) {
        $manager = new ImageManager(['driver' => 'gd']);
        $config = [
            'sizes' => [

                IMAGE_SIZE_NEWS_THUMB => [589, 288, 'fit'],  //crops the image //~[1596, 600, 'fit']
                IMAGE_SIZE_PARTNER_THUMB => [500, 500, 'fit'],

                /*  IMAGE_SIZE_THUMB => [370, 370, 'fit'],
                  TEAM_IMAGE_LARGE => [1170, 549, 'fit'],
                  TEAM_IMAGE_THUMB => [230, 250, 'fit'],



                  IMAGE_SIZE_NEWS_THUMBNAIL => [761, 798, 'fit'],
                  IMAGE_SIZE_NEWS_lARGE => [1314, 1374, 'fit'],
                  IMAGE_SIZE_MEDIUM_RESIZE => [568, 364, 'resize'],
                  IMAGE_SIZE_LARGE => [1169, 612, 'resize'],
                  IMAGE_SIZE_CMS_HEADER => [1749,465,'fit'],
                //  IMAGE_SIZE_BANNER_CIRCLE => [1920,569, 'fit'],
                 // IMAGE_SIZE_TEXT_BANNER => [1903,637, 'fit'],
                 // IMAGE_SIZE_PLAIN_BANNER => [1920,590, 'fit'],
                  IMAGE_SIZE_BANNER => [1920,1080, 'fit'],
                  IMAGE_SIZE_HERO_MEDIUM => [583, 369, 'fit'],*/
            ],
            'save_folder' => ABSPATH.'cache/images',
            'save_url' => site_url(). '/cache/images'
        ];

        $imageManager = new Manager($manager, $config);
    }
    return $imageManager;
}

/**
 * get the directory path of the image
 * @param $imageUri
 * @return string
 * todo check if the image belongs to current website
 */
function getImageDirectoryPath($imageUri){
    static $uploadDir;
    if(!$uploadDir) $uploadDir = wp_get_upload_dir();
    $imageUri = str_replace($uploadDir['baseurl'], '', $imageUri);
    return $uploadDir['basedir'].$imageUri;
}

function is_content_empty($str) {
    return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}
function is_post_content_empty(\WP_Post $post) {
    return is_content_empty($post->post_content);
}

if ( ! function_exists( 'storefront_cart_link' ) ) {
    /**
     * Cart Link
     * Displayed a link to the cart including the number of items present and the cart total
     *
     * @return void
     * @since  1.0.0
     */
    function app_cart_link()
    {
        ?>
        <a href="<?php echo esc_url(wc_get_cart_url()); ?>"><span class="topbar-icon"><i class="fa fa-shopping-basket"
                                                                                         aria-hidden="true"></i></span>
            <span class="amount"><?php echo wp_kses_data(WC()->cart->get_cart_subtotal()); ?></span> <span
                class="count"><?php echo wp_kses_data(sprintf(_n('Cart ( %d ) ', ' Cart ( %d )', WC()->cart->get_cart_contents_count(), 'app'), WC()->cart->get_cart_contents_count())); ?></span></a>

        <?php
    }
}

function showCategoryImage($cat)
{
    //if($cat->taxonomy == 'brand')
    //$cat = get_queried_object();
    $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
    $image = wp_get_attachment_url($thumbnail_id);

    if ($image) $image = \App\getImageManager()->resize(\App\getImageDirectoryPath($image), \App\IMAGE_SIZE_PRODUCT_CAT);
    //if brand we may not need to crop the image
    $str = '';
    if ($image) {
        $str = <<<EOF
    <div class="col-xs-12 product-item product-item--bkg js-last-col">
        <a href="">
            <figure class="product-item__cover" style="background-image:url({$image})">
            </figure>
        </a>
    </div>
EOF;

    }
    return $str;
}