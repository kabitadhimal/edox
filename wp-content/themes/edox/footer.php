<?php include 'includes/footer_content.php'; ?>
<!-- boostrap -->
<script src="<?php echo get_template_directory_uri();  ?>/assets/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
<!-- wow script -->
<script src="<?php echo get_template_directory_uri();  ?>/assets/wow/wow.min.js"  type="text/javascript"></script>
<!-- jquery mobile -->
<script src="<?php echo get_template_directory_uri();  ?>/assets/mobile/touchSwipe.min.js"  type="text/javascript"></script>
<!-- kwicks -->
<script  type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/kwicks/jquery.kwicks.js?ver=1.3.4"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/swipe/swipe.js?ver=1.3.4"></script>
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri();  ?>/assets/font-awesome-4.3.0/css/font-awesome.min.css?ver=1.3.4">


<?php wp_footer(); ?>
<script>

        var swiper = new Swiper('.swiper-container-news', {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
            },
            speed: 1000,
            autoplay:true,
        });
	<?php    
	//global $currentLocation;
	global $currCountryCode;
    if($currCountryCode == 'US'):     ?>
    	jQuery.ajax({
		  method: "GET",
		  url: "<?php echo site_url();?>/shop/index.php/en/categoryattribute/index/getCartQty",
		  success:function(resp){
			//console.log(resp);
			$(".cart-icon-link").append("<bold>"+resp+"</bold>");
			}
		});
    <?php endif; ?>
    </script>
</body>
</html>

