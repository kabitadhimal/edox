<?php

get_header(); ?>

     <section id="content-wrapper">
			<div class="content">
	<div class="inner-page-content cms-page-content page-404">
		
		<header class="page_header">
        	<div class="ico"></div>
			<h2 class="page-title"><?php _e("OOPS! THAT PAGE CAN'T BE FOUND.")?></h2>
        </header>
		<div class="page-wrapper">
			<div class="page-content">
            					<p><?php _e("It appears the page you are looking for doesn't exist any more or might have been moved.")?></p>				
            </div><!-- .page-content -->
		</div><!-- .page-wrapper -->
		     
		</div>
      <!---inner page content end--->
      	</div>
	</section>
  
  	<?php get_footer(); ?>
 


