/**
 * Created by procab on 12/14/17.
 */
//https://developers.google.com/youtube/iframe_api_reference
function onYouTubeIframeAPIReady() {
    homepageVdo.onYouTubeIframeAPIReady();
}
var homepageVdo = new function(){
    this._videoId = null;
    this._isScriptLoading = false;
    this._isScriptLoaded = false;
    this._videoContainer = 'player';
    this._videoOverlay = '#player-overlay';
    this._player = null;
    this._soundCtrl = null;

    this.loadVideoById = function(videoId, vdoContainer, vdoOverlay){
        this._videoId = videoId;
        this._videoContainer = vdoContainer;
        this._videoOverlay = vdoOverlay;
        if(this._isScriptLoaded){
            this.onPlayerReady();
        }else{
            this._loadScript();
        }
    };



    this.onYouTubeIframeAPIReady = function(){
        this._isScriptLoaded = true;
        var playerDefaults = {autoplay: 1,
            autohide: 1,
            modestbranding: 1,
            rel: 0,
            showinfo: 0,
            controls: 0,
            disablekb: 1,
            enablejsapi: 0,
            iv_load_policy: 3,
            wmode: 'transparent',
            branding: 0,
            vq: 'hd1080'

        };
        this._player = new YT.Player(this._videoContainer, {
            height: '100%',
            width: '100%',
            //videoId: 'M7lc1UVf-VE',
            playerVars: playerDefaults,
            events: {
                'onReady': this.onPlayerReady.bind(this),
                'onStateChange': this.onPlayerStateChange.bind(this)
            }
        });
    }

    this.onPlayerStateChange = function(el){
        console.log(el.data)
        if(el.data === 1) {
            if(this._videoOverlay) {
                window.bsCarouselNode.carousel('pause');
                jQuery(this._videoOverlay).fadeOut(400, function () {
                    jQuery(this._videoOverlay).remove();
                    this._videoOverlay = null;
                    console.log("this._videoOverlay",this._videoOverlay)
                }.bind(this));
            }
        }
        if (el.data === YT.PlayerState.ENDED) {
            console.log('loop ivdeo');
            //this.loadVideoById(this._videoId, this._videoContainer, this._videoOverlay);
            //this.onPlayerReady();
            //this._player.seekTo(0);
            window.bsCarouselNode.carousel('cycle');
            window.bsCarouselNode.carousel(1);
        }
    };

    this._loadScript = function(){
        if(this._isScriptLoading) return false;
        //This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";

        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        //This function creates an <iframe> (and YouTube player)
        // after the API code downloads.
    };


    this.onPlayerReady = function(event) {
        this._isScriptLoaded = true;
        //alert('play vdo');
        this._player.mute();
        this._player.loadVideoById({
            'videoId': this._videoId,
            //'startSeconds': 515,
            autoplay:1//,
            //'endSeconds': 690,
            //'suggestedQuality': 'hd720'
        });

        //event
        this._soundCtrl = document.querySelector('.js-sound');
        this._soundCtrl.addEventListener('click', function(event){
            event.preventDefault();
            console.log(this)
            if(this._player.isMuted()){
                this.unMuteVideo();
            }else{
                this.muteVideo();
            }
        }.bind(this));
        //event.target.playVideo();

        this._fullscreen = document.querySelector('#enable-fulscreen');

        this._fullscreen.addEventListener('click', function(){
            launchIntoFullscreen(document.getElementById('player'))
        }.bind(this));

        // document.getElementById('enable-fulscreen').addEventListener('click', function() {
        //         launchIntoFullscreen(document.getElementById('player'))
        // });

    };

    this.muteVideo = function(){
        if(!this._player) return;
        this._soundCtrl.classList.add('sound-ctrl--muted');
        this._player.mute();
    };

    this.unMuteVideo = function(){
        if(!this._player) return;
        this._soundCtrl.classList.remove('sound-ctrl--muted');
        this._player.unMute();
    };


    function launchIntoFullscreen(element) {
      if(element.requestFullscreen) {
        element.requestFullscreen();
      } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
      } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
      }
    }



};