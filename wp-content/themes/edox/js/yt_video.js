;var isiOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
function getYoutubeVideoId(url){
	var videoId = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
	if(videoId != null) {
		videoId = videoId[1];
		videoId = videoId.replace('embed/','');
		return videoId;
	} else { 
	   return false;		
	}
}

var loadVideo = function(){
	return true;	
}

if(!isiOS){
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      var player;
	  var isPlayerReady = false;
	  var currentVideoId = null;
	  
	  jQuery('#ytplayer').show();
	  jQuery('#ytplayerClose').hide();
	  
	 
	  //jQuery('#ytplayer').css({ 'left':'-9999px' });
	  jQuery('#ytplayer').hide();
	  
	  function onYouTubeIframeAPIReady() {
		  player = new YT.Player('ytplayer', {
          height: '390',
          width: '640',
		 // videoId: currentVideoId,
		  playerVars: {
            //controls: 0,
          //  autoplay: 1,
            disablekb: 1,
            enablejsapi: 1,
            iv_load_policy: 3,
            modestbranding: 1,
            showinfo: 0
        },
          events: {
			  'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
	  }
	  
	  function onPlayerReady(){
		isPlayerReady = true;
		  if(currentVideoId != null){
			  player.cueVideoById(currentVideoId);	
			  player.playVideo();
		}
	  }
	  
	  loadVideo = function(url){
		var videoId = getYoutubeVideoId(url);
		if(videoId == null) return;
		
		currentVideoId = videoId;
		jQuery('#ytplayerClose').show();
		jQuery('#ytplayer').show();
		
		if(!isPlayerReady) return;

		player.cueVideoById(videoId);	
		player.playVideo()
	  }
	  
	  function closeVideo(){
		jQuery('#ytplayerClose').hide();
		  jQuery('#ytplayer').hide();
		
		if(!isPlayerReady) return;
		player.stopVideo();
	  }
	  
	  function onPlayerStateChange(event) {
	  }
}else{
	$('a[data-video-href]').each(function(index, element) {
		var videoId = getYoutubeVideoId($(this).attr('data-video-href'));
		if(videoId != null) {
			$(this).attr('href', 'https://www.youtube.com/watch?v='+videoId+'');   
		}
        
    });
}

