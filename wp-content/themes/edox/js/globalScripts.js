;(function($){
    
    //$('li:nth-child(8)').addClass('us-enlang');


	// mobile menu freezescroll

	$('#menu-menu>li>a').on('hover', function(e){
		e.preventDefault();
		$('body').toggleClass('freezeScroll');
	})


	$('.meanmenu-reveal').on('click', function(e){
		e.preventDefault();
		$('body').toggleClass('freezeScroll');
		// $('.menu').toggleClass('menu-mobile-sm');
	})

	var subMenuBtns = document.querySelectorAll('.menu .dropdown-submenu');
	if(subMenuBtns.length){
		for(i=0; i < subMenuBtns.length; i++){
			subMenuBtns[i].addEventListener('click', function(e){
				// e.preventDefault();
				//get sub menu
				//  console.log(this.querySelector('ul'))
				this.querySelector('ul').classList.toggle('menu-mobile');
				this.classList.toggle('menu-icon');
			}.bind(subMenuBtns[i]));
		}
	}
    
	var isHomePage = true;
	var $body = $('body');
	var $mainNavWrapper = $('.main-nav-wrapper');
	if($body.attr('id')=='inside') isHomePage = false;
	var $menuOverlay = $('#main-nav-overlay');
	
	//menu overlay effect
	var nInterval = -1;
	$('#menu-menu > li:has(ul)').each(function(index, element) {
		$(this).mouseenter(function(e) {
	        $menuOverlay.show();    
			toggleMenuOverlay(true);
        });
		$(this).mouseleave(function(e) {
			$menuOverlay.removeClass('active');
			toggleMenuOverlay(false);
        });        
    });
	$menuOverlay.click(function(){	toggleMenuOverlay(false);	});
	function toggleMenuOverlay(open){
		if(nInterval>-1) clearInterval(nInterval);
		if(open==true){
			nInterval = setTimeout(function(){ $menuOverlay.addClass('active');	}, 50);
		}else{
			nInterval = setTimeout(function(){ $menuOverlay.hide();}, 400);			
		}
	}
	
	//collection menu
	var $subMenus = $('#menu-menu .collection .sub-menu:first > li');
	var $collectionWrapper = jQuery('.collection .sub-menu:first');
	var $previewPlaceHolder = $('<li id="collectionPreview"><div><img src=""></div></li>').appendTo($('#menu-menu .collection .sub-menu:first')).css({'opacity':0,'z-index':-1});
	var $preview = $previewPlaceHolder.find('img');
	$('#menu-menu .collection .sub-menu .sub-menu').each(function(index, element) {
		var $self = $(this);
		$self.mouseenter(function(e) {
			if(index==0){
				showPreview(1,2);
				$previewPlaceHolder.removeClass('preview-left');				
				$previewPlaceHolder.css('left', '25%');
			}
			if(index==1){
				showPreview(2,3);
				$previewPlaceHolder.removeClass('preview-left');				
				$previewPlaceHolder.css('left', '50%');
			}
			if(index>=2){
				showPreview(1,0);
				$previewPlaceHolder.addClass('preview-left');
				$previewPlaceHolder.css('left', '0%');
			}
		});
		
		$self.mouseleave(function(e) {reset(); });
		
		//anchor event
		$self.find('li a').each(function(index, element) {
			var $anchor = $(this);
			$anchor.mouseenter(function(e) {
				var previewSrc = $anchor.data('img');

				$previewPlaceHolder.css({'opacity':0, 'margin-top':'10px'});
				$preview.attr('src',previewSrc);
				
				$previewPlaceHolder.find('div').css('height', $collectionWrapper.height())
				$previewPlaceHolder.stop(true, true).animate({'opacity':1, 'margin-top' :0}, 300);
				$previewPlaceHolder.css('z-index', 'initial');
			});
		});
	});
	
	function showPreview(id1, id2){
		$($subMenus[id1]).stop(true, true).animate({'opacity':0}, 200);
		$($subMenus[id2]).stop(true, true).animate({'opacity':0}, 200);
	}
	
	function reset(){
		$subMenus.stop(true, true).animate({'opacity':1}, 200);
		$previewPlaceHolder.stop(true, true).animate({'opacity':0, 'margin-top' :'10px'}, 200);
		setTimeout(function(){
			$previewPlaceHolder.css('z-index', -1);
		}, 300);
	}
	//end of collection menu
			
	//navigation open/close
	var $mainNavigation = $('.main-nav');
	$('.main-nav .line-three').click(function(){
		openMenu(true);
	});
	
	$( ".main-nav .close-btn" ).click(function() {
	   openMenu(false);
	});
	
	function openMenu(open){
		if(open == true){
			$mainNavigation.addClass('active');	
		}else{
			$mainNavigation.removeClass('active');
		}
	}
	
	function showStickyMenu(open){
		if(isHomePage == true){
			if(open == true){
				if(!$mainNavWrapper.hasClass('main-nav-fixed')) $mainNavWrapper.addClass('main-nav-fixed');	
			}else{
				if($mainNavWrapper.hasClass('main-nav-fixed')) $mainNavWrapper.removeClass('main-nav-fixed');	
			}
			openMenu(open);
		}else{
			if(open == true){
				if($mainNavWrapper.hasClass('main-nav-inner')) $mainNavWrapper.removeClass('main-nav-inner');	
			}else{
				if(!$mainNavWrapper.hasClass('main-nav-inner')) $mainNavWrapper.addClass('main-nav-inner');	
			}
		}
		
	}
	
	
	//sildebar menu btn
	$('.main-nav-wrapper .side-nav-btn').click(function(){
		$('.main-nav-wrapper .side-nav').toggleClass('active');
	});


	function resizer(){
		if($body.scrollTop() > 100){
			showStickyMenu(true);
		}else{
			showStickyMenu(false);
		}
	}
	$(window).resize(function(e) {resizer();});
	$(window).scroll(function(e) {resizer();});
	resizer();

	$('#phone-menu-content').meanmenu({
		meanScreenWidth:960,
		//removeElements:'.wp-post-image, .btn-menu, .sub-menu li .menu-span, .menu-item-description, #collectionPreview, .sub-teaser',
		meanMenuContainer:'#phone-menu'
	});


	$(window).scroll(function(){
		if ($(window).scrollTop() >= 300) {
			$('header').addClass('header-up');
		}
		else {
			$('header').removeClass('header-up');
		}
	});
		
	//});
})(jQuery);


$(document).ready(function () {
    var swiper = new Swiper('.swiper-container', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        speed: 1000,
        autoplay:true,
    });
})