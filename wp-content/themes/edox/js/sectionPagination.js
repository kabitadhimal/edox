var sectionPagination = function( settings ){
	var THIS 		= $(this);
	
	THIS.settings = {
		sections				: '#content_wrapper > section',
		
		paginContainer			: "#sectionPagination",
		activePaginBG			: "#sectionPagination .hPaginCont .activePaginBG",
		paginDataArray			: ['Bata’s Story Begins in 1894','Success Through Innovation','The Modern Era','Looking Forward'],
		
		paginFadeEffect			: true,
		bgSlideEffect			: true,
		
		paginPosOffset			: 0
	};
	
	for (i in settings) THIS.settings[i] = settings[i];
	
	var $win = $(window);
	var sectionPos = [];
	var pages = [];
	var totalPages = 0;
	var currentSectionId = 0;
	var isSectionMovementBusy = false;
	var counter = 0;
	var $paginContr	= $(THIS.settings.paginContainer);
	var $paginEffBlk = $(THIS.settings.activePaginBG);
	var isPaginMsOver = false;
	var ua = navigator.userAgent;
	var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
	
	var scrollHandler = function(event){
		var tempCurrId = currentSectionId;
		for(var x =0; x<totalPages; x++){
			var $page = $(pages[x]);
			var yPos = -$win.scrollTop() + $page.offset().top;
			//if(x == 0)console.log(yPos);
			
			var offsetCoords = $page.offset(), topOffset = offsetCoords.top;
			if(($win.scrollTop() + $win.height()) > (topOffset) && (topOffset + $page.height() > $win.scrollTop())){
				
				//var perc = (x==0)?($win.scrollTop() / ($page.offset().top+1)):($win.scrollTop() / $page.offset().top);	
				//log($win.scrollTop()  + " : " + sectionPos[1])
				if(isSectionMovementBusy == false){
					if(x==0){
						if($win.scrollTop() < sectionPos[1] * 0.3){
							//log('first page');
							currentSectionId = 0;
						}
					}else{
						var perc = ($win.scrollTop() - sectionPos[x-1])/(sectionPos[x] - sectionPos[x-1]);
						if(perc > 0.56 && perc < 1.58){
							//log('show page ' + x);
							currentSectionId = x;
						}
						//log(x + " : " + perc)
					}
				}		
			}
		}
		if(tempCurrId!=currentSectionId)updatePaginAlpha();
	}
	
	var scrollToCurrent = function(){
		isSectionMovementBusy = true;
		window.clickSectionUpdate = true;
		window.ignoreSectionUpdate = true;
					
		var yPos = $(pages[currentSectionId]).offset().top;
		//console.log('ishomepage:'+$(".home-page").size());
		if($(".home-page").size() > 0){
			yPos = $(pages[currentSectionId]).offset().top + $(pages[currentSectionId]).height()/2 - $(window).height()/2;
			yPos = (yPos < 200) ? 0 : yPos;
			if(currentSectionId == 1) yPos -=40;
			else if(currentSectionId == 2) yPos -=60;
			else if(currentSectionId == 3) yPos +=30;
			//else if(currentSectionId == 4) yPos +=20;
			else if(currentSectionId == 5) yPos -=20;
		}
		$('body,html').stop(true, false).animate({'scrollTop':yPos+'px'},1000, 'easeInOutExpo', function(){
			isSectionMovementBusy = false;
			window.clickSectionUpdate = true;
			if(window.onSectionUpdate)window.onSectionUpdate();
			window.ignoreSectionUpdate = false;
			window.clickSectionUpdate = false;
		});
		updatePaginAlpha();
		
	}
	
	var updatePaginAlpha = function(){
		var $pagins = $($paginContr.find("ul > li"));
		
		$pagins.each(function(i){
			//log((currentSectionId-i));
			var $obj = $($pagins[i]); //console.log($obj);
			if(THIS.settings.paginFadeEffect){
				$obj.data('alpha',(100-Math.abs(currentSectionId-i)*30)/100);
				if(!isPaginMsOver)$obj.stop(false,true).animate({'opacity':$obj.data('alpha')},400);
			}
			
			if(i == currentSectionId)$obj.addClass('active');
			else{
				if($obj.hasClass('active'))$obj.removeClass('active');
			}
		});
		
		var paginSpacing = 20;
		if($(window).width() <= 1024)paginSpacing = 35;
		if(THIS.settings.bgSlideEffect)$paginEffBlk.stop(false,true).animate({"top":currentSectionId*paginSpacing},400);
	}
	
	THIS.init = function(context){
		var setup = function(){
			pages = $(context.settings.sections).each(function(index, element) {
                $(this).data('index',index);
				sectionPos.push(parseInt($(this).offset().top));
            });
				
			totalPages = pages.length;
			$currentPage = $(pages[0]);
			$win.bind('scroll', scrollHandler);
			//document.addEventListener("touchmove", scrollHandler, false);
			
			// Page Pagination
			var $paginUL = $($paginContr.find("ul"));
			$paginUL.empty();
			for(var i = 0; i < totalPages; i++){
				var $li = $("<li><a href='#'/></li>").attr('class',i+"_btn").appendTo($paginUL);
				if(context.settings.paginDataArray[i]){
					var $txt = $("<span/>").appendTo($li);
					$txt.text(context.settings.paginDataArray[i]);
				}
				$li.click(function(e){
					e.preventDefault();
					currentSectionId = $(this).index();
					scrollToCurrent();
				});
				if(!isiPad){
					$li.mouseenter(function(){
						$(this).find('span').fadeIn();
					});
					$li.mouseleave(function(){
						$(this).find('span').hide();
					});
				}
			}
			if(!isiPad && context.settings.paginFadeEffect){
				$paginContr.mouseenter(function(){
					isPaginMsOver = true;
					$($paginContr.find('li')).stop(false,true).animate({'opacity':1},400);
				});
				$paginContr.mouseleave(function(){
					isPaginMsOver = false;
					$($paginContr.find('li')).each(function(i){
						$(this).stop(false,true).animate({'opacity':$(this).data('alpha')},400);
					});
				});
			}
			
			$($paginContr.find('.hPaginCont')).css('margin-top',-$paginContr.height()/2 + context.settings.paginPosOffset);
			
			updatePaginAlpha();
			
		}
		
		
		setup();
		return context;
	}(THIS)
}
