;
	//credit to view-source:http://andrew.hedges.name/experiments/haversine/
	var Rm = 3961; // mean radius of the earth (miles) at 39 degrees from the equator
	var Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator

	// convert degrees to radians
	function deg2rad(deg) {
		rad = deg * Math.PI/180; // radians = degrees * pi/180
		return rad;
	}
	// round to the nearest 1/1000
	function round(x) {
		return Math.round( x * 1000) / 1000;
	}
	/**
     * 
     * t1, n1, t2, n2	//lat1, long1, ...
     */
	function findDistance(t1, n1, t2, n2) {
		/*
		var t1, n1, t2, n2, lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;
		
		// get values for lat1, lon1, lat2, and lon2
		t1 = frm.lat1.value;
		n1 = frm.lon1.value;
		t2 = frm.lat2.value;
		n2 = frm.lon2.value;
		*/
		
		var lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;
		
		// convert coordinates to radians
		lat1 = deg2rad(t1);
		lon1 = deg2rad(n1);
		lat2 = deg2rad(t2);
		lon2 = deg2rad(n2);
		
		// find the differences between the coordinates
		dlat = lat2 - lat1;
		dlon = lon2 - lon1;
		
		// here's the heavy lifting
		a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
		c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians
		//dm = c * Rm; // great circle distance in miles
		dk = c * Rk; // great circle distance in km
		
		// round the results down to the nearest 1/1000
		//mi = round(dm);
		km = round(dk);
		
		return km;
	}



(function($){
	var icon_path = themeUrl+'/images/cmarker.png';
	var isCountryFound = false;
	var $cityContainer = $('#cities');
	var $storesContainer = $('.store-address');
	var nFilterCallbckPointer = null;
	var isBusy = false;
	var $searchInput = $('#city-search-input');
	
	var $noResultDiv = $('#filter-error');

	//google map
	var myOptions = {
			zoom: 12, scrollwheel: false,
			center: new google.maps.LatLng(46.206923,6.153717),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [{"featureType":"all","elementType":"all","stylers":[{"saturation":-100},{"gamma":0.5}]}]
	}
	
	var map = new google.maps.Map(document.getElementById("city_map_address"), myOptions);

	var mcOptions = {styles: [{
                            height: 53,
                            url: icon_path,
                            width: 53,
							textColor:'white'
                            },
                            {
                            height: 53,
                            url: icon_path,
                            width: 53,
							textColor:'white'
                            },
                            {
                            height: 53,
                            url: icon_path,
                            width: 53,
							textColor:'white'
                            },
                            {
                            height: 53,
                            url: icon_path,
                            width: 53,
							textColor:'white'
                            },
                            {
                            height: 53,
                            url: icon_path,
                            width: 53,
							textColor:'white'
                            }]}
							
	var mapCluster = new MarkerClusterer(map, null, mcOptions);	
	var infowindow = new google.maps.InfoWindow();
	var currentMarkers = [];
	
	function loadMarkers(){
		clearMarkers();
		var bounds  = new google.maps.LatLngBounds();
		$('.filter input[type=checkbox]:checked').each(function(index, element) {
			var categoryId = $(this).val();
			var markerType = $(this).data('index');
			console.log(markerType);
			//console.log($('.store-address [data-category='+categoryId+'] .sm-block').length + " : " + categoryId);
			
			$('.store-address [data-category='+categoryId+'] .sm-block').each(function(index, element) {
                var $self = $(this);
				//console.log(markerType)
				var marker = createMarker($self, markerType, index);
				if(marker !== null ) bounds.extend(marker.position);
				
            });
	    });
		
		if(currentMarkers.length > 0){
			mapCluster.addMarkers(currentMarkers);
		}
		
		
		
		if(!bounds.isEmpty()){
			map.fitBounds(bounds);
			map.panToBounds(bounds);
		}
	}
	
	function clearMarkers(){
		for(var i in currentMarkers){
			google.maps.event.clearInstanceListeners(currentMarkers[i]);
			currentMarkers[i].setMap(null);
			currentMarkers[i] = null;
		}
		currentMarkers = [];
		mapCluster.clearMarkers();
	}
	
	//function createMarker(latlng, type, title, markerType, zindex){
	function createMarker($dom, markerType, zindex){
		var latlng = $dom.data('latlng');
		latlng = latlng.split(',');
		
		if(latlng.length == 1) return null;

		var title = $dom.find('h4').text();
		var markerIcon = themeUrl+'/images/map-marker/marker_' + markerType + '.png';
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(latlng[0], latlng[1]),
			//map: map,
			icon: markerIcon,
			//'icon': icon_path,
			title: title,
			zIndex: zindex
		});
		
		//click event
		google.maps.event.addListener(marker, 'click', function () {
			var __content = '<h1>'+title+'</h1>' + $dom.find('.saddress').html();
			infowindow.setContent(__content);
	        infowindow.open(map, this);
			//console.log(this.getPosition())
			if(map.getZoom() < 10)	map.setZoom(10);
			map.panTo(this.getPosition());
		});
		
		currentMarkers.push(marker);
		return marker;
	}
	
	
	
	function scrollToMap(){
		$('html, body').animate({ scrollTop: $("#city_map_address").position().top}, 600);
	}
	
	window.showMarker = function(target){
		//$('.filter input[type=checkbox]').removeAttr('checked');
		$('.filter input[type=checkbox]').prop('checked', false);//.change();
		var $self = $(target);
		var type = $($self.parent().parent().parent()).data('index');
		console.log("type is "+ type );
		
		clearMarkers();
		var marker = createMarker($self, type, 1);
		marker.setMap(map);
		google.maps.event.trigger(marker, 'click');
		
		scrollToMap();
		//map.setZoom(10);
		//map.panTo(marker.getPosition());
	}
	
	//filters + ajax requests
	//http://192.168.0.213/edox/wp-admin/admin-ajax.php?action=sale_country&city=a
	$('#countries').change(function(e) {  getContentByCountry($(this).val()); $(this).blur();});
	$cityContainer.change(function(e) { getContentByCity($(this).val()); $(this).blur();});
	
	function getContentByCountry(countryId){
		if(isBusy) return false;
		showLoader();
		//if(countryId == -1) return;
		var url = window.ajaxURL;
		var data = {
			locationId:countryId,
			action:'sale_country',
			lang:currentLang
		};
		$.ajax({
			url:url,
			data:data,
			success: function(result){
				$cityContainer.html($(result).filter('#ajx-cities').html());
				$storesContainer.html($(result).filter('#ajx-stores-list').html());
				updateMap();
			}
		});
	}
	
	function getContentByCity(cityID) {
		if(cityID == -1) return;			
		if(isBusy) return false;
		showLoader();
		
		
		var url = window.ajaxURL;
		var data = {
			locationId : cityID,
			action : 'sale_city',
			lang:currentLang
			
		};
		$.ajax({
			url:url,
			data:data,
			success: function(result){
				$storesContainer.html($(result).filter('#ajx-stores-list').html());
				updateMap();
			}
		});
	}
	
	function searchCity(val) {
		if(val == '') return;
		if(isBusy) return false;
		showLoader()
		
		var url = window.ajaxURL;
		var data = {
			city : val,
			action : 'sale_search',
			lang:currentLang
		};
		$.ajax({
			url:url,
			data:data,
			success: function(result){
				$storesContainer.html($(result).filter('#ajx-stores-list').html());
				updateMap();
			}
		});
	}
	
	
	function showLoader(){
		isBusy = true; 
		$noResultDiv.slideUp(300);
		$('body').addClass('search-progress');
		//$('.filter input[type=checkbox], .search-fields select').attr('disabled','disabled');
	}
	
	function updateMap(){
		clearMarkers();
		$('.filter input[type=checkbox]').each(function(index, element) {
			var categoryId = $(this).val();
			//console.log('categoryId ' + categoryId);
			if($('.store-address [data-category='+categoryId+']').length == 0){
				//$(this).attr('disabled','disabled');
				$(this).parent().parent().parent().hide();
				
				$(this).prop('checked', false).change();
				//$(this).removeAttr('checked');
				//console.log('disable');
			}else{
				$(this).parent().parent().parent().show();
				//$(this).removeAttr('disabled');
				//$(this).attr('checked','checked').change();
				$(this).prop('checked', true).change();
				//console.log('enable');
			}
	    });
		//remove overlay
		isBusy = false;
		$('body').removeClass('search-progress');
		
		if($storesContainer.find('.sm-block').length == 0){
			$noResultDiv.slideDown(300);
		}
		//scrollToMap();
	}
	
	
	
	//style inputs
	$('input[type="checkbox"], input[type="file"], input[type="radio"]').each(function(){
		var $this = $(this),
			$class_suffix = $(this).attr('type'),
			$wrap = $('<div class=""></div>');
		
		//Add Class 
		if($this.attr('type') == 'radio'){
			$wrap.addClass('wrap_' + $class_suffix);
		}
		else if($this.attr('type') == 'checkbox'){
			$wrap.addClass('wrap_' + $class_suffix);
		}else{
			$wrap.addClass('wrap_' + $class_suffix);
		}
		
		$this.wrap($wrap);
		$this.after('<i></i>');
	})
	
	//event for checkbox filter
	$('.filter input[type=checkbox]').change(function(e) {
		if(nFilterCallbckPointer != null) clearTimeout(nFilterCallbckPointer);
		nFilterCallbckPointer = setTimeout(function(){
			loadMarkers();
		}, 50);
	}).parent().parent().parent().hide();	
	
	
	//enter event
	$searchInput.keypress(function(event) {
        if (event.keyCode == 13) {
            searchCity($searchInput.val());
        }
    });
	
	
	//free search
	$('#filter_search').one('click', function(){
		$('#city-search').slideDown(300);
		$searchInput.focus();
		$(this).click(function(e) {
            searchCity($searchInput.val());
        });
	})
	
	//search nearest country
	/*
	if(visitorCountry != ''){
		$('#countries option').each(function(index, element) {
			if($(this).text().toLowerCase() == visitorCountry){
				$('#countries').val($(this).val()).change();
				isCountryFound = true;
				return false;
			}
		});
	}
	
	if(isCountryFound==false) getContentByCountry(-1);
	*/
	
	//compare country name on the dropdown
	function searchCountryByName(visitorCountry){
		var countryId = false;
		$('#countries option').each(function(index, element) {
			if($(this).text().toLowerCase() == visitorCountry.toLowerCase()){
				countryId = $(this).val();
			}
		});
		return countryId;
	}
	
	function getNearestCountry(vlat, vlong){
		var vDist = -1;
		var countryVal = false;
		$('#countries option').each(function(index, element) {
			var latlong = $(this).attr('data-lat-long')+"";
			if(latlong != "" && latlong != 'undefined'){
				latlong = latlong.split(',');
				var dist = findDistance(vlat, vlong, latlong[0], latlong[1]);
				//console.log(index + " : " + vDist + " : " + dist + " : " + $(this).text())				
				if(vDist == -1){
					vDist = dist;
					countryVal = $(this).val();
					//console.log('this is a first time, so set to anything that comes first ' + 'new val ' + vDist);					
				}else if(dist < vDist){
					vDist = dist;
					countryVal = $(this).val();	
					//console.log('new val ' + vDist);										
				}//else{	console.log('doenot need to change' + vDist);}
			}
		});
		//console.log(vDist)
		return countryVal;
	}
	
	if(visitorCountry != ''){
		var country = searchCountryByName(visitorCountry);
		if(country == false){
			country = getNearestCountry(visitorCountryLat, visitorCountryLong);
		}
		
		if(country){
			$('#countries').val(country).change();
		}else{
			getContentByCountry(-1);
		}
	}else{
		getContentByCountry(-1);
	}
	

})(jQuery);