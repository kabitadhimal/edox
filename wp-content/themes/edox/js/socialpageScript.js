(function($){
	var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
	if(!isiPad && $(window).width() > 768) var parall = new parallax();
	
	
	var $sBtns = $('.social_btns > li');
	$sBtns.mouseenter(function(){
		var currentIndex = $(this).index();
		$sBtns.each(function(index){
			if(currentIndex != index)$(this).stop(true,false).animate({opacity:0.4},600);
			else $(this).stop(true,false).animate({opacity:1},600);
		});
	});
	$sBtns.mouseleave(function(){
		$sBtns.stop(true,false).animate({opacity:1},600);
	});
	
	
	//** Section Pagination **
	var sectionPagin = new sectionPagination({
		sections: ".social #content_wrapper > section",
		paginDataArray: ["Follow us on Facebook","Join us on Pinterest","Our latest Tweets","Discover #batashoes"]
	});
	
	
	// *** Social Feeds ***
	// Facebook
	$('#facebook_feed_blk').dcSocialStream({
		feeds: {
			facebook: {
				id: '141549179270798',
				out: 'intro,thumb,text,user,share',
				image_width: 3,
				images: "large"
			}
		},
		control: false,
		filter: false,
		wall: false,
		cache: false,
		rotate: false,
		max: 20,
		limit: 4,
		iconPath: window.themeUrl + '/images/dcsns-dark/',
		imagePath: window.themeUrl + '/images/dcsns-dark/'
	});
	
	var fbIntv = setInterval(function(){
		if($("#facebook_feed_blk li.dcsns-facebook ").size() > 0){	
			clearInterval(fbIntv);
			fbIntv = null;
			
			var fadeInDelay = 0;
			$("#facebook_feed_blk li.dcsns-facebook ").each(function(index, element) {
				var $blk = $(this);
				$($(this).find('a')).attr('target','_blank');
				var $img = $(this).find('.section-text img')
				//var imgUrl = $img.attr('src');
				
				var linkStr = $img.parent().attr('href');
				linkStr = linkStr.split('/?')[0].split('/')
				linkStr = linkStr[linkStr.length-1];
				linkStr = 'http://graph.facebook.com/'+linkStr+'/picture?type=normal';
				//$img.attr('data-src',imgUrl);
				//imgUrl = imgUrl.replace("p130x130","p480x480");
				$img.attr('src',linkStr);
				//console.log(index + " ---- " + linkStr);
				
				/*if(index==0)$(this).attr({'data-type':'sprite','data-speed':0.4});
				else if(index==1)$(this).attr({'data-type':'sprite','data-speed':0.6});
				else if(index==2)$(this).attr({'data-type':'sprite','data-speed':0.8});
				else if(index==3)$(this).attr({'data-type':'sprite','data-speed':1});*/
				
				$(this).css('visibility','visible');
				setTimeout(function(){
					$blk.animate({'opacity':1},400);
				},fadeInDelay);
				
				fadeInDelay += 200;
			});
			
			if(parall)parall.scroll();
		}
	},100);
	
	//Pinterest
	$('#pinterest_bataheritage_blk').dcSocialStream({
		feeds: {
			pinterest: {
				id: 'batashoes/bata-heritage',// batashoes/bata-street-style',
				//out: "intro,thumb,title,text,user,share",
				out: "intro,thumb,text,user",
				icon: 'social_bata_small.gif'
			}
		},
		control: false,
		filter: false,
		wall: false,
		cache: false,
		rotate: false,
		max: 20,
		limit: 3,
		iconPath: window.themeUrl + '/images/',
		imagePath: window.themeUrl + '/images/dcsns-dark/'
		
	});
	
	$('#pinterest_batastyle_blk').dcSocialStream({
		feeds: {
			pinterest: {
				id: 'batashoes/bata-street-style',
				//out: "intro,thumb,title,text,user,share",
				out: "intro,thumb,text,user",
				icon: 'social_bata_small.gif'
			}
		},
		control: false,
		filter: false,
		wall: false,
		cache: false,
		rotate: false,
		max: 20,
		limit: 4,
		iconPath: window.themeUrl + '/images/',
		imagePath: window.themeUrl + '/images/dcsns-dark/'
		
	});
	
	var pinterestIntv = setInterval(function(){
		if($("#pinterest_bataheritage_blk li.dcsns-pinterest").size() > 0){	
			clearInterval(pinterestIntv);
			pinterestIntv = null;			
			var $feedColmnUl = $("<ul class='feed-ul' />").appendTo($("#pinterest_bataheritage_blk .dcsns-content"));
			var $fstColmn = $("<li class='first_colmn' data-type='sprite' data-speed='0.4' data-y='158'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $secColmn = $("<li class='second_colmn' data-type='sprite' data-speed='0.02' data-y='0'><ul class='stream' /></li>").appendTo($feedColmnUl);
			
			var fadeInDelay = 0;
			
			$("#pinterest_bataheritage_blk li.dcsns-pinterest").each(function(index, element) {
				var $blk = $(this);
				customizePinBlk($blk);
				if(index==0)$(this).appendTo($fstColmn.find('ul.stream'));
				else if(index == 1 || index == 2)$(this).appendTo($secColmn.find('ul.stream'));
				
				$(this).css('visibility','visible');
				setTimeout(function(){
					$blk.animate({'opacity':1},400);
				},fadeInDelay);
				
				fadeInDelay += 200;			
			});
			
			if(parall)parall.scroll();
		}
	},100);
	
	var pinterest2Intv = setInterval(function(){
		if($("#pinterest_batastyle_blk li.dcsns-pinterest").size() > 0){	
			clearInterval(pinterest2Intv);
			pinterest2Intv = null;			
			var $feedColmnUl = $("<ul class='feed-ul' />").appendTo($("#pinterest_batastyle_blk .dcsns-content"));
			var $thrdColmn = $("<li class='third_colmn' data-type='sprite' data-speed='0.3' data-y='0'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $lastColmn = $("<li class='last_colmn' data-type='sprite' data-speed='0.2' data-y='0'><ul class='stream' /></li>").appendTo($feedColmnUl);
			
			var fadeInDelay = 0;
			
			$("#pinterest_batastyle_blk li.dcsns-pinterest").each(function(index, element) {
				var $blk = $(this);
				customizePinBlk($blk);	
				if(index==0 || index == 1)$(this).appendTo($thrdColmn.find('ul.stream'));
				else if(index == 2 || index == 3)$(this).appendTo($lastColmn.find('ul.stream'));
				
				$(this).css('visibility','visible');
				setTimeout(function(){
					$blk.animate({'opacity':1},400);
				},fadeInDelay);
				
				fadeInDelay += 200;			
			});
			
			if(parall)parall.scroll();
		}
	},100);
	
	function customizePinBlk($blk){
		var pinitUrl = $blk.find('.section-thumb > a').attr('href');
		var imgUrl = $blk.find('.section-thumb img').attr('src');
		var desc = $blk.find('.section-text').text();
		var $pinitBtn = $("<a class='pinit_btn' />").attr({'href':'http://www.pinterest.com/pin/create/button/?url='+encodeURIComponent('http://bata.com')+'&media='+encodeURIComponent(imgUrl)+'&description='+encodeURI(desc)+'','data-pin-do':'buttonPin',"data-pin-config":'above'}).appendTo($blk.find('.section-thumb'));
		
		$pinitBtn.bind('click',function(e){
			e.preventDefault();
			window.open($(this).attr('href'), '_blank', 'location=yes,height=302,width=750,scrollbars=no,status=no');
		});
		var $sectionIntro = $blk.find('.section-intro').empty();
		var sectionUser = $blk.find('.section-user > a').text();
		sectionUser = sectionUser.split('/');
		var board = sectionUser[1];
		sectionUser = sectionUser[0]; //console.log(board, sectionUser);
		var boardURL = $blk.find('.section-user > a').attr('href');
		var $boardLink = $("<a class='board_link' />").attr('href',boardURL).appendTo($sectionIntro);
		var $user = $("<span class='user' />").text(sectionUser).appendTo($boardLink);
		var $board = $("<span class='board' />").text(board).appendTo($boardLink);
		
		var $img = $blk.find('.section-thumb img')
		var imgUrl = $img.attr('src');
		imgUrl = imgUrl.replace("192x","400x");
		$img.attr('src',imgUrl);
		$($blk.find('a')).attr('target','_blank');
	}
	
	//Twitter
	$('#twitter_feed_blk').dcSocialStream({
		feeds: {
			twitter: {
				id: '/515378891,batashoes',//,'/16548023,#9gag,9gag',
				url: window.themeUrl+'/twitter.php',
				icon: 'twitter.png',
				images: 'medium',
				out: 'intro,thumb,date,text,image'//'intro,thumb,date,text,user,share'
			}
		},
		twitterId: 'batashoes',
		control: false,
		filter: false,
		wall: false,
		cache: false,
		rotate: false,
		max: 30,
		limit: 10,
		iconPath: window.themeUrl + '/images/',
		imagePath: window.themeUrl + '/images/dcsns-dark/'
		
	});	
	
	var twitterIntv = setInterval(function(){
		if($("#twitter_feed_blk .dcsns-twitter").size() > 0){
			clearInterval(twitterIntv);
			twitterIntv = null;
			
			var $feedColmnUl = $("<ul class='feed-ul' />").appendTo($("#twitter_feed_blk .dcsns-content"));
			var $fstColmn = $("<li class='first_colmn' data-type='sprite' data-speed='0.2'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $secColmn = $("<li class='second_colmn' data-type='sprite' data-speed='0.6'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $lastColmn = $("<li class='last_colmn' data-type='sprite' data-speed='0.4'><ul class='stream' /></li>").appendTo($feedColmnUl);
			
			var fadeInDelay = 0;
			
			$("#twitter_feed_blk li.dcsns-twitter").each(function(index, element) {
				var $blk = $(this);
				var fTime = $(this).find('.section-intro span a').text().replace(" ago",'');
				fTime = " · " + fTime;
				var $fTime = $("<span class='feed-time' />").text(fTime).appendTo($(this).find('.twitter-user'));
				$(this).find('.section-text img').parents('a').addClass('image-link');
				
				$(this).find('.twitter-user').insertAfter($(this).find('.section-thumb'));
				
				if(index==0)$(this).appendTo($fstColmn.find('ul.stream'));
				else if(index == 1)$(this).appendTo($secColmn.find('ul.stream'));
				else if(index == 2)$(this).appendTo($lastColmn.find('ul.stream'));
				else if(index == 3){
					var $parent = $(this).parent();// console.log('parent',$parent);
					$(this).appendTo($lastColmn.find('ul.stream'));
					$parent.remove();
				}else{
					$(this).hide();
					$(this).remove();
				}
				$(this).css('visibility','visible');
				setTimeout(function(){
					$blk.animate({'opacity':1},400);
				},fadeInDelay);
				
				fadeInDelay += 200;
				$($(this).find('a')).attr('target','_blank');
			});
			
			if(parall)parall.scroll();
		}
	},50);
	
	//Instagram
	$('#instagram_feed_blk').dcSocialStream({
		feeds: {
			instagram: {
				id: '#batashoes', //,'675939920'
				intro: 'Posted',
				search: 'Search',
				out: 'thumb,text,user',//'intro,thumb,text,user,share,meta',
				accessToken: '610350922.718f421.41949eb6edeb4a5fa9116ded51f1be83',
				redirectUrl: 'http://localhost/SlicingProjs/Procab/Bata/html/social.html',
				clientId: '718f42181a1e43719aa8681282633723',
				thumb: 'standard_resolution',
				comments: 3,
				likes: 8,
				icon: 'social_bata_small.gif'
			}
		},
		control: false,
		filter: false,
		wall: false,
		cache: false,
		rotate: false,
		max: 20,
		limit: 7,
		iconPath: window.themeUrl + '/images/',
		imagePath: window.themeUrl + '/images/dcsns-dark/'
		
	});	
	
	
	var instagramIntv = setInterval(function(){
		if($("#instagram_feed_blk .dcsns-instagram").size() > 0){
			clearInterval(instagramIntv);
			instagramIntv = null;
			var fadeInDelay = 0;
			
			var $feedColmnUl = $("<ul class='feed-ul' />").appendTo($("#instagram_feed_blk .dcsns-content"));
			var $fstColmn = $("<li class='first_colmn' data-type='sprite' data-speed='0.2'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $secColmn = $("<li class='second_colmn' data-type='sprite' data-speed='0.4'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $thrdColmn = $("<li class='third_colmn' data-type='sprite' data-speed='0.6'><ul class='stream' /></li>").appendTo($feedColmnUl);
			var $lastColmn = $("<li class='last_colmn' data-type='sprite' data-speed='0.3'><ul class='stream' /></li>").appendTo($feedColmnUl);
			
			$("#instagram_feed_blk li.dcsns-instagram").each(function(index, element) {
				var $blk = $(this);
				if(index==0 || index==1)$(this).appendTo($fstColmn.find('ul.stream'));
				else if(index == 2 || index == 3)$(this).appendTo($secColmn.find('ul.stream'));
				else if(index == 4){
					$(this).appendTo($thrdColmn.find('ul.stream'));
					var $instagramLogo = $("<li class='instagram-logo' />").appendTo($thrdColmn.find('ul.stream'));
				}else if(index == 5 || index == 6)$(this).appendTo($lastColmn.find('ul.stream'));
				
				$($(this).find('.section-thumb a')).attr('href',$($(this).find('.section-user > a')).attr('href'));
				
				$($(this).find('a')).attr('target','_blank');
				
				$(this).css('visibility','visible');
				setTimeout(function(){
					$blk.animate({'opacity':1},400);
				},fadeInDelay);
				
				fadeInDelay += 200;
			});
			
			if(parall)parall.scroll();
		}
	},100); 
})(jQuery);	