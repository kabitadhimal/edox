<?php
/*
 *
* Menu
*
*/
function kd_menu_left() {
	$defaults = array(
			'theme_location'  =>'primary' ,
			'menu'            => '',
			'container_id'    => '',
			'menu_class'      => 'nav navbar-nav nav-sidebar',
			'menu_id'         => '',
			'echo'            => true,
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
	);

	if ( has_nav_menu( 'primary' ) ):
	wp_nav_menu( $defaults );
	endif;
}


/*
 *   Language Display
 */

Function kd_language_dislay() {
		if (function_exists('icl_get_languages')):
		$languages = icl_get_languages('skip_missing=' . intval($skip_missing).'&orderby=code&order=desc');
		//var_dump($languages);
		foreach ($languages as $l):
		if($l['active']==1) {
			echo '<button class="btn dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">'.strtoupper ($l['language_code']).'</button>';
		
		} else  {
		?>
			<ul class="language-down">
	            <li><?php  echo '<a href="' . $l['url'] . '">' . strtoupper ($l['language_code']) . '</a>';?></li>
	        </ul>
			<?php 
			}
        endforeach;
	endif;
}
