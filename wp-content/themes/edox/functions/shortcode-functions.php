<?php 

//Add header


function kd_header($atts,$content=null) {
	$header ='<div class="full-width header-section content-section bg-blue text-center"><div class="bg-line">'.$content.'</div></div>';
	return $header;
}


// Add introduction

function kd_introduction($atts,$content=null) {
	
	$intro ='<div class="full-width content-section bg-grey-2 text-center">'. do_shortcode($content).'</div>';
	
	return $intro;
	
}



/*
function kd_add_link($atts,$content=null) {
	extract(shortcode_atts(array('linkurl'), $atts));
	$link = '<a href="'.$link.'">click</a>';
	return $link;
	
}
*/


function kd_button($atts,$content=null) {
	extract(shortcode_atts(array(
		'type' =>'box',
		'href' =>'',
		'target' =>''
	), $atts));
	
	$label = '';
	
	if(!empty($target)):
		$tar = 'target="_blank"';
	endif;
	
	if($type=="arrow") {
		 $class = 'class="btn-arrow"';
	}else if($type=="transparent") {
		 $class = 'class="btn-arrow bg-transparent"';
	} else  {
		$label = $content;
		$class = 'class="btn-cms"';
	}
	
	return '<a '.$class.' href="'.$href.'" ' .$tar. '>'.$label.'</a>';
}


function kd_teaser( $atts, $content = null ) {
	return  '<div class="page-teaser">'.$content.'</div>';
}


//Banner
function kd_banner($atts,$content=NULL) {
	$banner = '<div class="full-width-extend banner-section row">'.$content.'</div>';
	return $banner;
}


/*
//Adds Button to the Editor
function kd_button($atts,  $content = null){
	extract(shortcode_atts(array(
	'link' => '',
	'target' =>''
			), $atts));

	if(!empty($target)):
	$tar = 'target="_blank"';
	endif;
	 return '<div class="button-rollover"><a data-text="'.$content.'" href="'.$link.'" >'.$content.'</a></div>';
}
*/
//Separator

function kd_header_separator() {
		$sep ='<span class="title-sep"></span>';
	return $sep;
}

//Text and  Image
function kd_row_img( $atts, $content = null ) {
	return  '<div class="full-width-extend half-image-text-section"><div class="row row-vertical bg-dark-grey text-center">'.do_shortcode($content).'</div></div>';
}

function kd_col_img($atts, $content=NULL) {
	extract(shortcode_atts(array('img' =>''), $atts));
	return '<div class="col-xs-6 col-sm-3">'.$img.'</div><div class="col-sm-3 teaser">'. do_shortcode($content).'</div>';
	
	/*
	return '<div class="col-md-6">
        	<div class="row">
            	<figure class="col-md-6">'.$img.'</figure>
            	<div class="col-md-6 teaser">'. do_shortcode($content).
                '</div>
            </div>
        </div>';	
		*/

}


function kd_slider($atts, $content=NULL) {
	extract(shortcode_atts(array('type' =>'normal','id'=>''), $atts));
	
	$query = new WP_Query( array( 'post_type' => 'slider', 'p' => $id ) );
	$sliderId = rand();
	//var_dump($query->get_posts());
	$totalItems = 0;
	if($type=='LeftTextImageRight'):
		
	$slider = '<div class="full-width-extend ">
		<div class="grey-bg">
		<div class="slider-2-section clearfix carousel slide carousel-fade row" data-interval="false" data-keyboard="false" data-ride="carousel" id="slider_'.$sliderId.'">
		<ul class="carousel-inner">';
	
	while ($query->have_posts()): $query->the_post();
		
	if( have_rows('add_slider') ):
	$cssClass = 'active';
	while( have_rows('add_slider') ): the_row();
		
	$image = get_sub_field('slider_image');
	$title = get_sub_field('slider_title');
	$text = get_sub_field('slider_text');
	$slider .= '<li class="item '.$cssClass.'">';
	$slider .= '<div class="slider-content">
					<div class="hidden-sm hidden-xs hidden-md"><h1>'.$title.'</h1>'.$text	.'</div>';
	if($image) :
	$imgSrc = wp_get_attachment_image_src($image,'large-slider');
	$slider .=	'<div><img src="'.$imgSrc[0].'"></div>';
	endif;
		
	$slider .='<div class="visible-sm visible-xs visible-md"><h1>'.$title.'</h1>'.$text	.'</div></div></li>';
	$cssClass = '';
	$totalItems++;
	endwhile;
	
	endif;
	
	endwhile;
	$slider .= '</ul>';
	if($totalItems > 1):
	$slider .= '<a class="left carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>';
	endif;
	$slider .= '</div>
			</div>
			</div>';
 
elseif ($type=='leftImageTextRight'):
			$slider ='<div class="full-width-extend">
			<div class="grey-bg">
			<div class="slider-2-section-reversed clearfix carousel slide carousel-fade" data-ride="carousel">
					<ul class="carousel-inner">';

			while ($query->have_posts()): $query->the_post();

			 if( have_rows('add_slider') ): ?>
			 
			 <?php 
				$cssClass = 'active';
				while( have_rows('add_slider') ): the_row(); 
					// vars
					$image = get_sub_field('slider_image');
					$title = get_sub_field('slider_title');
					$text = get_sub_field('slider_text');
			 
			 
					$slider .= '<li class="item '.$cssClass.'"><div>';
			
					if($image) :
						$imgSrc = wp_get_attachment_image_src($image,'large-slider');
						$slider .=	'<div> <img src="'.$imgSrc[0].'"> </div>';
					endif;
					
							$slider .= '<div><h1>'.$title.'</h1>'.$text.'</div></div></li>';
					$cssClass = '';
					$totalItems++;
				endwhile;
			    endif;			
			endwhile;
			
			$slider .=	'</ul>';
			if($totalItems > 1):
			$slider .='<p>
			<a class="left carousel-control" href=".slider-2-section-reversed" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href=".slider-2-section-reversed"" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>
			</p>';
			endif;
			$slider .=	'</div>
			</div>
			</div>';
			
			
	elseif ($type=='ImageLeftImageSliding') :
	
			$slider = '<div class="full-width-extend">';
			while ($query->have_posts()): $query->the_post();
			$slider .= '<div class="grey-bg">
			<div class="slider-2-section-static clearfix carousel slide carousel-fade" data-ride="carousel"><div class="hidden-md hidden-sm hidden-xs">'.get_the_content().'</div>
			<div>';
				$slider .= '<ul class="carousel-inner">';
		
				if( have_rows('add_slider') ): 
							
				$cssClass = 'active';
				while( have_rows('add_slider') ): the_row(); 
					// vars
					$image = get_sub_field('slider_image');
					$slider .= '<li class="item '.$cssClass.'">';
					if($image) :
						$imgSrc = wp_get_attachment_image_src($image,'large-slider');
						$slider .=	'<div> <img src="'.$imgSrc[0].'"> </div>';
					endif;
					$slider .= '</li>';
					$cssClass = '';
					$totalItems++;
				endwhile;
			$slider .= '</ul>';
				endif;
			endwhile;
			
			$slider .='</div>';
			$slider .= '<p class="visible-md visible-sm visible-xs">
			<a class="left carousel-control" href=".slider-2-section-static" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href=".slider-2-section-static" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>
			</p>';
			$slider .='<div class="visible-md visible-sm visible-xs">'.get_the_content().'</div>';
			if($totalItems > 1):
			$slider .= '<p class="hidden-md hidden-sm hidden-xs">
			<a class="left carousel-control" href=".slider-2-section-static" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href=".slider-2-section-static" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>
			</p>';
			endif;
		
			$slider .='</div>
			</div>
			</div>';
		wp_reset_postdata();
		
	elseif ($type=='ImageRightImageSliding'):
	
	
	$slider = '<div class="full-width-extend">
	<div class="grey-bg">
	<div class="slider-2-section-reversed-static clearfix carousel slide carousel-fade" data-ride="carousel">';
	
	while ($query->have_posts()): $query->the_post();
			$slider .= '<div>
			<ul class="carousel-inner">';
			if( have_rows('add_slider') ):
				$cssClass = 'active';
				while( have_rows('add_slider') ): the_row();
				
				$image = get_sub_field('slider_image');
				$slider .= '<li class="item '.$cssClass.'">';
				
				if($image) :
							$imgSrc = wp_get_attachment_image_src($image,'large-slider');
							$slider .=	'<div> <img src="'.$imgSrc[0].'"> </div>';
						endif;
					
				$slider .='</li>';
				$cssClass = '';
				$totalItems++;
				endwhile;
			endif;
			
			$slider .='</ul>
			</div>';
			$slider .= '<p class="visible-md visible-sm visible-xs">
			<a class="left carousel-control" href=".slider-2-section-reversed-static" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href=".slider-2-section-reversed-static" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
			</a>
			</p>';
			$slider .='<div class="reverse-content">'.get_the_content().'</div>';
			
		endwhile;

		if($totalItems > 1):
		$slider .='<p class="hidden-md hidden-sm hidden-xs">
		<a class="left carousel-control" href=".slider-2-section-reversed-static" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href=".slider-2-section-reversed-static" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
		</a>
		</p>';
		endif;
		$slider .='</div>
		</div>
		</div>';
		
	wp_reset_postdata();
	
	
	
  	else:
  	
			$slider ='<div class="fixed-width slider-1-section clearfix carousel slide row" data-interval="false" data-keyboard="false" data-ride="carousel" id="slider_'.$sliderId.'">
				<ul class="carousel-inner">';
			while ($query->have_posts()): $query->the_post();
			
						 if( have_rows('add_slider') ): ?>
						
							<?php 
							$cssClass = 'active';
							while( have_rows('add_slider') ): the_row(); 
								// vars
								$image = get_sub_field('slider_image');
								$title = get_sub_field('slider_title');
								$text = get_sub_field('slider_text');
								
								$slider .= '<li class="item '.$cssClass.'">';
								if($image) :
										$imgSrc = wp_get_attachment_image_src($image,'normal-slider');
										$slider .=	'<div> <img src="'.$imgSrc[0].'"> </div>';
								endif;
								$slider .= '<div><h1>'.$title.'</h1>'.$text.'</div></li>';
								$cssClass = '';
								$totalItems++;
							 endwhile;
						
						endif;
						
					endwhile;
						
					$slider .='</ul>';
					if($totalItems > 1):
					$slider .='<a class="left carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="prev">
							  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				              <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="next">
				               <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							   <span class="sr-only">Next</span>
							   </a>';
					endif;
					$slider .='</div>';
			
 endif;

return $slider;
}
		


/*
//Text
function kd_row_2($atts,$content=NULL) {
	
	return '<div class="fixed-width content-section half-text-section row clearfix">'.do_shortcode($content).'</div>';

}
*/

/*
function kd_column_1($atts,$content=NULL) {
	return '<div class="col-md-6">'.do_shortcode($content).'</div>';
}
*/


/*
//Two column Image

function kd_row_3($atts,$content=NULL) {

	return '<div class="full-width-extend">'.do_shortcode($content).'</div>';

}
*/


function kd_quotation($atts,$content=NULL) {
	return '<div class="full-width bg-grey quotation-section">'.do_shortcode($content).'</div>';
}


function kd_col($atts,$content = null) {
	extract(shortcode_atts(array('class' =>''), $atts));
	return '<div class="'.$class.' col-md-6">'.do_shortcode($content).'</div>';
}
function kd_row($atts,$content = null) {
	extract(shortcode_atts(array('class' =>''), $atts));
	return '<div class="'.$class.' row">'.do_shortcode($content).'</div>';
}




function register_shortcodes(){
	add_shortcode('header', 'kd_header');
	//add_shortcode('link', 'kd_add_link');
	
	//arrow
	add_shortcode('btn', 'kd_button');
	add_shortcode('teaser', 'kd_teaser');
	add_shortcode('introduction', 'kd_introduction');

	add_shortcode('banner', 'kd_banner');
	
	add_shortcode('row-img', 'kd_row_img');
	add_shortcode('col-img', 'kd_col_img');
	
	
	//add_shortcode('row-2', 'kd_row_2');
	//add_shortcode('row-3', 'kd_row_3');
	
	
	//add_shortcode('column-1', 'kd_column_1');
	add_shortcode('quote', 'kd_quotation');
	
	//bootstrap shortocodes
	add_shortcode('col', 'kd_col');
	add_shortcode('row', 'kd_row');
	add_shortcode('slider', 'kd_slider');
}
add_action( 'init', 'register_shortcodes');



