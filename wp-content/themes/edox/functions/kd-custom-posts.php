<?php
function add_post_type($name, $args = array()){
	add_action('init', function() use($name, $args){
		$upper = ucwords($name);
		$name = strtolower(str_replace(' ', '_', $name));
		
		$args = array_merge(
				array(
					'public'	=> true,
					'label'		=> "$upper",
					'labels'	=> array('add_new_item'=>"Add new $upper"),
					'supports' 	=> array('title','editor','thumbnail')
				), 
				$args);
		
		register_post_type($name, $args);
	});
}

function add_taxonomy($name, $post_type, $args = array()){
	$name = strtolower($name);
	add_action('init', function() use($name, $post_type, $args){
		$args = array_merge(
				array(
					'label' => ucwords($name),
					 'show_ui' => true,
		        	'query_var' => true,
				), 
				$args);
		//echo '<pre>';	var_dump($args);	echo '</pre>';
		register_taxonomy($name, $post_type,  $args);
	});
}

/*

function create_qa_post_types() {    
     global $qaplus_options;     
     $labels = array(
        'name' => _x( 'FAQ Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'FAQ Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search FAQ Categories' ),
        'all_items' => __( 'All FAQ Categories' ),
        'parent_item' => __( 'Parent FAQ Category' ),
        'parent_item_colon' => __( 'Parent FAQ Category:' ),
        'edit_item' => __( 'Edit FAQ Category' ), 
        'update_item' => __( 'Update FAQ Category' ),
        'add_new_item' => __( 'Add New FAQ Category' ),
        'new_item_name' => __( 'New FAQ Category Name' ),
    );
    register_post_type( 'qa_faqs',
        array(
            'labels' => array(
                'name' => __( 'FAQs' ),
                'singular_name' => __( 'FAQ' ),
                'edit_item' =>   __( 'Edit FAQ'),
                'add_new_item'  =>   __( 'Add FAQ')
            ),
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'rewrite' => array( 'slug' => $qaplus_options['faq_slug'], 'with_front' => false ),
            'taxonomies' => array( 'FAQs '),
            'supports' => array('title','editor')    
        )
    );    
    register_taxonomy('faq_category',array('qa_faqs'), array(
        'hierarchical' => true,
        'labels' => $labels,
       
        'rewrite' => array( 'slug' => 'faq-category' ),
  )); 
} 




/*
function create_qa_post_types() {    
     global $qaplus_options;     
     $labels = array(
        'name' => _x( 'FAQ Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'FAQ Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search FAQ Categories' ),
        'all_items' => __( 'All FAQ Categories' ),
        'parent_item' => __( 'Parent FAQ Category' ),
        'parent_item_colon' => __( 'Parent FAQ Category:' ),
        'edit_item' => __( 'Edit FAQ Category' ), 
        'update_item' => __( 'Update FAQ Category' ),
        'add_new_item' => __( 'Add New FAQ Category' ),
        'new_item_name' => __( 'New FAQ Category Name' ),
    );
    register_post_type( 'qa_faqs',
        array(
            'labels' => array(
                'name' => __( 'FAQs' ),
                'singular_name' => __( 'FAQ' ),
                'edit_item' =>   __( 'Edit FAQ'),
                'add_new_item'  =>   __( 'Add FAQ')
            ),
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'rewrite' => array( 'slug' => $qaplus_options['faq_slug'], 'with_front' => false ),
            'taxonomies' => array( 'FAQs '),
            'supports' => array('title','editor')    
        )
    );    
    register_taxonomy('faq_category',array('qa_faqs'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'faq-category' ),
  )); 
}   
*/