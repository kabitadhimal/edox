<?php
define ('TEMP_DIR_URI', get_template_directory_uri() );
define ('TEMP_DIR', get_template_directory() );
define('CATALOG_URL', get_site_url().'/Catalog/');
/**
 * Edox functions and definitions

 *

 * Set up the theme and provides some helper functions, which are used in the

 * theme as custom template tags. Others are attached to action and filter

 * hooks in WordPress to change core functionality.

 *

 * When using a child theme you can override certain functions (those wrapped

 * in a function_exists() call) by defining them first in your child theme's

 * functions.php file. The child theme's functions.php file is included before

 * the parent theme's file, so the child theme functions would be used.

 *

 * @link http://codex.wordpress.org/Theme_Development

 * @link http://codex.wordpress.org/Child_Themes

 *

 * Functions that are not pluggable (not wrapped in function_exists()) are

 * instead attached to a filter or action hook.

 *

 * For more information on hooks, actions, and filters,

 * @link http://codex.wordpress.org/Plugin_API

 *

 * @package WordPress

 * @subpackage Edox

 * @since Edox 1.0

 */

/**

 * Set up the content width value based on the theme's design.

 *

 * @see kd_content_width()

 *

 * @since Edox 1.0

 */

if ( ! function_exists( 'kd_setup' ) ) :

/**

 * Edox setup.

 *

 * Set up theme defaults and registers support for various WordPress features.

 *

 * Note that this function is hooked into the after_setup_theme hook, which

 * runs before the init hook. The init hook is too late for some features, such

 * as indicating support post thumbnails.

 *

 * @since Edox 1.0

 */

function kd_setup() {
	/*

	 * Make Edox available for translation.

	 *

	 * Translations can be added to the /languages/ directory.

	 * If you're building a theme based on Edox, use a find and

	 * replace to change 'kd' to the name of your theme in all

	 * template files.

	 */

	load_theme_textdomain( 'edox' );
	// Enable support for Post Thumbnails, and declare two sizes.

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 300, 300, true );

	add_image_size( 'bannerimage', 1900, 850, true );

	add_image_size('partnership-img',399,403,true);

	add_image_size('vip-img',400,400,true);

	add_image_size('press-img',300,403,true);

	add_image_size('newsimage',530, 540, true);	

	add_image_size('cms-image',1918,1019,true);

	add_image_size('normal-slider',450,271,true);

	add_image_size('large-slider',799,592,true);

	add_image_size('text-img',396,392,true);

	add_image_size('catolog-img',451,271,true);

	add_image_size('flims-img',1600,720,true);
	
	add_image_size('featured_preview',150,150,false);
	
	

	register_nav_menus(

			array(

			'primary' => __( 'Primary Menu' ),

			'top-menu' => __( 'Top Menu' )

	));
}

endif; // kd_setup

add_action( 'after_setup_theme', 'kd_setup' );

/*

 * Change the length of the Excerpt

 */

/*

function the_excerpt_max_charlength($charlength) {

	$excerpt = get_the_excerpt();

	$charlength++;



	if ( mb_strlen( $excerpt ) > $charlength ) {

		$subex = mb_substr( $excerpt, 0, $charlength - 5 );

		$exwords = explode( ' ', $subex );

		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

		if ( $excut < 0 ) {

			echo mb_substr( $subex, 0, $excut );

		} else {

			echo $subex;

		}

		echo '[...]';

	} else {

		echo $excerpt;

	}

}

*/
/**

 * Register three Edox widget areas.

 *

 * @since Edox 1.0

 */
function kd_widgets_init() {

	register_sidebar ( array (
	    'name' => __ ( 'Sidebar', 'edox' ),
	    'id' => 'sidebar-1',
	    'description' => __ ( 'An optional widget area for  formun page', 'edox' ),
	    'before_widget' => ' <div>',
	    'after_widget' => "</div>",
	    'before_title' => '<p>',
	    'after_title' => '</p>'
	) );
	
}

add_action ( 'widgets_init', 'kd_widgets_init' );

/**

 * Create a nicely formatted and more specific title element text for output

 * in head of document, based on current view.

 *

 * @since Edox 1.0

 *

 * @global int $paged WordPress archive pagination page count.

 * @global int $page  WordPress paginated post page count.

 *

 * @param string $title Default title text for current view.

 * @param string $sep Optional separator.

 * @return string The filtered title.

 */

function kd_wp_title( $title, $sep ) {

	global $paged, $page;

	if ( is_feed() ) {

		return $title;

	}

	// Add the site name.

	$title .= get_bloginfo( 'name', 'display' );
	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) ) {

		$title = "$title $sep $site_description";

	}
	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {

		$title = "$title $sep " . sprintf( __( 'Page %s', 'kd' ), max( $paged, $page ) );

	}
	return $title;

}
add_filter( 'wp_title', 'kd_wp_title', 10, 2 );

/*

require TEMP_DIR . '/functions/kd_list_posts/widget.php';

require TEMP_DIR . '/functions/aboutus.php';

*/

//require TEMP_DIR . '/functions/kd-custom-posts.php';

//shortcodes

include 'functions/home_functions.php';
require TEMP_DIR . '/functions/kd-custom-posts.php';
include 'includes/shortcodes.php';
include 'functions/shortcode-functions.php';
add_post_type ( 'slider', array (
	'name' => 'Slider ',
	'menu_icon' => 'dashicons-calendar',
	'label' => "Slider",

));

add_post_type ( 'home-slider', array (
'name' => 'Home Slider ',
'menu_icon' => 'dashicons-calendar',
'label' => "Home Slider",
));

add_post_type ( 'press-reviews', array (
'name' => 'Publicity ',
'menu_icon' => 'dashicons-calendar',
'label' => "Publicity"
));

add_post_type ( 'publicity', array (
'name' => 'Press Reviews',
'menu_icon' => 'dashicons-calendar',
'label' => "Press Reviews",
));

add_post_type ( 'vintage', array (
'name' => 'Vintage',
'menu_icon' => 'dashicons-calendar',
'label' => "Vintage",
));

add_post_type ( 'partnership', array (
'name' => 'Partnership ',
'menu_icon' => 'dashicons-calendar',
'label' => "Partnership",
));

add_post_type ( 'vip', array (
'name' => 'VIP ',
'menu_icon' => 'dashicons-calendar',
'label' => "VIP",
));

add_post_type ( 'films', array (
'name' => 'Films',
'menu_icon' => 'dashicons-calendar',
'label' => " Films",
));

add_post_type ( 'catalog', array (
'name' => 'Catalog',
'menu_icon' => 'dashicons-calendar',
'label' => "Catlog",
));

add_post_type('stores',array(
'name'=> 'Stores',
'menu_icon' => 'dashicons-calendar',
'Label' => 'Stores',
));

add_action( 'init', 'kd_create_stores_tax' );
function kd_create_stores_tax() {
	register_taxonomy(
		'category-stores',
		'stores',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'category-stores' ),
			'hierarchical' => true,
		));

register_taxonomy(
		'category-year',
		'films',
		array(
			'label' => __( 'Year' ),
			'rewrite' => array( 'slug' => 'year' ),
			'hierarchical' => true,
));


register_taxonomy(
	'countries',
	'stores',
	array(
	'label' => __( 'countries' ),
	'rewrite' => array( 'slug' => 'countries-stores' ),
	'hierarchical' => true,
	));

register_taxonomy(
    'countries-publicity',
    'publicity',
    array(
    'label' => __( 'Countries' ),
    'rewrite' => array( 'slug' => 'countries-publicity' ),
    'hierarchical' => true,
));

} // End of kd_create_stores_tax 





/*

add_post_type ( 'case-study', array (

	'name' => 'Case Study ',

	'menu_icon' => 'dashicons-calendar',

	'label' => "Case Study",

));



*/

/*



//Creating custom post type  News

add_post_type ( 'labotec', array (

	'name' => 'Labo & Techniques ',

	'menu_icon' => 'dashicons-calendar',

	'label' => "Labo & Tecniques",

	//'rewrite' => array('slug' => 'labo-tech-item')

) );



add_post_type ( 'reference', array (

	'name' => 'Reference ',

	'menu_icon' => 'dashicons-calendar',

	'label' => "References",

	'rewrite' => array('slug' => 'reference-item')

) );

*/



/*

function get_excerpt(){

	$excerpt = get_the_content();

	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);

	$excerpt = strip_shortcodes($excerpt);

	$excerpt = strip_tags($excerpt);

	$excerpt = substr($excerpt, 0, 150);

	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));

	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));

	$excerpt = $excerpt.'...';

	return $excerpt;

}

*/



/*

 * Allows extra HTML items in to the_excerpt instead of stripping them like WordPress does

*/

function theme_t_wp_improved_trim_excerpt($text) {

	global $post;

	if ( '' == $text ) {

		$text = get_the_content('');

		$text = apply_filters('the_content', $text);

		$text = str_replace(']]>', ']]&gt;', $text);

		$text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);

		$text = strip_tags($text, '<p>,<ul>,<li>,<ol>');

		$excerpt_length = 10;

		$words = explode(' ', $text, $excerpt_length + 1);

		if (count($words)> $excerpt_length) {

			array_pop($words);

			array_push($words, '');

			$text = implode(' ', $words);

		}

	}

	return $text;

}



remove_filter('get_the_excerpt', 'wp_trim_excerpt');

add_filter('get_the_excerpt', 'theme_t_wp_improved_trim_excerpt');



// Adding JQuery

function procab_scripts_method() {	

	// wp_enqueue_style ( 'style', TEMP_DIR_URI . '/css/style.css', true, '1.3' );

	// wp_enqueue_style ( 'owlcarousel', TEMP_DIR_URI . '/css/owl.carousel.css', true, '1.3' );

	// wp_enqueue_style ( 'owltransition', TEMP_DIR_URI . '/css/owl.transitions.css', true, '1.3' );

	// wp_enqueue_style ( 'font-box', TEMP_DIR_URI . '/css/jquery.fancybox.css', true, '1.3' );



	wp_enqueue_style ( 'font-awesome', TEMP_DIR_URI . '/assets/font-awesome-4.3.0/css/font-awesome.min.css', array(), '1.2.2' );

	wp_enqueue_style ( 'bootstrap', TEMP_DIR_URI . '/assets/bootstrap/css/bootstrap.css', array(), '1.2.2' );

	wp_enqueue_style ( 'animate', TEMP_DIR_URI . '/assets/animate/animate.css', array(), '1.2.2' );

//	wp_enqueue_style ( 'kwicks', TEMP_DIR_URI . '/assets/kwicks/jquery.kwicks.css', array(), '1.2.2' );

//	wp_enqueue_style ( 'dcsns', TEMP_DIR_URI . '/assets/social/dcsns_wall.css', array(), '1.2.1' );

//	wp_enqueue_style ( 'meanmenu', TEMP_DIR_URI . '/assets/meanmenu/meanmenu.css', array(), '1.2.2' );

	wp_enqueue_style ( 'swiper', TEMP_DIR_URI . '/assets/swiper/swiper.min.css', array(), '1.2.2' );







	wp_enqueue_style ( 'fancybox', TEMP_DIR_URI . '/assets/rollover/jquery.fancybox.css', array(), '1.2.4' );

	wp_enqueue_style ( 'pages', TEMP_DIR_URI . '/assets/pages.css', array(), '1.2.4' );

	wp_enqueue_style ( 'styles', TEMP_DIR_URI . '/assets/style.css', array(), '1.2.6' );

	//wp_enqueue_style ( 'common', TEMP_DIR_URI . '/assets/common.css?i=psps', array(), '1.2.4' );

	wp_enqueue_style ( 'common', get_site_url().'/shop/skin/frontend/rwd/edox/css/common.css', array(), '1.2.4' );



	

	wp_deregister_script('jquery');

	wp_register_script ( 'jquery', TEMP_DIR_URI . '/assets/jquery.js', true, '1.0');

	wp_enqueue_script('jquery');



	

	wp_register_script ( 'meanmenujs', TEMP_DIR_URI . '/assets/meanmenu/jquery.meanmenu.js', array(), '1.0');

	



/*	wp_register_script ( 'globalScripts', TEMP_DIR_URI . '/assets/waypoint/js/waypoints.min.js', true, '1.2', true);*/
	wp_register_script ( 'globalScripts', TEMP_DIR_URI . '/js/globalScripts.js', true, '1.2', true);



	wp_register_script ( 'swiperjs', TEMP_DIR_URI . '/assets/swiper/swiper.min.js', true, '1.2', true);	

	wp_register_script ( 'select', TEMP_DIR_URI . '/assets/select.js', true, '1.2', true);

	wp_register_script ( 'script', TEMP_DIR_URI . '/assets/script.js', true, '1.2', true);



	wp_register_script ( 'rollover', TEMP_DIR_URI . '/assets/rollover/rollover.js', array(), '1.2' );

	wp_register_script ( 'fancy-box-js', TEMP_DIR_URI . '/assets/rollover/jquery.fancybox.js?v=2.1.5', array(), '1.0' );



    

	

	if(is_home()):

		// wp_register_script ( 'homeScripts', TEMP_DIR_URI . '/js/homeScripts.js', true, '1.0',true );

	

	endif;

	wp_enqueue_script ( array (

		'meanmenujs',

		'waypoints',

		'globalScripts',

		'swiperjs',

		'select',

		'script',

		'rollover',

		'fancy-box-js'

			));

	

} // procab_scripts_method

add_action ( 'wp_enqueue_scripts', 'procab_scripts_method' );



// Display page

function kd_display_page($id) {

	$query = new wp_query('page_id='.$id);

	while ($query->have_posts()):$query->the_post();

	the_content();

	endwhile;

	wp_reset_query();

}

/*

//Adding body class

function kd_body_class($classes) {

	global $post;

	

	$post_type =get_query_var('post_type');



	$post_type1 = 'case-study';

	

	$pageTemplate = get_post_meta ( $post->ID, '_wp_page_template', true );

	

	$post_type =get_query_var('post_type');

	

	

	$post_type2 = 'glossary';

	

	 // only, if post type is active

	if (get_query_var ( 'post_type' ) === $post_type2) {

		// single page under Theme

		if (is_single ())

			$custom_class = 'lexicon-page';



	}else if ($pageTemplate == 'page-template/team-template.php') {

		$custom_class = 'inner-page team-page';

	} else if($pageTemplate == 'page-template/casestudy-template.php') {

		$custom_class = 'inner-page case-study-page';

	}else if( $post_type === $post_type1) {

		$custom_class ='inner-page case-detail-page';

	}else if($pageTemplate == 'page-template/news-template.php' || is_search()) {

		$custom_class = 'inner-page news-page';

	}else if($pageTemplate=='page-template/reference-template.php') {

		$custom_class = 'inner-page references-page';

	}else if($pageTemplate=='page-template/labo-tech-template.php') {

		$custom_class = 'inner-page techniques-page';

	}else if($pageTemplate=='page-template/contact-template.php') {

		$custom_class = 'page-template/inner-page contact-page';

	}

	else if(is_home()) {

		$custom_class = 'home';

	}else if (is_page ()) {

	

		$custom_class = 'cms-page';

	}

	$class [] = $custom_class;

	

	return $class;

	

}

add_filter ( 'body_class', 'kd_body_class' );

*/





//remove stress characters, stress characters

add_filter('wp_handle_upload_prefilter', 'procab_upload_filter',1,1 );

function procab_upload_filter( $file ){

	$file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/','-', $file['name']);

	return $file;

}



remove_filter( 'the_content', 'wpautop' );

add_filter( 'the_content', 'wpautop' , 12);



function last_words($amount, $string)

{

	$amount+=1;

	$string_array = explode(' ', $string);

	$totalwords= str_word_count($string, 1, '????3');

	if($totalwords > $amount){

		$words= implode(' ',array_slice($string_array, count($string_array) - $amount));

	}else{

		$words= implode(' ',array_slice($string_array, count($string_array) - $totalwords));

	}



	return $words;

}

//  Custom post type navigation

function kd_custom_post_nav() {

	global $wp_query;

	

	if ( $wp_query->max_num_pages > 1 ) :

		echo '<nav id="nyb_post_pagi">';

	$previous = (ICL_LANGUAGE_CODE == 'fr') ? '« articles plus anciens' : 'older articles';

	$new = (ICL_LANGUAGE_CODE == 'fr') ? '« nouveaux articles' : '« new articles';

		next_posts_link( $previous );

		previous_posts_link($new);

		echo '</nav>';

	endif;

}



// Adding class to next and previous link 

add_filter('next_posts_link_attributes', 'kd_posts_link_attributes_1');

add_filter('previous_posts_link_attributes', 'kd_posts_link_attributes_2');



function kd_posts_link_attributes_1() {

	return 'class="back-btn"';

}

function kd_posts_link_attributes_2() {

	return 'class="back-btn"';

}



/*

// Displays only posts on Search

add_filter('pre_get_posts', 'kd_filter_search');

function kd_filter_search($query) {

	if ($query->is_search) {

		$query->set('post_type', array('post','stores'));

	};

	return $query;

};

add_filter('pre_get_posts', 'kd_filter_search');

*/



/**

 * usess tim thumb

 * returns image url

 * <img data-src="" src="<?php echo kd_resize_url($imageUrl, array('w'=>1920, 'h'=>700)); ?>" alt="<?php the_title();?>">

 */

function kd_resize_url($image, $args = null) {

	$image = explode ( 'wp-content', $image );

	$image = '/wp-content' . $image [1];

	$query = '';

	if ($args === null)

		$args = array (

				'w' => 100

		);

		foreach ( $args as $key => $val ) {

			$query .= '&' . $key . '=' . $val;

		}

		$str = get_template_directory_uri () . '/timthumb.php?src=' . $image . '' . $query;

		return $str;

}



/**

 * usess tim thumb

 * returns image tag

 * echo kd_resize($imageUrl, array('w'=>1920, 'h'=>700), 'test-css-for-iamge')

 */

function kd_resize($image, $args = null, $cssClass = '') {

	// $imageUrl = wp_get_attachment_url( get_post_thumbnail_id(), 'full');

	$image = explode ( 'wp-content', $image );

	$image = '/wp-content' . $image [1];



	$query = '';

	if ($args === null)

		$args = array (

				'w' => 100

		);

		foreach ( $args as $key => $val ) {

			$query .= '&' . $key . '=' . $val;

		}

		$str = get_template_directory_uri () . '/timthumb.php?src=' . $image . '' . $query;

		$cssClass = ($cssClass != '') ? "class='" . $cssClass . "'" : '';

		return "<img src='" . $str . "' " . $cssClass . " />";

}



remove_filter( 'the_content', 'wpautop' );

add_filter( 'the_content', 'wpautop' , 12);



//  Take Content based on Maxwords and Max chars

function truncate($input, $maxWords, $maxChars)

{
	//$input = do_shortcode($input);

	$words = preg_split('/\s+/', $input);

	$words = array_slice($words, 0, $maxWords);

	$words = array_reverse($words);

	$chars = 0;

	$truncated = array();

	while(count($words) > 0) {

		$fragment = trim(array_pop($words));

		$chars += strlen($fragment);

		if($chars > $maxChars) break;

		$truncated[] = $fragment;

	}



	$result = implode($truncated, ' ');

	$res = do_shortcode($result);

	//return $result.' ...';

	return $result;

}



function the_slug($echo=true){

	$slug = basename(get_permalink());

	do_action('before_slug', $slug);

	$slug = apply_filters('slug_filter', $slug);

	if( $echo ) echo $slug;

	do_action('after_slug', $slug);

	return $slug;

}



remove_filter( 'the_content', 'wpautop' );



remove_filter( 'the_excerpt', 'wpautop' );



function kd_get_excerpt($count){

	$permalink = get_permalink($post->ID);

	$excerpt = get_the_content();

	$excerpt = strip_tags($excerpt);

	$excerpt = substr($excerpt, 0, $count);

	$excerpt = $excerpt;

	return $excerpt;

}





/**

 * Display descriptions in main navigation.

 *

 * @since Twenty Fifteen 1.0

 *

 * @param string  $item_output The menu item output.

 * @param WP_Post $item        Menu item object.

 * @param int     $depth       Depth of the menu.

 * @param array   $args        wp_nav_menu() arguments.

 * @return string Menu item with possible description.

 */



function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {

	if ( 'primary' == $args->theme_location && $item->description ) {

		if(trim($item->description) !=''){

			$item_output = str_replace( $args->link_after . '</a>', $args->link_after.'<div class="menu-item-title">'.$item->title.'</div><div class="menu-item-description">' . nl2br($item->description) . '</div></a><a href="'.$item->url.'" class="btn-menu"></a>', $item_output	 );			

		}else{

			$item_output = str_replace( $args->link_after . '</a>', $args->link_after.'</a><a href="'.$item->url.'" class="btn-menu"></a>', $item_output	 );			

		}

	}else if('top-menu' == $args->theme_location){

			$item_output = str_replace( $args->link_after . '</a>', $args->link_after.'<span>'.$item->title.'</span></a>', $item_output	 );			

		

		

	}

	return $item_output;

}

add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );





//enable html tags on the menu. this will let $item->post_content to have html tags

remove_filter('nav_menu_description', 'strip_tags');



//add filter for $item->description to have html tags on the front end and on the backend

add_filter( 'wp_setup_nav_menu_item', 'edox_wp_setup_nav_menu_item' );

function edox_wp_setup_nav_menu_item( $menu_item ) {

    if ( isset( $menu_item->post_type ) ) {

        if ( 'nav_menu_item' == $menu_item->post_type ) {

            $menu_item->description = apply_filters( 'nav_menu_description', $menu_item->post_content );

        }

    }

    return $menu_item;

}

/**/
//menus for magento

include 'includes/ajax-request.php';

//include 'includes/ajax-footer.php';



function sendAddressToMailChimp($email, $lang = en){

	require_once(__DIR__.'/libs/mailchimp/MCAPI.class.php');

	define('MC_API_KEY', '1e7fe41d3cf13341986c2fe7dafd5125-us10');

	if($lang == 'en'){

		$listID = '2152f34cb6';

	}else{

		$listID = '2152f34cb6';

	}

	$api = new MCAPI(MC_API_KEY);

	

	return ($api->listSubscribe($listID, $email, '',null,'html',false));

}



/*

 * Send mail based on Country selected

 */



function wpcf7_before_send_mail_attachReceiver ($wpcf7) {

	//var_dump($_POST);

	if(isset($_POST['subscribed']) && $_POST['subscribed']==true){

		//$properties = $wpcf7->get_properties();

		//var_dump($properties);

		$userEmail = $_POST['email'];

		//ues mailchimp api

		if($userEmail != '' && is_email($userEmail)) sendAddressToMailChimp($userEmail, ICL_LANGUAGE_CODE);

	}

	

	if(isset($_POST['change-email'])){

		$properties = $wpcf7->get_properties();

		//get receiver by country

		$countrySelected =  strtolower($_POST['country']);

		

		if($countrySelected == 'france'){

			//$properties['mail']['recipient'] = $properties['mail']['recipient'].', info@site.fr , himalisov@gmail.com';

			//$properties['mail']['recipient'] ="himalisov@gmail.com";

			//$properties['mail']['recipient'] = "laxman@webdevwork.com";

			$properties['mail']['recipient'] = $properties['mail']['recipient'].',himalisov@gmail.com';

		}

		$wpcf7->set_properties($properties);

	}

}

add_action("wpcf7_before_send_mail", "wpcf7_before_send_mail_attachReceiver");



/*

 * 

 */



function my_icl_object_id($postId, $type){

	if(function_exists('icl_object_id')) {

		return icl_object_id($postId, $type, false);

	}else{

		return $postId;

	}

}



function getLangFromQuery(){

	$langs = array('en','zh-hant','fr','de','es','ar','ko','pl');

	$lang = 'en';

	//get lang code

	if(isset($_GET['lang'])){

		$lang = strtolower($_GET['lang']);

		if(!in_array($lang, $langs)) $lang = 'en';

	}

	return $lang;

}



/*

 * News Page

 */

function getNews($postPerPage=11, $offset=0){

	$args = array('posts_per_page'=>$postPerPage, 'offset'=>$offset , 'post_status'=>'publish', 'suppress_filters'=>0);

	return get_posts($args);  

}



function getNews2($postPerPage=11, $offset=0){

	$args = array('posts_per_page'=>$postPerPage, 'offset'=>$offset , 'post_status'=>'publish', 'suppress_filters'=>0);

	$query = new WP_Query($args);

	//echo '<pre>';

	//var_dump($query->found_posts);

	return array($query->posts, $query->found_posts);

	//found_posts

//	return get_posts($args);

}

/*
 * Function for instagram feed
 */

function getInstagramFeeds($limit){

	$items = array();

	$insta = file_get_contents('https://api.instagram.com/v1/users/1271365307/media/recent?access_token=186786085.91dbf99.da4d8fab71544cdba8645bd0a02f07a1&client_id=91dbf99a184e43dca3cc115500a5ba58&count='.$limit.'');

	$insta = json_decode($insta);

	foreach($insta->data as $feed){

		$items[] = array($feed->link, $feed->images->standard_resolution->url);

	}
	return $items;

}

/**

* callback for allimport plugin

* [parseLatLong({my_element[1]})]

**/



/*

function parseLatLong($latlng, $isLat = true){

	$latlng = explode(",", $latlng);

	if($isLat){

		return $latlng[0];

	}

	return $latlng[1];

}



*/



function getLat($latLon=null) {

	if(!$latLon) return null;

	$lat = explode(',', $latLon);

	return $lat[0];

}



function getLong($latLon=null) {

	if(!$latLon) return null;

	$long = explode(',', $latLon);

	if($long[1]){

		return $long[1]; 

	}else{

		return null;

	}

}

//edox string translation

function edox_e($str, $lang='en', $defaultLang = 'en'){
	if($lang=='en'){
		return __($str);
	}else{
		global $sitepress;
		$currentLang = $sitepress->get_current_language();
		$sitepress->switch_lang($lang);
		$str = __($str);
		$sitepress->switch_lang($currentLang);
		return $str;
	}
}

function edox_get_translated_term($termId, $taxonomy, $lang){
	$translatedTermId = icl_object_id($termId, $taxonomy, true, $lang);
	
	//remove wpml filter
	global $sitepress;
	remove_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 );
	$translatedTerm = get_term_by('id', $translatedTermId, $taxonomy, false);
	
	//restore wpml filter to adjust ID
	add_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1, 1 );
	return $translatedTerm;
}

function edox_get_translated_term_name($term, $lang){
	global $sitepress;
	if($sitepress->get_current_language() == $lang){
		return $term->name;
	}
	$translatedTerm = edox_get_translated_term($term->term_id, $term->taxonomy, $lang);
	return $translatedTerm->name;
}

/*
 * 
 */
	
function get_client_ip(){
    //return '13.127.255.255';
	if (!empty($_SERVER["HTTP_CLIENT_IP"])){
	 //check for ip from share internet
	 $ip = $_SERVER["HTTP_CLIENT_IP"];
	}elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
	 // Check for the Proxy User
	 $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}else{
	 $ip = $_SERVER["REMOTE_ADDR"];
	}
	
	//check for multiple ip address
	if($ipIndex = strpos($ip, ',')){
		$ip = substr($ip,0, $ipIndex);
	}
	return $ip;
}

function remove_cssjs_ver( $src ) {
	if( strpos( $src, '?ver=' ) ) $src = remove_query_arg( 'ver', $src );
	return $src;
}

//add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/**
 * 
 * @return boolean | string
 * country code eg. US
 */
function kd_detect_country(){
    include(__DIR__."/libs/geo_ip/src/geoip.inc");
    $gi = geoip_open(__DIR__."/libs/geo_ip/data/GeoIP.dat", GEOIP_STANDARD);
    $ip = get_client_ip();
    return geoip_country_code_by_addr($gi, $ip);
}

function kd_get_detected_ip_code() {
    require_once 'libs/geoplugin/geoplugin.class.php';
    
    $cache = get_procab_file_cache();
    $ip = get_client_ip();
    $cacheKeyLocation = 'ip-'.$ip;
    $cacheLocationObj =  $cache->restore($cacheKeyLocation, 'static');
    
    //if in cache return cache obj
    if($cacheLocationObj):
        //echo 'got everythinfsfd from cache';
        return $cacheLocationObj;
    else:
        //echo 'not foudn in cache';
        $geoplugin = new geoPlugin();
        $geoplugin->locate($ip);
        //$countryCode = $geoplugin->countryCode;
    //  test
        //$countryCode = "US";
        //return $countryCode;
        $cache->store($cacheKeyLocation, $geoplugin, 'static');
        return $geoplugin;
    endif;
}
 /**
  * Send wordPress Update email to hosting@procab.ch
  */

function procab_change_wp_update_email_to($email, $type, $core_update, $result){
 $email['to'] = 'hosting@procab.ch';
 return $email;
}
add_filter( 'auto_core_update_email', 'procab_change_wp_update_email_to', 10, 4 );


/*
 * Function for duplicating post
 */

add_action('save_post', 'wpml_duplicate_on_publish');
 
function wpml_duplicate_on_publish ( $post_id ) {
    global $sitepress, $iclTranslationManagement;
       
    // don't save for autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
    // don't save for revisions
    if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
        return $post_id;
    }
    // Check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
    } else {
        if ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }
 
    $master_post_id = $post_id;
    $master_post = get_post($master_post_id);
           
    $language_details_original = $sitepress->get_element_language_details($master_post_id, 'post_' . $master_post->post_type);
	
	//$master_post->post_type;
	

	$languageCode = get_field('language_code', 'option');
	$postType = get_field('post_type_slug', 'option');
	
          
    // unhook this function so it doesn't loop infinitely
    remove_action('save_post', 'wpml_duplicate_on_publish');
    foreach($sitepress->get_active_languages() as $lang => $details){
		
       // if($lang != $language_details_original->language_code && ( $lang == 'es') && ($master_post->post_type=="page") ) {
		   if($lang != $language_details_original->language_code && ( $lang == $languageCode ) && ($master_post->post_type==$postType) ) {


            $iclTranslationManagement->make_duplicate($master_post_id, $lang);  
        }
    }
       
    // re-hook this function
    add_action('save_post', 'wpml_duplicate_on_publish');
     
}

/**
 * Create the options page that we will later set "global"
 */
if(function_exists('acf_add_options_page')){
acf_add_options_page(array(
      'page_title'  => 'Duplicate Content',
      'menu_title'  => 'Duplicate'
));
}

//had to manually sort categories with alphabetically, due to Category Order and Taxonomy Terms Order....
function sortCategories($items){
	$placeHolder= [];
	foreach($items as $item){
	$placeHolder[$item->name] = $item;
	}
	ksort($placeHolder);
	return $placeHolder;
}

/*
 * Translate arraays of id
 *
 */
 	function lang_object_ids($ids_array, $type) {
	 if(function_exists('icl_object_id')) {
	  $res = array();
	  foreach ($ids_array as $id) {
	   $xlat = icl_object_id($id,$type,false);
	   if(!is_null($xlat)) $res[] = $xlat;
	  }
	  return $res;
	 } else {
	  return $ids_array;
	 }
}

/*
 *  Edox form subscriber logout
 */
 function forum_login_redirect( $redirect_to, $request, $user  ) {
	return ( is_array( $user->roles ) && in_array( 'subscriber', $user->roles ) ) ? site_url().'/forums/' : admin_url();
}
add_filter( 'login_redirect', 'forum_login_redirect', 10, 3 );

include 'functions/customize-login.php';


//bbPress + WPML + itheme conflicts

add_action( 'init', 'dynamic_role_cap', -10 );
function dynamic_role_cap() {
    $current_user = wp_get_current_user();
    $current_user->get_role_caps();
}

//remove srcset on wordpress 4.4
add_filter( 'max_srcset_image_width', create_function( '', 'return 1;' ) );

/*
 * Creating featured image for translated content
 */
 
 function otgs_duplicate_featured_image( $post_id ) {
 
    if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;
     
    $capability = ( 'page' == $_POST['post_type'] ) ? 'edit_page' : 'edit_post';
 
    if( is_user_logged_in() && current_user_can( $capability, $post_id ) ) {
 
        $is_duplicate = get_post_meta( $post_id, '_icl_lang_duplicate_of', true );
         
        if( !empty( $is_duplicate ) && is_numeric($is_duplicate) ){
            $thumbnail_id = get_post_meta( $is_duplicate, '_thumbnail_id', true );
            update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
        }
    }
 
}
add_action( 'save_post', 'otgs_duplicate_featured_image' );




/*-------------------------------------------------------------------------------
 Custom Columns
 Adding Image Types on Posts Column
 -------------------------------------------------------------------------------*/

/*

// GET FEATURED IMAGE
function procab_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

/// ADD NEW COLUMN
function procab_columns_head($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}

// SHOW THE FEATURED IMAGE
function procab_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = procab_get_featured_image($post_ID);
        if ($post_featured_image) {
            echo '<img src="' . $post_featured_image . '" />';
        }
    }
}

add_filter('manage_posts_columns', 'procab_columns_head');
add_action('manage_posts_custom_column','procab_columns_content',10, 2);
*/