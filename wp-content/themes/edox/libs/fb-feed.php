<?php
require_once __DIR__ . '/facebook-php-sdk-v4/src/Facebook/autoload.php';
require_once 'Date_HumanDiff/src/Date/HumanDiff.php';


define('APP_ID', '543683432452714');
define('APP_SECRET_ID', '99ca2be063a9e9b0b372eda0b7a91a98');


function getPostHTML($post, $dh){

	$desc = '';
	if(isset($post['description'])) $desc = substr($post['description'],0,100).'...';
	if(isset($post['message'])) $desc = substr($post['message'],0,100).'...';

	?>
	<li class="dcsns-li dcsns-facebook dcsns-feed-0" >
	<a href="<?=$post['link']?>">
		<img src="<?=$post['full_picture']?>" alt="" class="img-responsive"></a>
          <div class="inner"><span class="section-thumb"></span><span class="section-text"><?=$desc?></span><span class="clear"></span></div>
          <span class="section-intro"><a href="<?=$post['link']?>">Posted <?=@$dh->get($post['created_time'])?></a> </span></li>
	<?php
}

function getFacebookFeeds($limit){
	$fb = new Facebook\Facebook(array(
		'app_id' => APP_ID,
		'app_secret' => APP_SECRET_ID,
		'default_graph_version' => 'v2.2',
	));
	$dh = new Date_HumanDiff();
	
	try {
		$response = $fb->get('/156587182819/posts?fields=full_picture,link,created_time,message,description&limit='.$limit.'', APP_ID.'|'.APP_SECRET_ID);
		$posts = $response->getDecodedBody()['data'];
		
		//foreach($posts as $post):
			
		//endforeach;
?>
<?php if($limit == 6): ?>
<div class="dcsns-content">
<ul class="feed-ul">
    <li class="first_colmn" data-type="sprite" data-speed="0.2">
	    <ul class="stream">
        	<?php if(!empty($posts[0])) echo getPostHTML($posts[0], $dh); ?>
        	<?php if(!empty($posts[1])) echo getPostHTML($posts[1], $dh); ?>
	    </ul>
    </li>
    
    <li class="first_colmn" data-type="sprite" data-speed="0.6">
	    <ul class="stream">
        	<?php if(!empty($posts[2])) echo getPostHTML($posts[2], $dh); ?>
        	<?php if(!empty($posts[3])) echo getPostHTML($posts[3], $dh); ?>
	    </ul>
    </li>
    
    <li class="first_colmn" data-type="sprite" data-speed="0.4">
	    <ul class="stream">
        	<?php if(!empty($posts[4])) echo getPostHTML($posts[4], $dh); ?>
        	<?php if(!empty($posts[5])) echo getPostHTML($posts[5], $dh); ?>
	    </ul>
    </li>
</ul>
</div>
<?php else: ?>
<div class="dcsns-content">
  <ul class="feed-ul">
    <li class="first_colmn" data-type="sprite" data-speed="0.2" >
      <ul class="stream">
       <?php if(!empty($posts[0])) echo getPostHTML($posts[0], $dh); ?>
      </ul>
    </li>
  </ul>
</div>
<?php endif; ?>
<?php	

	}catch(Facebook\Exceptions\FacebookSDKException $e) {
		
	exit;
	}
}
$limit = (!isset($_GET['more'])) ? 1 : 6;
getFacebookFeeds($limit);