<?php
/**
 * The sidebar containing the secondary widget area
 *
 * Displays footer
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Gilles
 * @since Gilles 1.0
 */
	
		if ( is_active_sidebar( 'sidebar-1' ) ) : 
			dynamic_sidebar( 'sidebar-1' );
		endif;
?>