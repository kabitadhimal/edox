<?php
/*
  * Template Name: News Template
 *
 * */
 
function wpml_count_posts( $lang = 'all', $type = 'posts' ) {
 
    $_icl_cache = get_option( '_icl_cache' );
 
    if( ! empty( $_icl_cache ) && isset( $_icl_cache[$type.'_per_language'][$lang] ) ) {
        return $_icl_cache[$type.'_per_language'][$lang];
    }
}

?>
<?php get_header();?>
<div class="header-section content-section bg-blue text-center">
<div class="bg-line">
<h1><?php the_title();?></h1>
<h2><?php the_field('subtitle');?></h2>
</div>
</div>   
<!-- section -->
 <div class="news-section news_page">
    <div class="row" id="news-container">

    <style>
*{
	box-sizing:border-box;
}
.box-sqr{
	position:relative;
	
	position:relative; overflow:hidden;  /*text-align:center; color:white; font-size:10px;*/
	width:33.33333333333333%; float:left;
}
.box-sqr:before{
	content: '';
	display: inline-block;
	padding-top: 100%;
}
.box-sqr > div{
	vertical-align: middle;
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0; /*background-color:black;*/
}
.box-sqr > div:before{
	content: '';
	display: inline-block;
	vertical-align: middle;
	/* width: 0; */
	height: 100%;
}
.box-sqr > div > div{
	display: inline-block;
	vertical-align: middle;
	width: 100%;
}
.box-sqr > figure{
	position:absolute; top:0; left:0; right:0; bottom:0;
}
.box-sqr > figure img{
	width:100%; height:auto;
}
.box-sqr > figure + div{
	visibility:hidden;
	opacity:0;
	/*-webkit-transition:opacity 0.3s linear;*/
}
.box-sqr:hover > figure + div{
	visibility:visible;	
	opacity:1;
	transition:all 0.5s ease-in-out 0s;
	-moz-transition:all 0.5s ease-in-out 0s;
	-webkit-transition:all 0.5s ease-in-out 0s;
	-ms-transition:all 0.5s ease-in-out 0s;
}
.box-sqr-thumb > div{
	background-color:rgba(0,0,0,0.6);	
}

@media (max-width:767px){
	.box-sqr{
		width:50%;
	}
}
</style>
    	<?php
    	$cache = get_procab_file_cache();
    	$cacheNewsKey = $cache->buildKey('news');
    	$cacheNewsData = $cache->restore($cacheNewsKey);
    	if($cacheNewsData): echo $cacheNewsData;
    	else:
    	   $cache->captureStart($cacheNewsKey);
    	?>
		<?php
			$newsPosts = getNews2(5);
			$newsItems = $newsPosts[0];//getNews(5);
			foreach($newsItems as $news):
				setup_postdata( $GLOBALS['post'] =& $news );
				get_template_part('partial/news');
			endforeach;
			wp_reset_postdata();
         ?>
        <div class="box-sqr box-sqr-thumb" id="loadMore" >
					<div class="gred_block"><div class="box-sqr-content" >
                        <div class="caption text-center">
                              <h3><?php 	the_field('click_news_title');?></h3><p><?php the_field('click_news_subtitle');?></p>
                              <div class="news-rollover">
	                        <a href="" data-text="<?php the_field('click_news_button_name');?>"  class="btn btn-primary" >
                                <?php the_field('click_news_button_name');?>
                            </a>
                            </div>
                          </div>
					</div>
                    </div>
				</div>
				
			<script>
        	var totalNewsItems = <?=$newsPosts[1];?>;
        	var isBusy = false;
        	jQuery('.caption a.btn-primary').click(function(event){
        		event.preventDefault();
        		if(isBusy) return;
        		isBusy = true;
        		var offset = jQuery('#news-container .box-sqr').length-1;
        		//http://api.jquery.com/jQuery.get/
        		jQuery.get(window.ajaxURL+'?action=edox_news&lang=<?=ICL_LANGUAGE_CODE?>&offset='+offset, function(data){
        			jQuery('#news-container').append(data);
        			//check if last news items is retrieved!
        			var offset = jQuery('#news-container .box-sqr').length-1;
        			if(offset >= totalNewsItems){
        				jQuery('#loadMore').remove();
        			}else{
        				jQuery('#loadMore').appendTo(jQuery('#news-container'));
        			}
        			isBusy = false;
        		});
        	});
        	</script>
			
		<?php
		  echo $cache->captureEnd($cacheNewsKey);
		  endif;
		?>
        
    </div>
  </div>
  <!-- social -->
<div class="social text-center social_stream">
    <div class="wrapper">
      <h4><?php _e("CONNECT WITH US &amp; share"); ?></h4>
      <h2><?php _e("FOLLOW US"); ?></h2>
          <section class="social-contr social-inner">
            <div id="facebook_section" class="feed-blk section-para">
                <a href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819" class="icon-large icon-large-fb"></a>
				<div class='fb-posts dcsns' style='position:relative;'></div>
              </div>
             <div id="twitter_section" class="feed-blk section-para">
              <a href="http://twitter.com/edoxwatches" class="icon-large icon-large-twitter"></a>
            </div>
            <div id="instagram_section" class="feed-blk section-para dcsns" >
              <a href="http://instagram.com/edoxswisswatches" class="icon-large icon-large-insta"></a>
              
<?php 
$cacheInstaKey = 'insta-news';
$cacheInstaData = $cache->restore($cacheInstaKey, 'static');
//$cacheInstaData = false;
if($cacheInstaData): echo $cacheInstaData;
else:
    $cache->captureStart($cacheInstaKey, 'static', 259200); //3days
?>
              
<?php $instaItems = getInstagramFeeds(6);
function getInstaHtml($instaItem){
	return '<li class="dcsns-li dcsns-instagram dcsns-feed-0"><div class="inner"><span class="section-thumb"><a href="'.$instaItem[0].'" target="_blank"><img src="'.$instaItem[1].'" alt=""></a></span><span class="clear"></span></div></li>';
}
 ?>
  <div class="dcsns-content">
<ul class="feed-ul">
    <li class="first_colmn" data-type="sprite" data-speed="0.2">
	    <ul class="stream">
        	<?php if($instaItems[0]) echo getInstaHtml($instaItems[0]); ?>
        	<?php if($instaItems[1]) echo getInstaHtml($instaItems[1]); ?>
	    </ul>
    </li>
    
    <li class="first_colmn" data-type="sprite" data-speed="0.6">
	    <ul class="stream">
        	<?php if($instaItems[2]) echo getInstaHtml($instaItems[2]); ?>
        	<?php if($instaItems[3]) echo getInstaHtml($instaItems[3]); ?>
	    </ul>
    </li>
    
    <li class="first_colmn" data-type="sprite" data-speed="0.4">
	    <ul class="stream">
        	<?php if($instaItems[4]) echo getInstaHtml($instaItems[4]); ?>
        	<?php if($instaItems[5]) echo getInstaHtml($instaItems[5]); ?>
	    </ul>
    </li>
</ul>
</div>
<?php 
    echo $cache->captureEnd($cacheInstaKey);
endif;
?>
            </div>
        </section>
    </div>
</div>


<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/social/jquery.social.stream.wall.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/social/jquery.social.stream.1.5.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/js/social-parallax.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/js/social-feeds.js"></script>
<script>
	var para = new parallax();
	//loadFbFeed($('#facebook_section'), 6);
	$('#facebook_section .fb-posts').addClass('dcsns').load(themeUrl+'/libs/fb-feed.php?more');
	loadTwitterFeed($('#twitter_section'), 7);
	//loadInstaFeed($('#instagram_section'), 6);

</script>
<!-- social -->
<?php get_footer();
