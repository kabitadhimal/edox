<?php

 /* Template Name: Pont of Sale Template
 *
 * */ 
get_header();
?>
<?php
$cache = get_procab_file_cache();
$cacheStoresKey = $cache->buildKey('stores');
$cacheStoresData = $cache->restore($cacheStoresKey);
if($cacheStoresData): echo $cacheStoresData;
else:
    $cache->captureStart($cacheStoresKey);
?>
<!-- HEADER STARTS -->

    <header class="row-wrap bg-blue text-center content-section filter-header"><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <div class=" wrap">
            <h1><?php the_title(); ?></h1>
			<?php 
			global $sitepress;
			$currentLang = $sitepress->get_current_language();
			$sitepress->switch_lang('en');
			$storeCategories = get_terms( 'category-stores' );
			?>
			
            <div class="search-fields" >
                <select id="countries" name="countries" >
					<option value="-1"><?=edox_e('select country', $currentLang);?></option>
                 <?php 
	                $args = array(
	                		//'type'                     => 'post',
							'orderby'          		   => 'name', 
   							'order'            		   => 'ASC',
	                		'child_of'                 => 0,
	                		'parent'				   => 0,
	                		'hide_empty'               => 0,
	                		'hierarchical'             => 0,
	                		'taxonomy'                 => 'countries'//,
	                		//'suppress_filters' => false
	                );
					$categoriesT = get_categories( $args );
					$categories = sortCategories($categoriesT);
					foreach($categories as $category) {
					//var_dump($category);
						echo '<option data-lat-long="'.get_field('lat_long', "countries_".$category->term_id).'" value="' . $category->term_id . '">' . $category->name.'</option>';
						}
					?>
                </select>
                <select id="cities"></select>
                <div class="search">
                    <a class="icon" id="filter_search" style="cursor:pointer" ></a>
                </div>   
                <div id="city-search" class="city-search-field">
                    <input type="text" id="city-search-input" class="form-control" placeholder="Eg. Geneva" />
                </div>
                <div id="filter-error"><small>0 results found!</small></div>
            </div>
        </div>
        <div class="filter-loader"></div>
    </header>      

<!-- HEADER ENDS -->
    <section>
                        <div id="city_map_address" class='hidden-xs'></div>
                        <div class="filter cityPoint_navigator">
                            <div class=" wrap">
                                <ul>
                                <?php 
								$termIndex = 0; 	//termIndex is set to syn the icon image of filter, markers and store sections
								foreach($storeCategories as $storeCategory):
								?>
                                    <li class="boutiques-check col-sm-6">
                                        <label for="store_category_<?=$storeCategory->term_id?>"><?=edox_get_translated_term_name($storeCategory, $currentLang)?>
                                        <input type="checkbox" data-index='<?=$termIndex++?>' value="<?=$storeCategory->term_id?>" id="store_category_<?=$storeCategory->term_id?>" name="store_category_<?=$storeCategory->term_id?>" class="darkblue" />
                                        </label>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>    
                        </div>
                    </section><!--/.type-map-->
    <div class="store-address">
    </div>
</div><style>
.show-top-four > .sm-block:nth-child(n+5) {
	display:none;
	/*background-color:green;*/
}
</style>
<?php
    echo $cache->captureEnd($cacheStoresKey);
endif;
?>

<?php 
global $currentLocation;
$currentLocation = kd_get_detected_ip_code();

$userCountry = $currentLocation->countryName;
$userLatitude = $currentLocation->latitude;
$userLongitude = $currentLocation->longitude;

/*
$ipResult = @json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip='.get_client_ip().''));
//$ipResult = @json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip=178.174.39.224'));
//$ipResult = @json_decode(file_get_contents('http://www.geoplugin.net/json.gp?ip=202.166.198.75'));
if($ipResult && $ipResult->geoplugin_countryName != null){
	$userCountry = strtolower($ipResult->geoplugin_countryName);
	
	$userLatitude = $ipResult->geoplugin_latitude;
	$userLongitude = $ipResult->geoplugin_longitude;
	//$userCountry = 'france';
}
//$userCountry = 'france';
*/


?>
<script>
var visitorCountry = '<?=$userCountry?>';
var visitorCountryLat = '<?=$userLatitude?>';
var visitorCountryLong = '<?=$userLongitude?>';
var currentLang = '<?=ICL_LANGUAGE_CODE?>';
</script>


<?php /*<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script> */ ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/markerclusterer_compiled.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/map.js'></script>
<script>

//if(isCountryFound == false) getContentByCountry(-1);

</script>

<script type="text/javascript"> 
var axel = Math.random() + ""; 
var a = axel * 10000000000000; 
document.write('<iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxs0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>'); 
</script> 
<noscript> 
<iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxs0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe> 
</noscript> 
<?php
$sitepress->switch_lang($currentLang);
get_footer();?>