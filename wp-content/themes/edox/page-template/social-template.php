<?php
/**
 * Template Name: Social Template
 *
 * The template for World of shoes
 *
 * @package WordPress
 * @subpackage Bata
 * @since Bata 1.0
 */
 get_header();
 ?>
 <script>

var ua = navigator.userAgent;

var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);

</script>
 <script type='text/javascript' src='http://www.bata.com/wp-content/themes/newbata/js/jquery.social.stream.wall.1.3.js?ver=1.3'></script>
<script type='text/javascript' src='http://www.bata.com/wp-content/themes/newbata/js/jquery.social.stream.1.5.4.min.js?ver=1.5.4'></script>
<link rel='stylesheet' id='dcsns-css'  href='http://www.bata.com/wp-content/themes/newbata/css/dcsns_wall.css?ver=3.3' type='text/css' media='all' />

<?php 
	$cache = get_procab_file_cache();
	$cacheSocialKey = $cache->buildKey('social');
	$cacheSocialData = $cache->restore($cacheSocialKey);
	if($cacheSocialData) : echo $cacheSocialData;
	else:

	?>
        <div id="content_wrapper">
        	<!-- Facebook Section -->
           <section id="facebook_section">
           		<div class="content">
                	<h1>Social Bata</h1>
                    <header><em></em><a href="https://www.facebook.com/Bata?brandloc=DISABLE" target="_new" >follow us on facebook<span></span></a></header>
                    <div id="facebook_feed_blk" class="social_blks_contr"></div>
                	<img src="<?php echo get_template_directory_uri(); ?>/images/social_bata_parallax_obj1.png" class="parallax_obj obj1 hidden-phone"  data-type="parallaxObj" data-y='-21' data-speed='2' />
                </div>
           </section> 
           <img src="<?php echo get_template_directory_uri(); ?>/images/social_fb_bg.jpg" id="social_fb_bg" class="social_bg hidden-phone" />
           
           <!-- Pinterest Section -->
           <section id="pinterest_section">
           		<div class="content">
					<header><em></em><a href="http://www.pinterest.com/batashoes/" target="_new">join us on pinterest<span></span></a></header>
                    <div id="pinterest_bataheritage_blk" class="social_blks_contr"></div>
                    <div id="pinterest_batastyle_blk" class="social_blks_contr"></div>
                </div>
           </section>
           <img src="<?php echo get_template_directory_uri(); ?>/images/social_pinterest_bg.png" id="social_pinterest_bg" class="social_bg hidden-phone" />
           
           <!-- Tweeter Section -->
           <section id="tweeter_section">
           		<div class="content">
                	<header><em></em><a href="https://twitter.com/batashoes" target="_new">our latest tweets<span></span></a></header>
                    <div id="twitter_feed_blk" class="social_blks_contr"></div>
                </div>
           </section>
           <img src="<?php echo get_template_directory_uri(); ?>/images/social_tweeter_bg.jpg" id="social_tweeter_bg" class="social_bg hidden-phone" />
           
           <!-- Instagram Section -->
           <section id="instagram_section">
           		<div class="content">
                	<header><em></em><a href="http://instagram.com/batashoes" target="_new">Discover #batashoes<span></span></a></header>
                   	<div id="instagram_feed_blk" class="social_blks_contr"></div>
                </div>
           </section>
           <img src="<?php echo  get_template_directory_uri(); ?>/images/social_instagram_bg.png" id="social_instagram_bg" class="social_bg hidden-phone" />
		</div>
        <div id="sectionPagination" class="hidden-phone">
            <div class="hPaginCont">
                <ul>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
                <div class="activePaginBG"></div>
            </div>
        </div>
        
        <div id="join_the_club">
        	<div class="content">
            	<h2>Join the club</h2>
                <ul class="social_btns">
                	<li class="fb"><a href="https://www.facebook.com/Bata?brandloc=DISABLE " target="_new"></a></li>
                    <li class="tweeter"><a href="https://twitter.com/batashoes" target="_new"></a></li>
                    <li class="instagram"><a href="http://instagram.com/batashoes" target="_new"></a></li>
                    <li class="pinterest"><a href="http://www.pinterest.com/batashoes/" target="_new"></a></li>
                    <li class="tumblr"><a href="http://batashoes.tumblr.com/" target="_new"></a></li>
                    <li class="gplus"><a href="https://plus.google.com/+batashoes/posts" target="_new"></a></li>
                </ul>
                <div class="social_bata_logo"></div>
            </div>
        </div>
       
       <?php 
	   			
				$cache->captureStart($cacheSocialKey);
				echo $cache->captureEnd($cacheSocialKey);
			endif;
	
	   ?>
        <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/parallax.js"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/socialSectionPagination.js"></script>
        <script>
			window.themeUrl = "<?php echo get_template_directory_uri(); ?>";
		</script>
        <?php /*<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/socialpageScript.min.js"></script>*/ ?>
        <script type="text/javascript" src="<?=get_template_directory_uri()?>/js/socialpageScript.js"></script>        
        
        <!--<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>-->

    <?php get_footer(); ?>
