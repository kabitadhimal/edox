<?php
/*
  * Template Name: Edox Universe Template
 *
 * 
 * */
get_header();
?>
 <script type="text/javascript">
	$(document).ready(function() {
		
		$('.fancybox').fancybox();
	});
</script> 
    
  <div class="partners text-center bg-blue">
  
  	<?php
    	$cache = get_procab_file_cache();
    	$cacheUniverseKey = $cache->buildKey('universe');
    	$cacheUniverseData = $cache->restore($cacheUniverseKey);
    	if($cacheUniverseData): echo $cacheUniverseData;
    	else:
    	   $cache->captureStart($cacheUniverseKey);
    	?>
        
  
    <div class="header-section">
      <div>
        <h1> <?php echo get_field('partnership_title', $post->ID); ?></h1>
        <h2><?php the_field('partnership_subtitle',$post->ID)?></h2>
      </div>
    </div>
    
    <div class="row">
      <div class="partnership-rollover">
      <?php 
      $i=1;
      $query = new WP_Query('post_type=partnership&suppress_filters=0&post_status=publish');
      while ($query->have_posts()):$query->the_post();
      
      if($i==5) {
      	$i=1;
      }
      
      $content = strip_tags(get_the_content());
     
      if($i==1 || $i==2):
      ?>
       <div class="col-sm-6 partnership-content">
        <div class="rollover-box slideRight"> <a href="<?php the_permalink();?>">
          <div class="partner-block"><h2><?php the_title();?></h2></div>
          </a>
          <div class="box-content">
            <div class="box-caption">
			<?php the_field('short_description');?>  
            </div>
          </div>
        </div>
       
        <?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'partnership-img' ); ?>
        <div class="rollover-box"> <img class="img-responsive" src="<?php echo $full_image_url[0] ?>" alt="partner"> </div>
        </div>
       <?php endif;?>
       
       
       <?php  if($i==3 || $i==4):?>
       
       <div class="col-sm-6 partnership-content">        
        <?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'partnership-img' ); ?>
        <div class="rollover-box"> <img class="img-responsive" src="<?php echo $full_image_url[0] ?>" alt="partner"> </div>
        
        <div class="rollover-box slideLeft"> <a href="<?php the_permalink();?>">
          <div class="partner-block"><h2><?php the_title();?></h2></div>
          </a>
          <div class="box-content">
            <div class="box-caption">
				<?php the_field('short_description');?>             
			</div>
          </div>
        </div>
        </div>
        <?php endif;
		      $i++;
		      endwhile;
		      wp_reset_postdata();
      		?>
        
      </div>
    </div>    
  </div>

    <?php
    echo $cache->captureEnd($cacheUniverseKey);
endif;
    ?>
  <!---end of partnership-->
<!--  <div class="vip-container text-center">
    <div class="header-section vip-header-section">
      <div>
         <h1> <?php /*echo get_field('vip_title', $post->ID); */?></h1>
        <h2><?php /*the_field('vip_subtitle',$post->ID)*/?></h2>
      </div>
    </div>
   
        <div id="vip-thumbs" class="vip-thumbs">
        <?php
/*        $args = array('post_type'=>'vip', 'posts_per_page'=>-1,'orderby'=>'date','order'=>'DESC','suppress_filters'=>0);//, 'orderby'=>'date','order'=>'DESC');
        $vipPosts = get_posts($args);
        $vipPostsDateAry = array();
        foreach ($vipPosts as $vip){
        	$postYear =  strtok($vip->post_date, '-');
        	if(!isset($vipPostsDateAry[$postYear])) $vipPostsDateAry[$postYear] = array();
        		$vipPostsDateAry[$postYear][] = $vip;
        }
		foreach ($vipPostsDateAry as $year=>$vipAry){
			foreach ($vipAry as $vip):
			setup_postdata( $GLOBALS['post'] =& $vip );
			*/?>
			<div class="col-xs-6 col-sm-6 col-md-3 vipRollover">
				<div class="vipSquare">
				<?php
/*				$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
					if(!empty($image[0])) :
				*/?>
					<img class="img-responsive" src="<?/*=$image[0]*/?>" />
				<?php /*endif;*/?>
					<div>
                    	<div class="vip-info">
                          <h2><?php /*the_title();*/?></h2>
                          <h3><?php /*the_field('vip_subtitle');*/?></h3>
                           <div class="vip-gallery">
							<?php /*
							
							$sliderId = rand();
							if( have_rows('vip_gallery') ):
								while ( have_rows('vip_gallery') ) : the_row();
								$imageID = get_sub_field('vip_image').'<br/>';
								$galleryImage = wp_get_attachment_image_src($imageID,'full');
								if(!empty($galleryImage[0])):
								*/?>
			                   <a class="fancybox" href="<?/*=$galleryImage[0]*/?>" data-fancybox-group="gallery[<?/*=$sliderId*/?>]" title=""><i class="fa fa-camera"></i></a>
							<?php /*
								endif;
								endwhile;
							else :
								echo '';
							endif;
						*/?>
						</div>
					</div>
				</div>
			  </div>
			</div>
		<?php /*
		endforeach;
	    }
		wp_reset_postdata();
        */?>
        
	</div>
    <?php /*
		  echo $cache->captureEnd($cacheUniverseKey);
		  endif;
	*/?>
  </div>-->

  <?php get_footer();
