<?php

/*
  * Template Name: Edox New Home Page Template
 *
 *
 *
 */
get_header();
?>
<?php include get_template_directory() . '/partial/home/slider.php'; ?>

<?php

global $storeCode;

    $cache = get_procab_file_cache();
    $cacheDiscoverKey = 'home-discover-'.ICL_LANGUAGE_CODE."-".$storeCode;
    $cacheDiscoverData = $cache->restore($cacheDiscoverKey, 'static');
    if($cacheDiscoverData): echo $cacheDiscoverData;
    else:
    $cache->captureStart($cacheDiscoverKey, 'static', 259200); //3days

    $disoverCollectionList = site_url('/').discoverCollectionUrl($storeCode);
    $discoverCollectionContent = @file_get_contents_curl($disoverCollectionList);

    echo $discoverCollectionContent;
    ?>

<?php    echo $cache->captureEnd($cacheDiscoverKey); endif;?>
    <?php
    //discover collection form Magento
   ;
  
/*     if($_GET['test']) {
     echo  'I am discover'.$disoverCollectionList;
 }
 
    $cache = get_procab_file_cache();
	//$magentoDiscoverCollectionKey = 'megento-disover-collection-'.$disoverCollectionList;
    $magentoDiscoverCollectionKey = $cache->buildKey('megento-disover-collection');
	$magentoCollectionData = $cache->restore($magentoDiscoverCollectionKey, 'static');
	if($magentoCollectionData): echo $magentoCollectionData;
    else:
        $cache->captureStart($magentoDiscoverCollectionKey, 'static');*/

   /*     echo $cache->captureEnd($magentoDiscoverCollectionKey);
    endif; // end of magento menu cache build*/

    ?>

    <?php include get_template_directory() . '/partial/home/watch-finder.php'; ?>
    <?php include get_template_directory() . '/partial/home/news-block.php'; ?>
    <?php include get_template_directory() . '/partial/home/store-locator.php'; ?>
    <?php include get_template_directory() . '/partial/home/about-us.php'; ?>

    <?php
    $cacheSocialResponsiveKey = 'home-social-responsive';
    $cacheSocialResponsiveData = $cache->restore($cacheSocialResponsiveKey, 'static');
    if($cacheSocialResponsiveData): echo $cacheSocialResponsiveData;
    else:
    $cache->captureStart($cacheSocialResponsiveKey, 'static', 259200); //3days
    ?>

    <div class="visible-xs social-mobile">
        <h2><a href="https://www.edox.ch/contact"><?php _e("Contact Us","edox"); ?></a></h2>
        <div class="twitter-mobile"><a target="_blank" href="https://twitter.com/edoxwatches"><i class="fab fa-twitter"></i></a></div>
        <div class="instagram-mobile"><a target="_blank" href="http://instagram.com/edoxswisswatches"><i class="fab fa-instagram"></i></a></div>
        <div class="youtube-mobile"><a target="_blank" href="https://www.youtube.com/user/EdoxWatches"><i class="fab fa-youtube"></i></a></div>
        <div class="fb-mobile"><a target="_blank" href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819"><i class="fab fa-facebook-square"></i></a></div>
        <div class="pinterest-mobile"><a target="_blank" href="http://www.pinterest.com/edoxwatches/"><i class="fab fa-pinterest-square"></i></a></div>
    </div>

        <?php
        echo $cache->captureEnd($cacheSocialResponsiveKey);
    endif;?>

    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxh0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
        <iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxh0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>
<?php get_footer(); ?>