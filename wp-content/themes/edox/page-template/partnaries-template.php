<?php
/*
  * Template Name: Partenaries Template
 *
 * 
 * */

 get_header(); ?>
<script>
jQuery('p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});
</script>  

 <div class="col-sm-12 col-md-9 main col-sm-offset-0 col-md-offset-3">
    <?php
		$cache = get_procab_file_cache();
		$cachePartnerKey = $cache->buildKey('partner');
		$cachePartnerData = $cache->restore($cachePartnerKey);
		if($cachePartnerData): echo $cachePartnerData;
		else :
			$cache->captureStart($cachePartnerKey);
	?>
      <div class="show_desktop">
      <?php 
      while (have_posts()):the_post();
		the_content();
		endwhile;
		wp_reset_query();
	 ?>
      </div>
      <div class="show_mobile">
      <?php 
      while (have_posts()):the_post();
		the_content();
		endwhile;
		wp_reset_query();
	 ?>
      </div>
      <?php 
	  echo $cache->captureEnd($cachePartnerKey);
	endif;
	  ?>
     <?php get_footer();?>
</div>
