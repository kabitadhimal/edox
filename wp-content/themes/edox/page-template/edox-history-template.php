<?php
/*
  * Template Name: History Test Template
 *
 * 
 * */
get_header();



$cache = get_procab_file_cache();
$cacheHistoryKey = $cache->buildKey('history');
$catchHistoryData = $cache->restore($cacheHistoryKey);

if($catchHistoryData): echo $catchHistoryData;
else :
	$cache->captureStart($cacheHistoryKey);


		
		$paths = array(
			realpath(dirname(__FILE__) . '/../library'),
			'.'
			);
		set_include_path(implode(PATH_SEPARATOR, $paths));
		
		include 'Zend/Dom/Query.php';
		
		$xmlPath = __DIR__.'/../xml/history_en.xml';
		
		
		if (ICL_LANGUAGE_CODE=='zh-hant') {
			$xmlPath = __DIR__.'/../xml/history_chinese.xml';
		}else if (ICL_LANGUAGE_CODE=='de') {
			$xmlPath = __DIR__.'/../xml/history_de.xml';
		}else if (ICL_LANGUAGE_CODE=='ru') {
			$xmlPath = __DIR__.'/../xml/history_russian.xml';
		}else if (ICL_LANGUAGE_CODE=='es') {
			$xmlPath = __DIR__.'/../xml/history_spanish.xml';
		}else if (ICL_LANGUAGE_CODE=='ar') {
			$xmlPath = __DIR__.'/../xml/history_arabic.xml';
		}else if (ICL_LANGUAGE_CODE=='ko') {
			$xmlPath = __DIR__.'/../xml/history_korean.xml';
		}else if (ICL_LANGUAGE_CODE=='pl') {
			$xmlPath = __DIR__.'/../xml/history_polish.xml';
		}else if (ICL_LANGUAGE_CODE=='fr') {
			$xmlPath = __DIR__.'/../xml/history_fr.xml';
                }else if (ICL_LANGUAGE_CODE=='bs') {
			$xmlPath = __DIR__.'/../xml/history_bosnian.xml';
		}else {
			$xmlPath = __DIR__.'/../xml/history_en.xml';
		}
		
		$html = file_get_contents($xmlPath);
		
		global $dom;
		$dom = new Zend_Dom_Query($html);
		
		function getContentByYear($year){
			global $dom;
			$story = $dom->query('story[year="'.$year.'"]');
			if($story != null) return $story->current()->nodeValue;
			return '';
		}



///$years = $dom->query('story');


//$year = $dom->query('story[year="1870"]');
//var_dump($year->current()->nodeValue);
//die();
/*
foreach($years as $year){
	var_dump($year->nodeValue);
}
*/

//die();

?>	
    <div class="timeline-container" style="position: relative;">
        <div class="sticky-dates" data-spy="affix" data-offset-top="100" data-offset-bottom="700">
            <ul class="sticky-navigation">
                <li><a href="#first-part" class="scroll active">1860</a></li>
                <li><a href="#1870" class="scroll">1870</a></li>
                <li><a href="#1883" class="scroll">1883</a></li>
                <li><a href="#second-part" class="scroll">1884</a></li>
                <li><a href="#third-part" class="scroll">1900</a></li>
                <li><a href="#fourth-part" class="scroll">1955</a></li>
                <li><a href="#fifth-part" class="scroll">1961</a></li>
                <li><a href="#fifth-part-one" class="scroll">1965</a></li>
                <li><a href="#sixth-part" class="scroll">1969</a></li>
                <li><a href="#seventh-part" class="scroll">1970</a></li>
                <li><a href="#eighth-part" class="scroll">1998</a></li>
                <li><a href="#eighth-part-one" class="scroll">2005</a></li>
                <li><a href="#ninth-part" class="scroll">2007</a></li>
                <li><a href="#tenth-part" class="scroll">2008</a></li>
                <li><a href="#eleventh-part" class="scroll">2010</a></li>
                <li><a href="#twelveth-part" class="scroll">2012</a></li>
                <li><a href="#thirteenth-part" class="scroll">2013</a></li>
                <li><a href="#end-part" class="scroll">2014</a></li>
            </ul>
        </div>
    
        <div class="timeline-wrapper " data-spy="scroll" data-target=".sticky-dates" style="position: relative;">
        <section>
        <div class="first-part target" id="first-part">
            <div class="history-top">
                <?=getContentByYear('intro')?>
            </div>
    
            <div class="timeline-start">
                <div id="1860" class="scroll year-point first">1860</div>
                <div id="1870" class="scroll year-point second target">1870</div>
                <div id="1883" class="scroll year-point third target">1883</div>
                <div class="timeline-content">
                    <div class="half">
                        <?=getContentByYear('1860')?>
                    </div><div class="half">
                    <img src="<?php echo get_template_directory_uri();?>/images/history/timeline-1.png">
                    </div>
                </div>
                <div class="timeline-fixed-content watchmaking">
                    <div class="cover-bg">
                        <?=getContentByYear('1870')?>
                    </div>
                </div>
                <div class="timeline-content marriage">
                    <div class="half">
                        <img src="<?php echo get_template_directory_uri();?>/images/history/pauline.png">
                    </div><div class="half">
                    <div class="timeline-boxed pauline">
                        <?=getContentByYear('1883')?>
                    </div>
                </div>
            </div>
        </div>
        <div class="parallaxObject item-1" data-type="parallaxObj" data-speed="0.2"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-1.png"></div>
        <div class="parallaxObject item-2" data-type="parallaxObj" data-speed="-0.4"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-2.png"></div>
        </div>
        </section>
    
        <section class="para_bg target" id="second-part">
        <div class="second-part parallax_bg " >
            <div class="timeline-content">
                <?=getContentByYear('1884')?>
            </div>
        </div>
        </section>
    
        <section class="target" id="third-part">
        <div class="third-part">
            <div class="timeline-start">
                <div id="1900" class="scroll year-point fourth">1900</div>
                <div class="timeline-content">
                    <div class="timeline-fixed-content birthof-logo">
                        <div class="cover-bg">
                            <?=getContentByYear('1900')?>
                        </div>
                    </div>
                    <div class="half logo os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s"><img src="<?php echo get_template_directory_uri();?>/images/history/logo-birth.png"></div><div class="half">&nbsp;</div>
                </div>
            </div>
            <div class="parallaxObject item-3" data-type="parallaxObj" data-speed="0.2"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-3.png"></div>
        </div>
        </section>
    
        <section class="para_bg target" id="fourth-part">
        <div class="fourth-part parallax_bg ">
            <div class="timeline-content">
                <?=getContentByYear('1955')?>
                <div class="parallaxObject item-4" data-type="parallaxObj" data-speed="-0.4"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-4.png"></div>
            </div>
        </div>
        </section>
    
        <section id="fifth-part" class="target">
            <div class="fifth-part">
                <div class="timeline-start">
                    <div id="1961" class="scroll year-point fourth">1961</div>
                    <!-- <div id="1963" class="scroll year-point fifth">1963</div> -->
                    <div class="timeline-content">
                        <div class="half crown">
                            <div class="timeline-boxed">
                                <?=getContentByYear('1961-box')?>
                                        <!--
                                        <p>Invention of the crown with double gasket for greater water resistance. Patended in 1961 by Edox in Bern, Switzerland.</p>
                                        <p>Nowadays this invention is still used by several major Swiss Watch brands.</p>
                                    -->
                                </div>
                                <img src="<?php echo get_template_directory_uri();?>/images/history/1961-watch.png" class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
                            </div><div class="half water-champion">
                            <div>
                                <img src="<?php echo get_template_directory_uri();?>/images/history/invention.png" class="os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0s">
                                <?=getContentByYear('1961')?>
                            </div>
                            </div>
                            <!-- <div class="half hydro-sub">
                                <div>
                                    <?=getContentByYear('1963')?>
                                </div>
                            </div><div class="half hydro-sub-watch">
                            &nbsp;
                            <img src="<?php echo get_template_directory_uri();?>/images/history/1963-watch.png" class="os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0s"> -->
                        </div>
                    </div>
                </div>	
        </section>
    
        <section id="fifth-part-one" class="target">
            <div class="fifth-part">
                <div class="timeline-start">
                    <div id="1963" class="scroll year-point fifth">1965</div>
                    <div class="timeline-content">
                        <div class="half hydro-sub">
                                <div>
                                    <?=getContentByYear('1963')?>
                                </div>
                            </div><div class="half hydro-sub-watch">
                            &nbsp;
                            <img src="<?php echo get_template_directory_uri();?>/images/history/1963-watch.png" class="os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0s">
                    </div>
                </div>
            </div>
        </section>
    
        <section class="para_bg target" id="sixth-part">
        <div class="sixth-part parallax_bg">
            <div class="timeline-content">
                <?=getContentByYear('1969')?>
            </div>
            <div class="parallaxObject item-5" data-type="parallaxObj" data-speed="0.2"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-5.png"></div>
        </div>
        </section>
    
        <section class="para_bg target" id="seventh-part">
        <div class="seventh-part parallax_bg ">
            <div class="timeline-content">
                <?=getContentByYear('1970')?>
            </div>
            <div class="parallaxObject item-6" data-type="parallaxObj" data-speed="-0.4"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-6.png"></div>
        </div>
        </section>
    
        <section class="target" id="eighth-part">
        <div class="eighth-part">
            <div class="timeline-start">
                <div id="1998" class="scroll year-point sixth">1998</div>
                <div class="timeline-content">
                    <div class="timeline-fixed-content worldrecord">
                        <div class="cover-bg">
                            <?=getContentByYear('1998')?>						<?php 													if(ICL_LANGUAGE_CODE=="fr") :								$link98= "http://www.edox.ch/shop/fr_fr/style-elegance/les-bemonts.html"; 							else : 								$link98 ="http://www.edox.ch/shop/en/style-elegance/les-bemonts.html"; 							endif; 												?>
                            <a href="<?php echo $link98; ?>"><?php _e("see the collection"); ?></a>
                        </div>
                    </div>				
                    <div class="half">&nbsp;</div><div class="half worldrecord-watch"><img src="<?php echo get_template_directory_uri();?>/images/history/1998-watch.png" class="os-animation" data-os-animation="fadeInRight" data-os-animation-delay="0s"></div>
                </div>
                <div class="parallaxObject item-7" data-type="parallaxObj" data-speed="-0.4"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-7.png"></div>
            </div>
        </div>
        </section>
    
        <section class="target" id="eighth-part-one">
        <div class="eighth-part">
            <div class="timeline-start">
                <div id="2005" class="scroll year-point seventh">2005</div>
                <div class="timeline-content">
                    <div class="timeline-fixed-content achivements">
                        <div class="cover-bg">
                            <?=getContentByYear('2005')?>
                        </div>
                    </div>					
                    <div class="half achivements-watch"><img src="<?php echo get_template_directory_uri();?>/images/history/2005-watch.png" class="os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s"></div><div class="half">&nbsp;</div>
                </div>
                <div class="parallaxObject item-8" data-type="parallaxObj" data-speed="0.2"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-8.png"></div>
            </div>
        </div>
        </section>
    
        <section class="para_bg target" id="ninth-part">
        <div class="ninth-part parallax_bg">
            <div class="timeline-content">
                <?=getContentByYear('2007');?>
            </div>
        </div>
        </section>
    
        <section class="para_bg target" id="tenth-part">
        <div class="tenth-part parallax_bg">
            <div class="timeline-content">
                <?=getContentByYear('2008')?>						<?php 			if(ICL_LANGUAGE_CODE=="fr") :				$link08= "http://www.edox.ch/shop/fr_fr/sporting-instruments/chronoffshore-1.html";			else :				$link08 ="http://www.edox.ch/shop/en/sporting-instruments/chronoffshore-1.html";			endif;			?>
                <a href="<?=$link08?>"><?php _e("see the collection"); ?></a>
                <img class="timekeeping-watch os-animation" src="<?php echo get_template_directory_uri();?>/images/history/timekeeping-watch.png" data-os-animation="fadeInRight" data-os-animation-delay="0s">
            </div>
        </div>
        </section>
    
        <section class="para_bg target" id="eleventh-part">
        <div class="eleventh-part parallax_bg">
            <div class="timeline-content">
                        <?=getContentByYear('2010-title')?>					<?php 					if(ICL_LANGUAGE_CODE=="fr") :						$link10= "http://www.edox.ch/shop/fr_fr/sporting-instruments/chronorally.html";					else :						$link10 ="http://www.edox.ch/shop/en/sporting-instruments/chronorally.html";					endif;					?>
                <a href="<?=$link10?>"><?php _e("see the collection"); ?></a>
                <img class="timekeeper-watch os-animation" src="<?php echo get_template_directory_uri();?>/images/history/timekeeper-watch.png" data-os-animation="fadeInRight" data-os-animation-delay="0s">
            </div>
            <!--<div class="timeline-boxed timekeeper">
                <?=getContentByYear('2010')?>
            </div>-->
        </div>
        </section>
    
        <section class="para_bg target" id="twelveth-part">
        <div class="twelveth-part parallax_bg">
            <div class="timeline-content">
                <?=getContentByYear('2012-2013')?>			
            </div>
            <div class="parallaxObject item-9" data-type="parallaxObj" data-speed="-0.6"><img src="<?php echo get_template_directory_uri();?>/images/history/parallax/parallax-item-9.png"></div>
        </div>
        </section>
    
        <section class="target" id="thirteenth-part">
        <div class="thirteenth-part">
        <div class="timeline-start">
            <div id="2013" class="scroll year-point eighth">2013</div>
            <div class="timeline-content">
                <div class="timeline-fixed-content icon-revival">
                    <div class="cover-bg">
                        <?=getContentByYear('2013')?>					
						<?php 					
							if(ICL_LANGUAGE_CODE=="fr") :						
								$link13= "http://www.edox.ch/shop/fr_fr/sporting-instruments/delfin.html";
							elseif(ICL_LANGUAGE_CODE=="es"):
								$link13 ="http://www.edox.ch/shop/es_es/sporting-instruments/delfin.html";					
							else :						
								$link13 ="http://www.edox.ch/shop/en/sporting-instruments/delfin.html";					
							endif;					
						?>
                        <a href="<?=$link13?>"><?php _e("see the collection"); ?></a>
                    </div>
                </div>				
                <div class="half new-delfin">
                    <img src="<?php echo get_template_directory_uri();?>/images/history/delfin-new-watch.png" class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="1s">
                </div><div class="half old-delfin">
                <img src="<?php echo get_template_directory_uri();?>/images/history/delfin-old-watch.png" class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="0s">
                <div class="timeline-boxed revival">
                    <?=getContentByYear('2014')?>						
                </div>
                </div>
            </div>
        </div>
        </section>
    
        <section id="end-part" class="target">
        <div class="end-part">
        <div id="2014" class="scroll year-point ninth">2014</div>
            <h1 class="os-animation" data-os-animation="fadeIn" data-os-animation-delay="0s"><img src="<?php echo get_template_directory_uri();?>/images/history/end-title.png"></h1>
            <?=getContentByYear('2014-desc')?>
        </div>
        </section>
    
    </div>
    
    <!-- parallax script -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/history/waypoints.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/assets/history/animate.css">
    <script src="<?php echo get_template_directory_uri();?>/assets/history/parallax-new.js"></script>
    <script type="text/javascript">
        var ua = navigator.userAgent;
        var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
    </script>
    <script type="text/javascript">
        var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
        if(!isiPad)var homeParallax = new parallax()
    </script>
    <script type="text/javascript">
        //smoothScroll.init();
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.sticky-navigation li a').on('click', function(event) {
                $(this).parent().find('a').removeClass('active');
                $(this).addClass('active');
            });
    
            $(window).on('scroll', function() {
                $('.target').each(function() {
                    if($(window).scrollTop() >= $(this).position().top) {
                        var id = $(this).attr('id');
                        $('.sticky-navigation li a').removeClass('active');
                        $('.sticky-navigation li a[href=#'+ id +']').addClass('active');
                    }
                });
            });
    
            $('.right-arrow').click(function(e){
                e.preventDefault();
                
                $(this).prev('.shuffle-content').find('.shuffle').each(function(){
                    if(!$(this).hasClass('hide')){
                        $(this).addClass('hide').animate({"opacity":"0", "left":"-1000px"}, "slow");
                    }else{
                        $(this).removeClass('hide').animate({"opacity":"1", "left":"0px"}, "slow");
                    }
    
                });
    
            });
    
    
        });
    </script>
    
    <style type="text/css">
        .os-animation{
            opacity: 0;
        }  
    
        .os-animation.animated{
            opacity: 1;
        }   
    </style>
    
    <script type="text/javascript">//<![CDATA[ 
        $(function(){
            function onScrollInit( items, trigger ) {
                items.each( function() {
                var osElement = $(this),
                    osAnimationClass = osElement.attr('data-os-animation'),
                    osAnimationDelay = osElement.attr('data-os-animation-delay');
                  
                    osElement.css({
                        '-webkit-animation-delay':  osAnimationDelay,
                        '-moz-animation-delay':     osAnimationDelay,
                        'animation-delay':          osAnimationDelay
                    });
    
                    var osTrigger = ( trigger ) ? trigger : osElement;
                    
                    osTrigger.waypoint(function() {
                        osElement.addClass('animated').addClass(osAnimationClass);
                        },{
                            triggerOnce: true,
                            offset: '60%'
                    });
                });
            }
    
            onScrollInit( $('.os-animation') );
            onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
    });//]]>  
    
    </script>
    <?php 
    echo $cache->captureEnd($cacheHistoryKey);
endif;
?>

<?php get_footer();?>