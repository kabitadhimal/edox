<?php
/*
  * Template Name: Media Template 
 *
 * 
 * */
get_header(); ?>
<style>
.modal-open{overflow-y:scroll; padding-right:0 !important;}
.media-lists{ font-size:0; overflow:hidden;}
.mymedia{ width:inherit;}
.mymedia article{float:left; line-height:1; position:relative; overflow:hidden;  text-align:center;}
.mymedia article figure{ cursor:pointer;}
.mediaTitle{background-color:#152e51; cursor:pointer;}
.header-section.bg-dark, .mediaTitle{
	background-image:-webkit-linear-gradient(top, rgba(255,255,255,.05) 0%, rgba(0,0,0,.4) 100%);
	background: -moz-linear-gradient(top,  rgba(255,255,255,.05) 0%, rgba(0,0,0,0.4) 100%); /* FF3.6+ */
	background-image:-o-linear-gradient(top, rgba(255,255,255,.05) 0%, rgba(0,0,0,.4) 100%);
	background-image:linear-gradient(to bottom, rgba(255,255,255,.05) 0%, rgba(0,0,0,.4) 100%);
	background: -ms-linear-gradient(top,  rgba(255,255,255,.05) 0%,rgba(0,0,0,0.4) 100%); /* IE10+ */
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#000000', GradientType=0);
	background-repeat:repeat-x;
}
.bg-dark{background-color:#25262a;}
.header-section.bg-dark h1,.header-section.bg-dark h2{ color:#fff;}
.tab-content{position:relative;}

/********************************************
# MEDIAMAG/MEDIATITLE WIDTH/HEIGHT 
*********************************************/
.mediaTitle, .mediaMag{width:calc(100% /2);}
	.mediaMag:before, .mediaTitle:before{content:'';display:block;padding-top:calc(100% + 50px);}
		.mediaMag .mediaMag-fullHeight{position:absolute;height:100%;top:0;right:0;bottom:0;left:0;}
		
		
		

/********************************************
# MEDIA TITLE
*********************************************/
.mediaTitle h2{font-size:38px; color:#efeef0; font-family: 'CenturyGothicBold'; text-transform:uppercase; vertical-align:middle; position:absolute;height:100%;top:0;right:0;bottom:0;left:0;}
	.mediaTitle h2:before{content:'';display:inline-block;vertical-align:middle;width:0;height:100%;}
	.mediaTitle h2 span{display:inline-block;vertical-align:middle;width:100%;}

/********************************************
# MEDIA CONTENT
*********************************************/
.mediaMag figure{height:100%; width:100%; display:block;text-align:left; background-repeat:no-repeat; background-position:center top; background-size:cover; width:100%;display:block; position:relative;}
.mediaMag figure:after{
	content:'';
	display:block;
	position:absolute;
	top:0; right:0; left:0; bottom:0;
	background-color:red;
	-webkit-transition: all 0.4s ease-in-out;
	   -moz-transition: all 0.4s ease-in-out;
		 -o-transition: all 0.4s ease-in-out;
		-ms-transition: all 0.4s ease-in-out;
			transition: all 0.4s ease-in-out;
			background-color:#000;
			opacity:0;
}
.mediaMag figure:hover:after{opacity:0.7;}

	.mediaMag--date{font-size:16px;}
	.mediaMag .modal p{font-size:16px;}


.nav-tabs{ text-align:center; border-bottom:none; font-size:0;}
	.nav-tabs li{height:43px; display:inline-block; font-family:'robotolight'; float:none; margin-bottom:0;}
	.nav-tabs li:not(:last-child){ border-right:1px solid #dbdddd;}
		.nav-tabs li a{background-color:#e6ecf0; display:block; font-size:15px; color:#737373; padding:0 45px; border-radius:0; border:none; margin:0; line-height:43px;}
		.nav-tabs li a:hover{background-color:#25262a; color:#fff;}
		.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{ border:none; background-color:#18345b; color:#fff;}

.films{background-color:#fff;}
h1.section-title{color:#25262a; font:normal 30px/1 'CenturyGothicBold'; text-align:center; padding:114px 0 15px; margin:0;}
h2.section-title--sub{font:normal 20px/1 'CenturyGothic'; color:#2e3546; text-align:center; padding-bottom:50px;}

ul.carousel-inner{margin:0 0 108px; padding:0; width:100%;}
ul.carousel-inner li{overflow:hidden;}
ul.carousel-inner a{width:calc(50% - 30px); float:left; overflow:hidden;}

/*
.carousel-inner a figure figcaption{
	-webkit-transition:background-color 0.4s linear;
}
.carousel-inner a:hover figure img{
	-webkit-transition:opacity 0.4s linear;	
}
.carousel-inner a:hover figure img{opacity:0.9}
.carousel-inner a:hover figure figcaption{background-color:black;}
*/


ul.carousel-inner a + a, ul.carousel-inner a + a figure{float:right;}

.carousel-inner figure{position:relative; display:inline-block; max-width:451px; width:100%;}
.carousel-inner figure figcaption{position:absolute; bottom:0; width:100%; left:0; padding:11px 16px 13px; font-size:16px; color:#ffffff; background-color:rgba(0,0,0,0.6); font-family:'robotolight';}

.video.item {max-height:768px;}

.modal-content{overflow:hidden;}
.modal-content img{display:block; margin:0 auto 10px; border-radius:3px 3px 0 0}
.modal-open .modal{padding-right:0 !important;}
.modal-dialog{margin:56px auto 0;}

@media (max-width: 767px){
	.modal-content{width:80%; margin:0 auto;}
	.modal-content img{max-width:100%;}
	.mediaMag .close-btn{top:10px; left:auto; right:50px; background-color:#18345b}
	
	.closeVideo{top:10px; right:10px; border-radius:0;}
	
	
	ul.carousel-inner a{width:100%;}
	ul.carousel-inner a figure{margin:0 auto 2px; float:none !important; display:block; width:60%;}
	ul.carousel-inner a figure img{max-width:100%;}
	
	.slider-1-section .carousel-control{top:50%; background-color:#e4e4e4 !important; margin-top:-50px;}
	
	.nav-tabs{text-align:left;}
	.nav-tabs li {border-top:1px solid #dbdddd;border-right:1px solid #dbdddd;}
	.nav-tabs li a{padding:0 33px;}
	
}

@media (max-width: 480px) {
	.mediaMag .modal-content h2{font-size:16px;}
}
@media (min-width: 480px) and (max-width: 767px) {
	.mediaTitle, .mediaMag{width:calc(100% /4);}
	.mediaMag .modal-content h2{font-size:22px;}
}
/* Small devices (tablets, 768px and up) */
@media (min-width: 768px) {
	.mediaTitle, .mediaMag{width:20%;}
	.mediaTitle:before, .mediaMag:before{content:'';display:inline-block;padding-top:calc(100% + 100px);}
}

</style>

<?php
while ( have_posts() ) : the_post();
    $blocks = get_field('media_flexible_contents');
    if($blocks):
        foreach ($blocks as $block):
            $tpl =  get_template_directory().'/partial/media-flexible-contents/'.strtolower($block['acf_fc_layout']).'.php';
            if (file_exists($tpl)) {include $tpl;}
        endforeach;
    endif;
endwhile;
?>
<script src="<?php echo get_template_directory_uri();  ?>/js/yt_video.js"></script>
<script>
$('.fancybox').fancybox();
</script>

<?php get_footer();
