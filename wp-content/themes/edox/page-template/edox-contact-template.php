<?php 
/*
 * Template Name: Contact Template
*
*
*
* */

get_header(); ?>
<?php
	$cache = get_procab_file_cache();
	$cacheContactKey = $cache->buildKey('contact');
	$cacheContactData = $cache->restore($cacheContactKey);
	if($cacheContactData) : echo $cacheContactData;
	else:
					
		$cache->captureStart($cacheContactKey);
 ?>

	<div class="contact-container">
		<div class="contact-header">
			<h1><?php the_title();?></h1>
			<h2 class="edox-phone"><i class="fa fa-phone"></i><?php echo get_field('contact_title',$post->ID);?></h2>
		</div>
		<div class="contact-map">
			<div id="map-canvas"></div>
		</div>
		<div class="contact-address">
				<div class="top-border"><img src="<?php echo get_template_directory_uri();?>/images/contact-seperation-up.png"></div>
				
				<?php /*?>
					<p>Montres Edox et Vista SA - Suisse - Direction</p>
					<p>2714 Les Genevez JU</p>
					<p>Tel. 032 484 70 10   -    Fax. 032 484 70 19</p> */?>
					<?php the_content();?>
					
				<div class="bottom-border"><img src="<?php echo get_template_directory_uri();?>/images/contact-seperation-down.png"></div>
		</div>
		<div class="contact-form">

			
				
				<h1><?php the_field('contact_form_title');?></h1>
	
			    <?php /*
			    	while (have_posts()):the_post();
			      		
			    		the_content();
			    	endwhile;
			    	*/
			    ?>
			    
			    <?php 
			    	$contactShortcode = get_field('contact_form_shortcode');
			    	
			    	echo do_shortcode($contactShortcode);
			    	
			    	
			    	$location = get_field('contact_google_map');
			    	
			    	
			    ?>
	        
		</div>
	</div>
	<script>		    
        jQuery("#country option:first:contains('---')").html('<?php _e('Country*') ?>');//Replace ---
        jQuery("#subject option:first:contains('---')").html('<?php _e('Subject*') ?>');
    </script>

	<script src="<?php echo get_template_directory_uri();  ?>/assets/select.js"></script>
<?php /*	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script> */ ?>
    <script>
		var map;
		var center =  new google.maps.LatLng(<?php echo $location['lat'].','.$location['lng'];?>);
		//var edox = new google.maps.LatLng(47.256244, 7.128529);
		var edox = new google.maps.LatLng(<?php echo $location['lat'].','.$location['lng'];?>);
		var MY_MAPTYPE_ID = 'custom_style';
		function initialize() {

		  var featureOpts = [
		    {
		      stylers: [
		        { hue: '#000' },
		        { saturation: -100 },
		        { visibility: 'simplified' },
		        { gamma: 0.7 },
		        { weight: 0.5 }
		      ]
		    }
		  ];
		  var isDraggable = $(document).width() > 767 ? true : false;
		  var mapOptions = {
		    zoom: 14,
		    center: edox,
		    mapTypeControlOptions: {
		      mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
		    },
		    mapTypeId: MY_MAPTYPE_ID,
		    draggable: isDraggable,
		    scrollwheel: false
		  };

		  map = new google.maps.Map(document.getElementById('map-canvas'),
		      mapOptions);

		  var styledMapOptions = {
		    name: 'Custom Style'
		  };

		  
		  var image = "<?php echo get_template_directory_uri();?>/images/map-marker.png";
		  var marker = new google.maps.Marker({
		       position: edox,
		       map: map,
		       icon: image,
		       title: 'Les Genevez'
		  });

		  
		  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

		  map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("select").css("turnintodropdown");
		});
	</script>
    
  <?php  
  			echo $cache->captureEnd($cacheContactKey);
  		endif;
   ?>
    

    <style>
		.wpcf7-list-item-label {
			display:none;
		}
	</style>
 
  <?php get_footer(); ?>