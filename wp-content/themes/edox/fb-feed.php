<?php
use Facebook\Authentication\AccessToken;
require_once __DIR__ . '/libs/facebook-php-sdk-v4/src/Facebook/autoload.php';
require_once __DIR__ . '/libs/Date_HumanDiff/src/Date/HumanDiff.php';


define('APP_ID', '642642572502521');
define('APP_SECRET_ID', '0051d6983734f15082bdd7af69cb0e25');


function getPostHTML($post, $dh){
	?>
	<li class="dcsns-li dcsns-facebook dcsns-feed-0" >
	<a href="<?=$post['link']?>">
		<img src="<?=$post['full_picture']?>" alt="" class="img-responsive"></a>
          <div class="inner"><span class="section-thumb"></span><span class="section-text"><?=$post['message']?></span><span class="clear"></span></div>
          <span class="section-intro"><a href="<?=$post['link']?>">Posted <?=$dh->get($post['created_time'])?></a> </span></li>
	<?php
}

function getFacebookFeeds($limit){
	$fb = new Facebook\Facebook(array(
		'app_id' => APP_ID,
		'app_secret' => APP_SECRET_ID,
		'default_graph_version' => 'v2.2',
	));

	
	try {
    	$response = $fb->get('/141549179270798/posts?fields=full_picture,link,created_time,message&limit='.$limit.'', APP_ID.'|'.APP_SECRET_ID);
		
		$dh = new Date_HumanDiff();
		$posts = $response->getDecodedBody()['data'];
		
		//foreach($posts as $post):
			
		//endforeach;
?>
<div class="dcsns-content">
<ul class="feed-ul">
    <li class="first_colmn" data-type="sprite" data-speed="0.2">
	    <ul class="stream">
        	<?php if(!empty($posts[0])) echo getPostHTML($posts[0], $dh); ?>
        	<?php if(!empty($posts[1])) echo getPostHTML($posts[1], $dh); ?>
	    </ul>
    </li>
    <?php if($limit == 6): ?>
    <li class="first_colmn" data-type="sprite" data-speed="0.6">
	    <ul class="stream">
        	<?php if(!empty($posts[2])) echo getPostHTML($posts[2], $dh); ?>
        	<?php if(!empty($posts[3])) echo getPostHTML($posts[3], $dh); ?>
	    </ul>
    </li>
    
    <li class="first_colmn" data-type="sprite" data-speed="0.4">
	    <ul class="stream">
        	<?php if(!empty($posts[4])) echo getPostHTML($posts[4], $dh); ?>
        	<?php if(!empty($posts[5])) echo getPostHTML($posts[5], $dh); ?>
	    </ul>
    </li>
    <?php endif; ?>
</ul>
</div>
<?php	

	}catch(Facebook\Exceptions\FacebookSDKException $e) {
		
	exit;
	}
}
$limit = (!isset($_GET['more'])) ? 1 : 6;
getFacebookFeeds($limit);
