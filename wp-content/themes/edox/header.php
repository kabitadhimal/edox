<?php
if(session_id() == '') {
	session_start();
}

//Available store location
$availableCountries = ['cn','fr','bs','en','de','es','ch','at'];

if(isset($_GET['location']) && $_GET['location'] != ''){
	//if(in_array( strtolower($_GET['location']), $availableCountries)){
	//set on session
	$_SESSION['location'] = $_GET['location'];
	//}
}

global $currCountryCode;
global $storeCode;

if(isset($_SESSION['location']) && in_array($_SESSION['location'], $availableCountries) ){
	$currCountryCode = strtolower($_SESSION['location']);
}else{
	$currCountryCode = strtolower(kd_detect_country());
}
$storeCode = getStoreByLangAndLocation(ICL_LANGUAGE_CODE, $currCountryCode);
/* if(isset($_GET['test'])) {
   var_dump($currCountryCode , $storeCode);
 }*/
?>
<!DOCTYPE html>
<html lang="<?php language_attributes();?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5NDSTZ');</script>
    <!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' );?>">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php
$mailChimpScript = !empty(get_global_option('mailchimp_script'))? get_global_option('mailchimp_script'):'' ;
if(!empty($mailChimpScript)):
    echo $mailChimpScript;
endif;?>
	<!-- favicon -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<script type="text/javascript">
		var themeUrl = "<?php echo get_template_directory_uri(); ?>";
		window.ajaxURL = "<?=site_url('/');?>wp-admin/admin-ajax.php";
	</script>
	<?php
	if( is_home() || is_page("store-locator")) {
		?>
		<script type="text/javascript" async="true"> (function() { setTimeout(function(){ var adscienceScript = document.createElement("script" ); adscienceScript.type = "text/javascript"; adscienceScript.async = true; adscienceScript.src = "//rtb8.adscience.nl/segmentpixel.php?advertiser_id=310"; var s = document.getElementsByTagName("script" )[0]; s.parentNode.insertBefore(adscienceScript, s); },1); })(); </script><noscript><img src="//rtb7.adscience.nl/cgi-bin/sgmnt.fcgi?advertiser_id=310"/></noscript>

	<?php } ?>
	<?php wp_head(); ?>
	<!--[if lte IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/ie9-and-down.css" />
	<![endif]-->
  
</head>
<?php
global $post;
$pageTemplate = get_post_meta ( $post->ID, '_wp_page_template', true );

if(is_home() || is_front_page()) :?>
	<script type='text/javascript'>
		// Conversion Name: Edox- Landing page
		// INSTRUCTIONS
		// The Conversion Tags should be placed at the top of the <BODY> section of the HTML page.
		// In case you want to ensure that the full page loads as a prerequisite for a conversion
		// being recorded, place the tag at the bottom of the page. Note, however, that this may
		// skew the data in the case of slow-loading pages and in general not recommended.
		//
		// NOTE: It is possible to test if the tags are working correctly before campaign launch
		// as follows: Browse to http://bs.serving-sys.com/Serving/adServer.bs?cn=at, which is
		// a page that lets you set your local machine to 'testing' mode. In this mode, when
		// visiting a page that includes an conversion tag, a new window will open, showing you
		// the data sent by the conversion tag to the MediaMind servers.
		//
		// END of instructions (These instruction lines can be deleted from the actual HTML)
		var ebRand = Math.random()+'';
		ebRand = ebRand * 1000000;
		document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=614242&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
	</script>
	<noscript>
		<img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=614242&amp;ns=1"/>
	</noscript>
<?php endif;
if($pageTemplate == 'page-template/point-of-sale.php') :?>
	<?php /*<script type='text/javascript'>
	// Conversion Name: Edox- Store locator
	// INSTRUCTIONS
	// The Conversion Tags should be placed at the top of the <BODY> section of the HTML page.
	// In case you want to ensure that the full page loads as a prerequisite for a conversion
	// being recorded, place the tag at the bottom of the page. Note, however, that this may
	// skew the data in the case of slow-loading pages and in general not recommended.
	//
	// NOTE: It is possible to test if the tags are working correctly before campaign launch
	// as follows: Browse to http://bs.serving-sys.com/Serving/adServer.bs?cn=at, which is
	// a page that lets you set your local machine to 'testing' mode. In this mode, when
	// visiting a page that includes an conversion tag, a new window will open, showing you
	// the data sent by the conversion tag to the MediaMind servers.
	//
	// END of instructions (These instruction lines can be deleted from the actual HTML)
	var ebRand = Math.random()+'';
	ebRand = ebRand * 1000000;
	document.write('<scr'+'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=614243&amp;rnd=' + ebRand + '"></scr' + 'ipt>');
	</script>
    */ ?>
	<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: EDOX Homepage
URL of the webpage where the tag is expected to be placed: http://www.edox.ch
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 04/21/2016
-->
	<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxh0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
	</script>
	<noscript>
		<iframe src="https://4954433.fls.doubleclick.net/activityi;src=4954433;type=edoxs0;cat=edoxh0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
	</noscript>
	<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

	<noscript>
		<img width="1" height="1" style="border:0" src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=614243&amp;ns=1"/>
	</noscript>

<?php endif;?>
<?php //$id=is_home()?'id="home"': 'id="inside"';

$id ='id="home"';
?>
<?php
//$countryCode = $currentLocation->countryCode;
//if($currCountryCode == 'US'  || $currCountryCode == 'AT' || $currCountryCode == 'GB') {
if($currCountryCode=='at') {
	$class=" us-site";
}else {
	$class="";
}
?>
<body <?php echo $id; ?> <?php body_class( $class ); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NDSTZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- mega menu -->
<header id="phone-menu">
	<a href="<?php echo home_url();?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-small.png"></a>
</header>



	<header class="main-nav-wrapper main-nav-fixedx main-nav-inner">
		<div>
			<a href="<?php echo home_url();?>" class="logo-small wow fadeIn"  data-wow-duration="0.5s" data-wow-delay="0.10s"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-small.png"></a>
			<a href="<?php echo home_url();?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"></a>
			<div class="main-nav">
				<div id="mega-menu">
					<?php
					$menuArgs = array(
						'theme_location'  => 'primary',
						'menu'            => '',
						'container'       => '',
						'container_class' => 'menu list-inline',
						'container_id'    => '',
						'menu_class'      => 'menu',
						'menu_id'         => 'menu-menu',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '<span class="menu-span">',
						'link_after'      => '</span>',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					echo wp_nav_menu($menuArgs);
					?>

				</div>
			</div>

			<div id="main-nav-overlay" style="display: none;" class=""></div>
			<!--<a class="line-three side-nav-btn" id="side-nav-btn"></a> -->
			<ul id="menu-top-menu" class="side-nav list-inline menu-block">

				<?php
				$menuArgs = array(
					'theme_location'  => 'top-menu',
					'menu'            => '',
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'side-nav list-inline menu-block',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '%3$s',
					'depth'           => 0,
					'walker'          => ''
				);
				echo wp_nav_menu($menuArgs);
				?>
                <?php
                if(ICL_LANGUAGE_CODE=="zh-hant") {
                    $currentLang = "中国";
                }else {
                    $currentLang = strtoupper(ICL_LANGUAGE_CODE);
                }
                ?>
                <li class="menu-item lang"><a href=""><?php echo $currentLang; ?></a>
                    <ul class="menu-lang">
                        <?php
                        $languages = apply_filters( 'wpml_active_languages', NULL, array( 'skip_missing' => 0 ) );
                        if( !empty( $languages ) ) {
                            foreach( $languages as $language ){
                                if($language['code']=="zh-hant") {
                                    $langCode = "中国";
                                }else {
                                    $langCode = strtoupper($language['code']);
                                }

                                if( !$language['active'] ) :
                                    echo '<li><a href="' . $language['url'] . '">'.$langCode.'</a></li>';
                                endif;
                            }
                        }
                        ?>
                    </ul>
                </li>
			</ul>
		</div>
	</header>

	<div id="phone-menu-content" class="hidden"><ul></ul></div>

    <?php
    $cache = get_procab_file_cache();
    $cacheCollectionMenuKey = 'collection-menu-'.ICL_LANGUAGE_CODE.'-'.$storeCode;
    $cacheCollectionMenuData = $cache->restore($cacheCollectionMenuKey, 'static');
    if($cacheCollectionMenuData): echo $cacheCollectionMenuData;
    else:
        $cache->captureStart($cacheCollectionMenuKey, 'static', 259200); //3days
    ?>
	<?php

	    $collectionMenuUrl = site_url('/').getStoreCollectionUrl($storeCode);
		$collectionContent = @file_get_contents_curl($collectionMenuUrl);
		?>
		<div id='nav-collection' style="display:none;"><?=$collectionContent?></div>
		
<?php    echo $cache->captureEnd($cacheCollectionMenuKey);
    endif;?>

<script>
	$('#mega-menu .services>.sub-menu>li .menu-span img').each(function(index, element) { $(this).appendTo(jQuery(this).parent().parent());});
	$($('#nav-collection').html()).prependTo($('#mega-menu > ul'));
	$('#nav-collection').remove();
	$('.cart-icon a').addClass('cart-icon-link');

	$('#phone-menu-content > ul').html($('#mega-menu > ul').html());
//	$('#phone-menu-content .collection .sub-menu > li:last').remove();
	//replace 'vintage' collection
	var $lastAnchor = $('#phone-menu-content .collection > ul > li:last > a:last')
	var $lastUrl = $('#phone-menu-content .collection > ul > li:last > ul:last')
	var $lastList = $('<li />').appendTo($('#phone-menu-content .collection > ul'));
	$lastAnchor.appendTo($lastList);
	$lastUrl.appendTo($lastList);

	$('#menu-top-menu li.menu-item').clone().appendTo($('#phone-menu-content > ul'));
	$('#mega-menu>ul>li>.sub-menu').addClass('animated fadeInDown');
	//move cart link to the header menu
	$($('.cart-icon a')[0]).clone().appendTo($('#header > div'))

	<?php if($currCountryCode=='at') {?>
	var $campurl = $('li.purse a').attr("href");
	url = $campurl.replace('us_en', 'au_au');
	$('li.purse a').attr('href', url);
	<?php } ?>

</script>
<style>
    ul.menu-lang li.mean-last {
        display : block !important;
    }
</style>
<div class="container " id="main">