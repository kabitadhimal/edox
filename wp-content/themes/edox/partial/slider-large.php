<div class="full-width-extend ">
		<div class="grey-bg">
		<div class="slider-2-section clearfix carousel slide carousel-fade row" data-ride="carousel" id="slider_<?=$sliderId?>">
		<ul class="carousel-inner">
<?php
while ($query->have_posts()): $query->the_post();
	
if( have_rows('add_slider') ):
$cssClass = 'active';
while( have_rows('add_slider') ): the_row();
	
$image = get_sub_field('slider_image');
$title = get_sub_field('slider_title');
$text = get_sub_field('slider_text');
?>
	<li class="item <?=$cssClass?>">
	<div class="slider-content">
		<div class="hidden-sm hidden-xs"><h1><?=$title?></h1><?=$text?></div>
<?php if($image):	$imgSrc = wp_get_attachment_image_src($image,'large-slider');?>
	<div><img src="<?=$imgSrc[0]?>"></div>
<?php endif; ?>
	<div class="visible-sm visible-xs"><h1><?=$title?></h1><?=$text?></div></div></li>
<?php
$cssClass = ''; $totalItems++;
endwhile;
endif;
endwhile;
?>
</ul>
<?php if($totalItems > 1): ?>
<a class="left carousel-control" href="#slider_<?=$sliderId?>" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#slider_<?=$sliderId?>" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>
<?php endif; ?>
</div>
			</div>
			</div>


<?php
/*
$slider = '<div class="full-width-extend ">
		<div class="grey-bg">
		<div class="slider-2-section clearfix carousel slide carousel-fade row" data-ride="carousel" id="slider_'.$sliderId.'">
		<ul class="carousel-inner">';

while ($query->have_posts()): $query->the_post();
	
if( have_rows('add_slider') ):
$cssClass = 'active';
while( have_rows('add_slider') ): the_row();
	
$image = get_sub_field('slider_image');
$title = get_sub_field('slider_title');
$text = get_sub_field('slider_text');
$slider .= '<li class="item '.$cssClass.'">';
$slider .= '<div class="slider-content">
					<div class="hidden-sm hidden-xs"><h1>'.$title.'</h1>'.$text	.'</div>';
if($image) :
$imgSrc = wp_get_attachment_image_src($image,'large-slider');
$slider .=	'<div><img src="'.$imgSrc[0].'"></div>';
endif;
	
$slider .='<div class="visible-sm visible-xs"><h1>'.$title.'</h1>'.$text	.'</div></div></li>';
$cssClass = '';
$totalItems++;
endwhile;

endif;

endwhile;
$slider .= '</ul>';
if($totalItems > 1):
$slider .= '<a class="left carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#slider_'.$sliderId.'" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>';
endif;
$slider .= '</div>
			</div>
			</div>';
			*/