<?php
$cache = get_procab_file_cache();
$cacheStoreLocatorKey = 'home-store-locator-'.ICL_LANGUAGE_CODE;
$cacheStoreLocatorData = $cache->restore($cacheStoreLocatorKey, 'static');
if($cacheStoreLocatorData): echo $cacheStoreLocatorData;
else:
$cache->captureStart($cacheStoreLocatorKey, 'static', 259200); //3days

/*$storeImage = !empty(get_field('store_locator_background_image')) ? get_field('store_locator_background_image'):'';
$storeTitle = !empty(get_field('store_locator_title')) ? get_field('store_locator_title'):'';
$storeText = !empty(get_field('store_locator_text')) ? get_field('store_locator_text'):'';
$storeLink = !empty(get_field('store_locator_link')) ? get_field('store_locator_link'):'#';*/
$storeImage = get_field('store_locator_background_image');
$storeTitle = get_field('store_locator_title');
$storeText = get_field('store_locator_text') ;
$storeLink = get_field('store_locator_link');

?>
<div class="c-md-banner bg-default bg-cover c-banner c-slash" style="background-image: url(<?php echo $storeImage ?>);">
    <?php if(!empty($storeTitle)) : ?><h1 class="c-title"><?php echo $storeTitle; ?></h1><?php endif; ?>
   <?php echo $storeText; ?>
    <a href="<?=$storeLink?>" class="btn btn-primary ecc-login-btn"><?php the_field("store_locator_link_label")?></a>
</div>
    <?php
    echo $cache->captureEnd($cacheStoreLocatorKey);
endif;