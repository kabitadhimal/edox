<div class="clearfix"></div>
<div class="watch-section rowx widget-tblx container-md">
    <h3 class="title-section color-default text-center c-slash"><?php _e("about us","edox"); ?></h3>
    <div class="col-xs-12 col-md-6 full-height widget-tbl-cellx">
        <div class="half-image-text-section text-center bg-black bg-darkgrey">
            <?php
            $cachePartnershipKey = 'home-partnership-'.ICL_LANGUAGE_CODE;
            $cachePartnershipData = $cache->restore($cachePartnershipKey, 'static');
            if($cachePartnershipData): echo $cachePartnershipData;
            else:
                $cache->captureStart($cachePartnershipKey, 'static', 259200); //3days
                ?>
                <div class="rowx row-verticalx">
                <div class="swiper-container swiper-container-news">
                    <div class="swiper-wrapper">
                        <?php
                        $championListID = get_field('champions_list','options');
                        if(!empty($championListID)) :
                            $championListID = my_icl_object_id($championListID,'page');
                            $champLink = get_permalink($championListID);
                        endif;
                        
                        $query = new WP_Query('post_type=partnership&posts_per_page=4&post_status=publish');
                        while ($query->have_posts()):$query->the_post();
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'partnership-thumbnail-img');
                            ?>
                            <div class="swiper-slide">

								<div class="col-xs-6"><a href="<?php the_permalink(); ?>"><img src="<?php echo $image[0]; ?>" style="width:100%"></a></div>
								<div class="col-xs-6 teaser">
									<img src="<?php the_field('vip_sponsor_logo');?>" class="hidden-sm" style="width:auto; max-width:100%">
									<h1><?php the_title(); ?></h1>
									<?php if(!empty($champLink)): ?><br><br/> <a href="<?=$champLink?>" data-text="<?php _e("See all champions","edox"); ?>"  class="btn btn-primary"><?php _e("See all champions","edox"); ?></a><?php endif;?>
								</div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <?php
            echo $cache->captureEnd($cachePartnershipKey);
            endif;?>
            <?php
            $cacheUniverseKey = 'home-universe-'.ICL_LANGUAGE_CODE;
            $cacheUniverseData = $cache->restore($cacheUniverseKey, 'static');
            if($cacheUniverseData): echo $cacheUniverseData;
            else:
            $cache->captureStart($cacheUniverseKey, 'static', 259200); //3days
            ?>
            <div class="row row-vertical">
                <?php
            /*    $universeImage = !empty(get_field('universe_image')) ? get_field('universe_image'):'';
                $universeTitle = !empty(get_field('universe_title')) ? get_field('universe_title'):'';
                $universeText = !empty(get_field('universe_text')) ? get_field('universe_text'):'';
               */
               
                $universeImage = get_field('universe_image');
                $universeTitle = get_field('universe_title');
                $universeText = get_field('universe_text');
                
               
                ?>
                <div class="col-xs-6 teaser">
                    <?php if(!empty($universeTitle)) : ?><h1 class="c-slash"><?=$universeTitle?></h1><?php endif; ?>
                    <?php if(!empty($universeText)) : ?><p><?=$universeText?></p><br /><br /><br /><?php endif;?>
                    <?php if(!empty($champLink)): ?><a href="<?=$champLink?>" data-text="<?php _e("See more","edox"); ?>" class="btn btn-primary"><?php _e("See more","edox"); ?></a><?php endif; ?>
                </div>
                <div class="col-xs-6">
					<a href="<?=$champLink?>">
						<img src="<?php echo $universeImage; ?>" class="img-responsivex" style="width:100%">
					</a>
				</div>
            </div>
            <?php
                echo $cache->captureEnd($cacheUniverseKey);
            endif;?>
        </div>
    </div>

    <?php
    $cacheHistoryKey = 'home-history-'.ICL_LANGUAGE_CODE;
    $cacheHistoryData = $cache->restore($cacheHistoryKey, 'static');
    if($cacheHistoryData): echo $cacheHistoryData;
    else:
        $cache->captureStart($cacheHistoryKey, 'static', 259200); //3days

   /*     $histoireTitle = !empty(get_field('block_histoire_title')) ? get_field('block_histoire_title'):'';
        $histoireYear = !empty(get_field('histoire_year')) ? get_field('histoire_year'):'';
        $histoireImage = !empty(get_field('histoire_image')) ? get_field('histoire_image'):'';
        $histoireRightLabel = !empty(get_field('histoire_image_right_label')) ? get_field('histoire_image_right_label'):'';
        $histoireLeftLabel = !empty(get_field('histoire_image_left_label')) ? get_field('histoire_image_left_label'):'';
        $histoireLinkLabel = !empty(get_field('histoire_link_label')) ? get_field('histoire_link_label'):'';*/
        
             $histoireTitle = get_field('block_histoire_title');
        $histoireYear = get_field('histoire_year');
        $histoireImage = get_field('histoire_image');
        $histoireRightLabel = get_field('histoire_image_right_label');
        $histoireLeftLabel = get_field('histoire_image_left_label');
        $histoireLinkLabel = get_field('histoire_link_label');
        
        
        $historyListID = get_field('history_list','options');
        if(!empty($historyListID)) :
            $historyListID = my_icl_object_id($historyListID,'page');
            $histoireLink = get_permalink($historyListID);
        endif;
        ?>
        <div class="col-xs-12 col-md-6">
            <div class="text-center world-of-edox" style="position:relative">
                <div class="caption wow fadeInDown">
                    <?php if(!empty($histoireTitle)): ?><h3><?=$histoireTitle?></h3><?php endif; ?>
                    <?php if(!empty($histoireYear)): ?><p><?=$histoireYear?></p><?php endif; ?>
                </div>
                <div class="watch"><img src="<?php echo $histoireImage; ?>">
                    <?php if(!empty($histoireRightLabel)): ?><span class="right wow fadeInRight hidden-xs"><?=$histoireRightLabel?></span><?php endif;?>
                    <?php if(!empty($histoireLeftLabel)): ?><span class="left wow fadeInLeft hidden-xs"><?=$histoireLeftLabel?></span><?php endif;?>
                </div>
                <div class="watch-sec-btn">
                    <a href="<?=$histoireLink?>" data-text="<?php _e("See more"); ?>" class="btn btn-primary"><?php _e("See more","edox");?></a>
                </div>
            </div>
        </div>
    <?php
    echo $cache->captureEnd($cacheHistoryKey);
    endif;?>
<div class="clearfix"></div>
</div>