<?php
$cache = get_procab_file_cache();
$cacheWatchFinderKey = 'watch-finder-'.ICL_LANGUAGE_CODE;
$cacheWatchFinderData = $cache->restore($cacheWatchFinderKey, 'static');
if($cacheWatchFinderData): echo $cacheWatchFinderData;
else:
    $cache->captureStart($cacheWatchFinderKey, 'static', 259200); //3days
    $title = get_field('watch_finder_title');
    $text = get_field('watch_finder_text');
    $backgroundImage =  get_field('watch_finder_background_image');
    $linkLabel =  get_field('watch_link_label');
    $link =  get_field('watch_finder_link');
?>

<div class="c-md-banner bg-default bg-cover c-banner c-slash" style="background-image: url(<?php echo $backgroundImage; ?>);">
    <?php if(!empty($title)): ?>
        <h1 class="c-title"><?=$title?></h1>
    <?php endif; ?>
    <?php if(!empty($text)): ?>
        <p><?=$text?></p>
    <?php endif; ?>
    <a href="<?=$link?>" class="btn btn-primary ecc-login-btn"><?=$linkLabel?></a>
</div>
<?php
    echo $cache->captureEnd($cacheWatchFinderKey);
endif;