<script src="<?php echo get_template_directory_uri(); ?>/js/fullscreen.js"></script>
<?php
$cache = get_procab_file_cache();
$cacheHomeSliderKey = "home-slider-" . ICL_LANGUAGE_CODE;
$cacheHomeSliderData = $cache->restore($cacheHomeSliderKey, 'static');
if ($cacheHomeSliderData): echo $cacheHomeSliderData;
else:
    $cache->captureStart($cacheHomeSliderKey, 'static', 259200); //3days
    ?>
    <div class="banner video-banner">
        <div id="carousel-banner" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <?php
                $args = array(
                    "post_type" => 'home-slider',
                    "suppress_filters" => false,
                    'posts_per_page' => -1,
                    "order" => "DESC",
                    "orderby" => "date",
                    "post_status" => 'publish'
                );
                $query = new WP_Query($args);
                $countSlider = 1;

                function getVideoId($url)
                {
                    $url = parse_url($url);
                    parse_str($url['query'], $query);
                    if (isset($query['v'])) {
                        return $query['v'];
                    }
                    return null;
                }
                $videoID = '';
                while ($query->have_posts()): $query->the_post();
                    //$postCount = $query->post_count;
                    $activeClass = ($countSlider == 1) ? 'active' : "";
                    $contentType = get_field("content_type", $post->ID);
                    $sliderLink = get_field('home_slider_link');
                    $title1 = get_field('title_1');
                    $title2 = get_field('title_2');
                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                    $vUrl = get_field('video_url');

                    $vID = getVideoId($vUrl);

                    if ($contentType == "Video") {
                        ?>
                            <div class="item video-item <?= $activeClass ?>" data-videoid="<?=$vID?>">
                            <button id="enable-fulscreen">play fullscreen</button>
                            <div class="vdo-wrap" >
                                <script type="text/tpl" id="video-fallback">
                                <img src="<?= $image['0'] ?>" class="img-responsive" />
                                </script>
                                <div class="caption">
                                    <?php if (!empty($title1)) : ?>
                                        <p class="title"><?= $title1 ?></p>
                                    <?php endif; ?>
                                    <?php if (!empty($title2)) : ?>
                                        <p class="title-sub"><?= $title2 ?></p>
                                    <?php endif; ?>
                                </div>

                                <div class="embed">
                                    <div class="embed__overlay" id="player-overlay"
                                         style="background-image:url('<?= $image['0'] ?>');"></div>
                                    <div class="embed__content">
                                        <div id="player" class="z-index-back"></div>
                                    </div>
                                </div>

                                <div class="vdo-wrap-sibling">
                                    <a class="js-sound sound-ctrl sound-ctrl--muted control"></a>
                                    <script>
                                        if (window.screen.width > 700) {
                                            homepageVdo.loadVideoById('<?=$vID?>', 'player', '#player-overlay');

                                        } else {
                                            document.querySelector('.vdo-wrap').innerHTML = document.getElementById('video-fallback').innerHTML;
                                            document.querySelector('.js-sound').remove();
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="item <?= $activeClass ?>">
                            <a href="<?= $sliderLink ?>">
                                <img src="<?= $image['0'] ?>" class="img-responsive"/>
                            </a>
                            <div class="caption">
                                <?php if (!empty($title1)) : ?>
                                    <p class="title"><?= $title1 ?></p>
                                <?php endif; ?>
                                <?php if (!empty($title2)) : ?>
                                    <p class="title-sub"><?= $title2 ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php $countSlider++; endwhile;
                wp_reset_postdata(); ?>

            </div>
            <ol class="carousel-indicators">
                <?php
                $args = array(
                    "post_type" => "home-slider",
                    "suppress_filters" => false,
                    'posts_per_page' => -1,
                    "order" => "DESC",
                    "orderby" => "date",
                    "post_status" => "publish"
                );
                $query = new WP_Query($args);
                $count = 0;
                while ($query->have_posts()): $query->the_post();
                    //$count = $query->post_count;
                    $activeClass = ($count == 0) ? 'class="active"' : "";
                    ?>
                    <li data-target="#carousel-banner"
                        data-slide-to="<?php echo $count; ?>" <?php echo $activeClass; ?>></li>
                    <?php
                    $count++;
                endwhile;
                wp_reset_postdata();
                ?>
            </ol>
        </div>
        <a href="#content" class="scroll anchor bounce"></a>
    </div>

    <?php
    echo $cache->captureEnd($cacheHomeSliderKey);
endif;
?>

<script>
    $(document).ready(function () {
        window.bsCarouselNode = jQuery('#carousel-banner').carousel({
            interval: 5000,
        });
        window.bsCarouselNode.on('slid.bs.carousel', function (event) {
            if ($(event.relatedTarget).index() === 0) {
                if (window.screen.width > 700) {
                    var $v= $('.video-item').data('videoid');
                    homepageVdo.loadVideoById($v, 'player', '#player-overlay');
                } else {
                    document.querySelector('.vdo-wrap').innerHTML = document.getElementById('video-fallback').innerHTML;
                    document.querySelector('.js-sound').remove();
                }
            }
        });
        /*  $('#player').on('stop pause ended', function (e) {
              $("#carousel-banner").carousel({
                  interval: 2000
              });
          });*/
    });
</script>