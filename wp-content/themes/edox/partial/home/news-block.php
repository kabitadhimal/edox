<div class="watch-section news-social-section clearfix container-md">
    <h3 class="title-section color-default text-center c-slash"><?php _e("connect & SHARE","edox"); ?></h3>
    <div class="display-flex bg-darkgrey">
        <?php
        $cache = get_procab_file_cache();
        $cacheNewsKey = 'home-news-'.ICL_LANGUAGE_CODE;
        $cacheNewsData = $cache->restore($cacheNewsKey, 'static');
        if($cacheNewsData): echo $cacheNewsData;
        else:
        $cache->captureStart($cacheNewsKey, 'static', 259200); //3days
        ?>
        <div class="col-xs-12 col-md-6">
            <?php
            $query = new WP_Query('posts_per_page=1');
            $count = 1;
            $newsListID = get_field('news_list','options');
            if(!empty($newsListID)) :
                $newsListID = my_icl_object_id($newsListID,'page');
                $newsLink = get_permalink($newsListID);
            endif;
            while($query->have_posts()): $query->the_post();
                $newsImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                if($newsImage) $newsImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($newsImage[0]), \App\IMAGE_SIZE_NEWS_THUMB);
                $shortDesc = get_field('short_description')
                ?>
                <div class="text-center bg-black">
                    <a href="<?php the_permalink(); ?>"><img src="<?php echo $newsImage;?>" style="width:100%"></a>
                    <div class="c-news__content wow fadeInDown">
                        <?php the_title('<h3>','</h3>');?>
                        <?php if(!empty($shortDesc)): ?>
                            <?=$shortDesc?>
                        <?php endif; ?>
                        <br /><br />
                        <a href="<?=$newsLink?>" data-text="See more" class="btn btn-primary"><?php _e("See more","edox"); ?></a>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <?php
            echo $cache->captureEnd($cacheNewsKey);
        endif;
        ?>
        
        <?php
        $cacheInstaKey = 'insta';
        $cacheInstaData = $cache->restore($cacheInstaKey, 'static');
        //$cacheInstaData = false;
        if($cacheInstaData): echo $cacheInstaData;
        else:
            $cache->captureStart($cacheInstaKey, 'static', 259200); //seconds
            ?>
            <div class="col-xs-12 col-md-6">
                <div class="c-insta">
                    <?php
                    $instaItems = getInstagramFeeds(4);
                    foreach($instaItems as $instaItem):
                        $link = $instaItem[0];
                        $image = $instaItem[1];
                        $image = str_replace("http://","https://", $image);
                        ?>
                        <a href="http://instagram.com/edoxswisswatches" target="_blank" class="full-absolute"></a>
                        <div class="overlap-default"><i class="fab fa-instagram"></i></div>
                        <div class="col-xs-6"><img src="<?=$image?>" class="img-responsivex" style="width:100%"></div>
                    <?php endforeach; ?>

                </div>
            </div>
            <?php
            echo $cache->captureEnd($cacheInstaKey);
        endif;
        ?>

    </div>
    <div class="text-center follow-us">
        <span><?php _e("FOLLOW US","edox"); ?></span>
        <div class="footer-social">
            <a href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819" target="_blank"><i class="fab fa-facebook"></i></a>
            <a href="https://twitter.com/edoxwatches"><i class="fab fa-twitter" target="_blank"></i></a>
            <a href="http://instagram.com/edoxswisswatches" target="_blank"><i class="fab fa-instagram"></i></a>
            <a href="https://www.youtube.com/user/EdoxWatches"><i class="fab fa-youtube" target="_blank"></i></a>
            <a href="https://www.pinterest.com/edoxwatches/" target="_blank"><i class="fab fa-pinterest-square"></i></a>
        </div>
    </div>
</div>