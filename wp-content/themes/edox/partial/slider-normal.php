<?php 

	$query = new WP_Query( array( 'post_type' => 'slider', 'p' => $id ) );
	$count = 1;
	$slider ='<div class="fixed-width slider-1-section clearfix carousel slide row" data-ride="carousel">
			<ul class="carousel-inner">';
	while ($query->have_posts()): $query->the_post();
	
	 if( have_rows('add_slider') ): ?>
	
		<?php while( have_rows('add_slider') ): the_row(); 

		// vars
		$image = get_sub_field('slider_image');
		$title = get_sub_field('slider_title');
		$text = get_sub_field('slider_text');
		
		$active = ($count==1)?' active' :'';
		$slider .= '<li class="item'.$active.'">';
		if($image) :
				$imgSrc = wp_get_attachment_image_src($image,'normal-slider');
				$slider .=	'<div> <img src="'.$imgSrc[0].'"> </div>';
		endif;
		$slider .= '<div><h1>'.$title.'</h1>'.$text.'</div></li>';
		$count ++;
	endwhile;
	
	endif;
	
	endwhile;
	
	$slider .='</ul>
              <a class="left carousel-control" href=".slider-1-section" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href=".slider-1-section" role="button" data-slide="next">
               <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>';
	
