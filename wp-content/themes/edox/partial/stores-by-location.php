<div id="ajx-stores-list">
<?php
//http://192.168.0.213/edox/wp-admin/admin-ajax.php?action=sale_country&city=a
extract($_GET);

$isResultEmpty = true;
$categoryIndex = 0;
$termIndex = 0;
$storeCategories = get_terms( 'category-stores' );
foreach ( $storeCategories as $storeCategory ): 
	$taxArgs = array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'category-stores',
					'field' => 'id',
					'terms' => $storeCategory->term_id
				));
				
	//add country/city to the query
	if(isset($locationId)){
		$locationId = (int) $locationId;
		if($locationId > 0){
			array_push($taxArgs, array(
									'taxonomy' => 'countries', 
									'field' => 'id', 
									'terms' => $locationId
									));
		}
	}
	
	//add free search to the query
	if(isset($city) && $city !=''){
		array_push($taxArgs, array(
								'taxonomy' => 'countries', 
								'field' => 'slug', 
								'terms' => $city
								));
								
		//free search - search on post title
		$freeSearchArgs = array('numberposts' => -1,	
					'post_type' => 'stores', 
					'post_status'=> 'publish', 
					's'=>$city, 
					'tax_query' => array(
						'taxonomy' => 'category-stores',
						'field' => 'id',
						'terms' => $storeCategory->term_id
					), 
					'suppress_filters'=>0);
		//get stores on this category term with post title simlar to $city variable
		$freeSearchPosts = get_posts($freeSearchArgs);
	}	
	
	$args = array('numberposts' => -1,	'post_type' => 'stores', 'tax_query' => $taxArgs, 'suppress_filters'=>0);
	$storesPost = get_posts($args); 
	
	if(isset($city) && $city !=''){
		$storesPost = array_merge($storesPost, $freeSearchPosts);
	}
	
	
	if( $storesPost ): $isResultEmpty = false;
        if($termIndex==0) {
            $indexCount = '1';
            $image = 'marker_small_1.png';
        }else {
            $image = 'marker_small_0.png';
            $indexCount = '0';
        }

	?>
    
		<div class="address-block" data-index='<?=$indexCount?>' data-category='<?=$storeCategory->term_id?>' ><?php /*id="stores_block_<?=$termIndex?>" >*/ ?>
     	   <div class="wrap">
           	<div class="store-title">
            	<h2><img src="<?php echo get_template_directory_uri();?>/images/map-marker/<?=$image?>" /> <?=edox_get_translated_term_name($storeCategory, $lang)?></h2>
				<?php /*if(count($storesPost)>4): */?><!--
            	<button onclick="jQuery(this).parent().next().toggleClass('show-top-four');"><?php /*echo __('Show all stores'); */?></button>
				--><?php /*endif;*/ ?>
				</div>
			   <!--<div class="store-list-wrapper show-top-four">-->

			   <div class="store-list-wrapper">
            	<?php foreach ( $storesPost as $store ):?>
                <?php $location = get_field('stores_google_map_cordinates', $store->ID);
				//echo '<pre>';	var_dump($location);	echo '</pre>';				
				?>
	        <div class="sm-block" id='store_<?=$store->ID?>' data-latlng="<?=($location!='')?$location['lat'].','.$location['lng']:""?>">
            <address><h4 class="stitle"><?php echo get_the_title($store->ID);?></h4>
                <div class="saddress">
                    <p><?php the_field('stores_address', $store->ID)?></p>
                    <?php 
					$tel = get_field('stores_phone_number',$store->ID);
					$email = get_field('stores_mail',$store->ID);
					?>
                    <?php if($tel!=''): ?><label><span>Tel.</span> <?=$tel?></label><?php endif; ?>
                    <?php if($email!=''): ?><label><span>Mail.</span> <a href="mailto:<?=$email?>"><?=$email?></a></label><?php endif; ?>                    
                </div>
                <?php 
				//store_782
				if(is_array($location) && !empty($location)):
				if($location['lat'] != ''):
				?>
                <a onclick="window.showMarker('#store_<?=$store->ID?>')" class="btn btn-primary pull-right hidden-xs"><?php the_field('',$store->ID);?><?php _e('Locate'); ?></a>
                 <a href="https://www.google.com/maps/place/<?=$location['address']?>/@<?=$location['lat']?>,<?=$location['lng']?>" class="btn btn-primary pull-right visible-xs" target="_blank"><?php the_field('',$store->ID);?><?php _e('Locate'); ?></a>
                <?php endif; endif; ?>
            </address>
        </div>
	
		<?php endforeach; ?>
			</div>
            </div>
		</div>
	<?php 
	endif;
	$termIndex++;
	endforeach;
	
	if($isResultEmpty):?>
	    <div class="address-block">
     	   <div class=" wrap">
           	<div class="store-title"><h2>Zero results found</h2></div>   		
			</div>    
		</div>
    <?php endif;
	
?>
</div>


