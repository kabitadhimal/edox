<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 3/6/2019
 * Time: 1:13 PM
 */

if(empty($block['check_to_disable'])):
?>
<div class="media-lists">
    <?php
    $args = array('post_type'=>'press-reviews', 'posts_per_page'=>-1, 'orderby'=>'date','order'=>'DESC','suppress_filters'=>0);
    $pressReviewPosts = get_posts($args);
    $pressReviewPostsDateAry = array();
    foreach ($pressReviewPosts as $pressReview){
        $postYear =  strtok($pressReview->post_date, '-');
        if(!isset($pressReviewPostsDateAry[$postYear])) $pressReviewPostsDateAry[$postYear] = array();
        $pressReviewPostsDateAry[$postYear][] = $pressReview;
    }
    foreach ($pressReviewPostsDateAry as $year=>$presRevAry){
        ?>
        <div class="mymedia">
            <article class="mediaTitle">
                <h2><span><?=$year?></span></h2>
            </article>
            <?php
            foreach ($presRevAry as $pressReview):
                setup_postdata( $GLOBALS['post'] =& $pressReview );
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'press-img');
                $imageFull = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');

                if(!empty($image[0])) :
                    ?>
                    <article class="mediaMag">
                        <div class="mediaMag-fullHeight">
                            <figure data-toggle="modal" data-target="#mediaMag-modal_<?=$post->ID?>" style="background-image:url(<?php echo $image[0]; ?>)"></figure>
                            <div class="modal fade in" id="mediaMag-modal_<?=$post->ID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                                <div class="modal-dialog">
                                    <a type="button" class="close-btn pull-right glyphicon glyphicon-remove" data-dismiss="modal" aria-label="Close"></a>
                                    <div class="modal-content">
                                        <img src="<?php echo $imageFull[0]; ?>" alt="" />
                                        <h2 class="mediaMag--title"><?php the_title()?></h2>
                                        <?php //the_content();?>

                                        <span class="mediaMag--date"><?php //the_time('jS M Y'); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div><!--/.mediaMag-fullHeight-->
                    </article>
                <?php
                endif;
            endforeach;
            ?>
        </div>
        <?php
    }
    wp_reset_postdata();
    ?>
</div><!--/.media-lists-->
<?php endif;
