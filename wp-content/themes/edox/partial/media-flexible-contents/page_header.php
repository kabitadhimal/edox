<?php if(!empty($block['title']) || !empty($block['subtitle'])): ?>
<div class="header-section content-section bg-blue text-center">
    <div class="bg-line">
        <h1><?php echo $block['title'];?></h1>
        <h2><?php echo $block['subtitle']; ?></h2>
    </div>
</div>
<?php endif; ?>
