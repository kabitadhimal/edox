<?php
if(empty($block['check_to_disable'])): ?>
<style>
    .media_rollover{
        background: rgba(0, 0, 0, 0.9);
        position:absolute;
        top:0;
        left:-100%;
        width:100%;
        height:100%;
        -webkit-transition: all 0.3s ease;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        z-index:99;
    }
    .media_rollover a {
        display:block;
        height:100%;
        width:100%;
    }
    .media_rollover a:before {
        content:'';
        display:inline-block;
        height:100%;
        vertical-align:middle;
        width:0;
    }
    .media_rollover a div {
        display:inline-block;
        vertical-align:middle;
    }
    .media_rollover a h2{font-size:25px; color:#fff;}
    .media_rollover a i {font-size:20px;color:#fff;}
    .mediaMag-fullHeight:hover .media_rollover{left:0;}
</style>

<div class="media-lists">
    <div class="mymedia">
        <?php
        global $sitepress;
        $currentLang = $sitepress->get_current_language();
        $sitepress->switch_lang('en');

        $args = array(
            'type'                     => 'publicity',
            'suppress_filters'		   	=> 0,
            'child_of'                 => 0,
            'parent'				   => 0,
            'hide_empty'               => 1,
            'hierarchical'             => 0,
            'taxonomy'                 => 'countries-publicity',

        );
        $categories = get_categories( $args );
        //var_dump($categories);
        foreach($categories as $category) :
            $countryImageSrc = get_field('countries_upload_image', 'countries-publicity_'.$category->term_id);

            ?>
            <article class="mediaTitle bg-dark">
                <h2><span><img src="<?=$countryImageSrc?>"  id="taxImg" alt="<?=$category->name?>"/></span></h2>
            </article>

            <?php
            $taxArgs = array(
                'post_type' => 'publicity',
                'suppress_filters'=>0,
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'countries-publicity',
                        'field' => 'id',
                        'terms' => $category->term_id
                    ),
                ),
            );


            $query = new WP_Query( $taxArgs );

            while ( $query->have_posts() ) : $query->the_post();
                $imageFull = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'press-img');
                if(!empty($image[0])) :
                    ?>
                    <article class="mediaMag">
                        <div class="mediaMag-fullHeight">
                            <?php
                            //vip-img'
                            $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
                            if(!empty($image[0])) :
                                ?>
                                <figure style="background-image:url(<?php echo $image[0]; ?>)"></figure>
                                <div class="media_rollover">
                                    <?php $sliderId = rand();
                                    $images = get_field('gallery');
                                    if(empty($images)):
                                        ?>
                                        <a><div><h2 class="mediaMag--title"><?php the_title()?></h2></div></a>
                                    <?php
                                    else:

                                        foreach( $images as $image ):?>
                                            <a class="fancybox" href="<?php echo $image['url']; ?>" data-fancybox-group="gallery[<?=$sliderId?>]" title="">
                                                <div>
                                                    <h2 class="mediaMag--title"><?php the_title()?></h2>
                                                    <i class="fa fa-camera"></i>
                                                </div>
                                            </a>
                                        <?php endforeach;
                                    endif;
                                    ?>
                                </div>
                            <?php
                            else :
                                echo '';
                            endif;
                            ?>

                            <?php /*
				<figure data-toggle="modal" data-target="#mediaMag-modal_<?=$post->ID?>" style="background-image:url(<?php echo $image[0]; ?>)"></figure>
                <div class="modal fade in" id="mediaMag-modal_<?=$post->ID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
               		<div class="modal-dialog">
                    	<a type="button" class="close-btn pull-right glyphicon glyphicon-remove" data-dismiss="modal" aria-label="Close"></a>
                        <div class="modal-content">
                            <img src="<?php echo $imageFull[0]; ?>" alt="" />
                            <h2 class="mediaMag--title"><?php the_title()?></h2>
                            <?php //the_content();?>
                            <span class="mediaMag--date"><?php //the_time('jS M Y'); ?></span>
                        </div>
                    </div>
                </div>
				*/ ?>
                        </div><!--/.mediaMag-fullHeight-->
                    </article>
                <?php endif;?>

            <?php endwhile;    wp_reset_postdata();?>
        <?php endforeach;?>
    </div>

</div><!--/.media-lists-->
<?php endif;