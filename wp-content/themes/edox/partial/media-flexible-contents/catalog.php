<?php if(empty($block['check_to_disable'])): ?>
<div class="container">
    <div class="main-wrapper">
        <div class="cms-content">
            <?php if(!empty($block['title']) || !empty($block['subtitle'])): ?>
            <h1 class="section-title"><?php echo $block['title'];?></h1>
            <h2 class="section-title--sub"><?php echo $block['subtitle']; ?></h2>
            <?php endif; ?>
            <div class="fixed-width clearfix carousel slider-1-section slide row" data-interval='false' data-keyboard='false' data-ride="carousel" id="catlog-11">
                <ul class="carousel-inner">
                    <?php
                    $cssClass = 'active';
                    $count= 1;
                    $args = array('post_type'=>'catalog', 'posts_per_page'=>-1, 'orderby'=>'date','order'=>'DESC');
                    $query = new WP_Query($args);
                    while ($query->have_posts()):$query->the_post();

                        $setLi = ($count==1) ?'<li class="item '.$cssClass.'">':'';

                        echo $setLi;

                        $catalogUrl = get_field('catalog_pdf');
                        if( $catalogUrl != '') $catalogUrl = CATALOG_URL.$catalogUrl;
                        ?>
                        <a <?=($catalogUrl != '')?'href="'.$catalogUrl.'" target="_blank"': ''?>><figure>

                                <?php $catalogueImage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'catolog-img');
                                if(!empty($catalogueImage[0])):
                                    ?>
                                    <img src="<?=$catalogueImage[0]?>" alt="">
                                <?php endif;?>
                                <figcaption><?php the_title();?></figcaption>
                            </figure></a>
                        <?php


                        if($count==2) {
                            echo '</li>';
                            $count=0;
                        }
                        $count++;
                        ?>
                        <?php $cssClass=''; endwhile; wp_reset_postdata();?>
                </ul>

                <a class="left carousel-control" href="#catlog-11" role="button" data-slide="prev"></a>
                <a class="right carousel-control" href="#catlog-11" role="button" data-slide="next"></a>
            </div>
        </div>
    </div>
</div>
<?php endif;