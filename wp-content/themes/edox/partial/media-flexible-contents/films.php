<?php if(empty($block['check_to_disable'])): ?>

<div class="container hidden-xs">
    <div class="films">
        <?php if(!empty($block['title']) || !empty($block['subtitle'])): ?>
            <h1 class="section-title"><?=$block['title']?></h1>
            <h2 class="section-title--sub"><?=$block['subtitle']?></h2>
        <?php endif; ?>
        <ul class="nav nav-tabs" role="tablist">
            <?php
            $count=1;
            //$args = array('post_type'=>'films', 'posts_per_page'=>-1);
            $cssClass = 'active';
            $years = get_terms( 'category-year' );
            foreach ( $years as $year ):?>
                <li class="<?=$cssClass?>"><a onclick="closeVideo();" role="tab" data-toggle="tab" href="#film-<?=$year->term_id?>"><?=$year->name?></a></li>
                <?php
                $cssClass = '';
            endforeach;
            ?>
        </ul>


        <div class="tab-content">
            <div class="closeVideo" id="ytplayerClose" onclick="closeVideo();" style="display:none">Close Video</div>
            <?php
            $count = 1;
            $cssClass = ' in active' ;
            foreach ( $years as $year ):
                $taxArgs = array(
                    array(
                        'taxonomy' => 'category-year',
                        'field' => 'id',
                        'terms' => intval($year->term_id)
                    ));

                $args = array('numberposts' => -1,	'post_type' => 'films', 'tax_query' => $taxArgs, 'suppress_filters');

                ?>
                <div class="tab-pane <?=$cssClass?>" id="film-<?=$year->term_id?>">
                    <div class="carousel slide"  data-interval="false" data-keyboard="false" data-ride="carousel" id="film-item-<?=$year->term_id?>">

                        <div class="carousel-inner">
                            <?php

                            $videoTitle = get_field('video_title',$post->ID);
                            $cssClass = '';
                            $query = new WP_Query($args);
                            $itemCssClass = 'active';
                            while ($query->have_posts()):$query->the_post();
                                if(has_post_thumbnail()):
                                    ?>
                                    <div class="video item <?=$itemCssClass?>">
                                        <?php
                                        $itemCssClass ='';
                                        $featuredImage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
                                        <img src='<?=$featuredImage[0]?>' class="img-responsive" />


                                        <div class="overlay">
                                            <div class="caption">
                                                <div class="caption-text">
                                                    <h3><?php the_title()?></h3>
                                                    <h2><?php the_field('films_shorttitle');?></h2>
                                                    <p><?php echo $videoTitle;?></p>
                                                    <?php /*<a href="#" data-toggle="modal" data-target="#videoModal<?=$post->ID?>" class="play"><i class="fa fa-play"></i></a>*/ ?>
                                                    <a data-video-href="<?=get_field('films_youtube_video_link')?>" target="_blank" onclick="loadVideo('<?=get_field('films_youtube_video_link')?>')" class="play"><i class="fa fa-play"></i></a>
                                                </div><!--/.caption-text-->
                                            </div><!--/.caption-->
                                        </div><!--/.overlay-->
                                    </div><!--/.video-->

                                <?php

                                endif;
                            endwhile;
                            ?>
                        </div><!--/.carousel-inner-->
                        <?php if($query->post_count > 1): ?>
                            <a class="left carousel-control" href="#film-item-<?=$year->term_id?>" role="button" data-slide="prev">Previous</a>
                            <a class="right carousel-control" href="#film-item-<?=$year->term_id?>" role="button" data-slide="next">NEXT</a>
                        <?php endif; ?>
                    </div><!--/.carousel-->
                </div>
            <?php
            endforeach; ?>

            <div id="ytplayer"></div>

        </div>
    </div><!--/.films-->
</div><!--/.container-->
<?php endif;