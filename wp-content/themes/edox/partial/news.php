<?php 
$visible = get_field('visible'); 
if(empty($visible)) :
$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'newsimage' ); ?>
		<div class="box-sqr box-sqr-thumb">
	                <figure><img src="<?php echo $full_image_url[0] ?>" class="img-responsive"></figure>
					<div><div class="box-sqr-content">
                        <div class="caption text-center">
                              <h3><?php the_title()?></h3>
                              <?php echo '<p>'.kd_get_excerpt(180).'</p>';?><a href="<?php the_permalink();?>" class="btn btn-default">
                              </a>
                          </div>
					</div>
                    </div>
				</div>
<?php endif; ?>
<?php /*
<?php 
$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'newsimage' );
?>
<div class="col-md-4 col-sm-4 news_block news-item column" style="position:relative" >
	<img src="<?php echo $full_image_url[0] ?>" class="img-responsive">
  <div class="news_img"  style="position:absolute; top:0; ">
    <div class="align-wrapper">
      <div class="align-middle">
        <div class="caption">
          <h3><?php the_title()?></h3>
          <?php echo '<p>'.kd_get_excerpt(180).'</p>';?><a href="<?php the_permalink();?>" class="btn btn-default">
          <img src="<?php echo home_url();?>/wp-content/themes/edox/images/arrow.png"></a> </div>
      </div>
    </div>
  </div>
</div>
*/ ?>