<?php
$cache = get_procab_file_cache();
//$cacheBannerKey = $cache->buildKey('banner'); 
$cacheBannerKey = 'banner-'.strtolower(ICL_LANGUAGE_CODE); 
$cacheBannerData = $cache->restore($cacheBannerKey);
if($cacheBannerData): echo $cacheBannerData;
else:
    $cache->captureStart($cacheBannerKey);
?>
<div class="banner">
    <div id="carousel-banner" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      	   <?php 
      	   	$query = new WP_Query('post_type=home-slider');
      	   	$countSlider = 1;
      	   	while($query->have_posts()): $query->the_post();
      	   //$postCount = $query->post_count;
      	   
      	   	 $activeClass = ($countSlider==1)?'active': "";
      	   	
        		if ( has_post_thumbnail() ) {
        			//$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'bannerimage' );  
        			//var_dump($full_image_url);      			
        		?>
        		<div class="item <?php echo $activeClass;?>"><a href="<?php the_field('home_slider_link');?>">
				<?php the_post_thumbnail('full',['class'=>'img-responsive']); ?>
				<?php /*<img src="<?php echo $full_image_url[0]; ?>" class="img-responsive">*/ ?>
				</a><div class="caption wow animated fadeInRight animated">
				
        		<?php remove_filter ('the_content', 'wpautop'); ?>
					<?php the_content(); ?>
			   </div></div>
            <?php  }
            $countSlider++;
            endwhile; wp_reset_postdata();?>
    </div>    
    <!-- Controls -->
	<!-- <a class="left carousel-control" href="#carousel-banner" role="button" data-slide="prev"><i class="fa fa-3x fa-angle-left"></i></a>
    <a class="right carousel-control" href="#carousel-banner" role="button" data-slide="next"><i class="fa fa-3x fa-angle-right"></i></a> -->
	<!-- Indicators -->
	<ol class="carousel-indicators">
	<?php   
	$query = new WP_Query('post_type=home-slider');
     $count = 0;
	while($query->have_posts()): $query->the_post();
      	   //$count = $query->post_count;
      	   	 $activeClass = ($count==0)?'class="active"': "";
      	   	 ?>
			<li data-target="#carousel-banner" data-slide-to="<?php echo $count;?>" <?php echo $activeClass;?>></li>
	<?php 
		$count++;
	endwhile;
	wp_reset_postdata();
	?>
	</ol>
    </div>
    <a href="#content" class="scroll anchor"><?php _e("See the collections",'edox'); ?></a>
</div>
<?php
    echo $cache->captureEnd($cacheBannerKey);
endif;
?>
<?php /*
<script>

(function($){
//set responsive minheight for carousel;
	var $carouselBanner = $('#carousel-banner');
	function resizeCarousel(){
		var ratio = 1900/850;
		var newHeight = $(window).width() / ratio;
		$carouselBanner.css('min-height', newHeight+'px');
	}
	$(window).bind('resize', resizeCarousel);
	
	resizeCarousel();
	$carouselBanner.on('slide.bs.carousel', function () {
		$(window).unbind('resize', resizeCarousel);
		$carouselBanner.css('min-height', 'inherit');		
	})	
	
})(jQuery);
</script>
*/ ;?>

<?php 
        //global $currentLocation;
        $cache = get_procab_file_cache();
        //$cacheCollectionKey = $cache->buildKey('colection');

        $cacheCollectionKey = 'colection-'.strtolower(ICL_LANGUAGE_CODE).'-'.$currCountryCode;//.'-'.$currCountryCode;

        
        //if($currentLocation->countryCode=="US") $cacheCollectionKey .='-US';
        
        $cacheCollectionData = $cache->restore($cacheCollectionKey);
        if($cacheCollectionData): echo $cacheCollectionData;
        else:
            $cache->captureStart($cacheCollectionKey);
            /*
            if($currentLocation->countryCode=="US") :
                $collectionHomeUrl = site_url('/').'shop/index.php/us_en/categoryattribute/index/getHomeCollection/lang/us';
            elseif($currCountryCode == 'bs') :
                $collectionHomeUrl = site_url('/').'shop/index.php/bs_ba/categoryattribute/index/getHomeCollection/lang/bs';
	        elseif(strtoupper($currCountryCode)=='CH') :
	            $collectionHomeUrl = site_url('/').'shop/index.php/ch_fr/categoryattribute/index/getHomeCollection/lang/ch_fr';
            else :
            	$collectionHomeUrl = site_url('/').'shop/index.php/fr_fr/categoryattribute/index/getHomeCollection/lang/'.strtolower(ICL_LANGUAGE_CODE);
            endif;
            */
            $collectionHomeUrl = site_url('/').getStoreSliderUrl($storeCode);
            $collectionHomeHtml = @file_get_contents_curl($collectionHomeUrl);
            echo $collectionHomeHtml;
            
            echo $cache->captureEnd($cacheCollectionKey);
        endif;        
    
?>



<?php
$cacheHomeKey = $cache->buildKey('home');
$cacheHomeData = $cache->restore($cacheHomeKey);
if($cacheHomeData): echo $cacheHomeData;
else: 
    $cache->captureStart($cacheHomeKey);
		//Display Video
	  	$homePostId = my_icl_object_id(43, 'page');
	  	$homePost = get_posts('page_id='.$homePostId.'&post_per_page=1&post_type=page&suppress_filters=0');
	?>
  <div class="video hidden-xs" id="video">
  
  <?php
  
  	$videoImage = get_field('background_image', $homePost[0]->ID);
	$defaultVideoImage = get_template_directory_uri().' /images/video/1.jpg';
	
	
	$vImage = (!empty($videoImage)) ? $videoImage : $defaultVideoImage;
	
	

  ?>
    <img src="<?php echo $vImage;?>" class="img-responsive">
    <div class="overlay">
    <div class="caption">
    	<div class="caption-text">
        <h3 class="wow animated fadeInDown"><?php the_field('home_video_title', $homePost[0]->ID);?></h3>
        <h2 class="wow animated fadeInDown"><?php the_field('home_video_subtitle',$homePost[0]->ID);?></h2>
        <p class="wow animated fadeInDown"><?php the_field('home_video_play_button_title',$homePost[0]->ID);?></p>
        	<a href="#video" data-video-href="<?php the_field('home_video_link',$homePost[0]->ID);?>" target="_blank"  onclick="loadVideo('<?php the_field('home_video_link',$homePost[0]->ID);?>')" class="play wow animated fadeInDown"><i class="fa fa-play"></i></a>
    	</div>
    </div>   
    </div>
    <!-- video popup -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
         <button type="button" class="close-btn pull-right" data-dismiss="modal" aria-label="Close"><img src="<?php echo get_template_directory_uri();?>/images/cross.png"></button>
            <div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="<?php the_field('home_video_link',$homePost[0]->ID);?>" frameborder="0" allowfullscreen></iframe></div>
        </div>
    </div>
    <!-- video popup -->
	 <div class="closeVideo" id="ytplayerClose" onclick="closeVideo();" style="display:none">Close Video</div>
     <div id="ytplayer"></div>
  </div>
 
<div class="watch-section row widget-tbl" style="position:relative;">
	<div class="col-xs-6 full-height widget-tbl-cell hidden-xs">
	<?php $linkLeftPicture = get_field('home_left_picture_link',$homePost[0]->ID); ?>
	    <a href="<?=$linkLeftPicture?>"><img src="<?php the_field('home_left_picture', $homePost[0]->ID);?>" class="img-responsive"></a>
    </div>
    <div class="widget-fill widget-fill-push-50" style="background-color:black;">
        <div class="text-center widget-tbl widget-height-50 historie" style="position:relative">
			<div class="widget-tbl-cell">
	                <div class="caption wow animated fadeInDown">
                    	<h3><?php the_field('home_right_section_block1_title',$homePost[0]->ID);?></h3>
                        <p><?php the_field('home_right_section_block1_subtitle',$homePost[0]->ID);?></p>
					</div>
	                <div class="watch wow animated fadeInDown"><img src="<?php the_field('home_right_section_background_picture', $homePost[0]->ID); ?>" >
	                <span class="top wow animated fadeInLeft hidden-xs"><?php the_field('home_right_section_block1_title1',$homePost[0]->ID);?></span>
                    <span class="right wow animated fadeInRight hidden-xs"><?php the_field('home_right_section_block1_title2',$homePost[0]->ID);?></span>
	                </div>
	                <div class="watch-sec-btn wow animated fadeInDown"><a href="<?php the_field('home_right_section_button_block1_link', $homePost[0]->ID);?>" data-text="<?php _e('read more'); ?>" class="btn btn-primary "><?php _e('read more')?></a></div>
            </div>
        </div>
        <div class="text-center widget-tbl widget-height-50 world-of-edox" style="position:relative">
            <div class="widget-tbl-cell">
                <div class="caption wow animated fadeInDown">
                	<h3><?php the_field('home_right_section_block2_title',$homePost[0]->ID);?></h3>
                    <p><?php the_field('home_right_section_block2_subtitle',$homePost[0]->ID);?></p>
				</div>
                <div class="watch wow animated fadeInDown"><img src="<?php the_field('home_right_section_background_picture_1',$homePost[0]->ID); ?>" >
                <span class="right wow animated fadeInRight hidden-xs"><?php the_field('home_right_section_block2_title1',$homePost[0]->ID);?></span>
                <span class="left wow animated fadeInLeft hidden-xs"><?php the_field('home_right_section_block2_title2',$homePost[0]->ID);?></span>
                </div>
               <div class="watch-sec-btn wow animated fadeInDown"> <a href="<?php the_field('home_right_section_button_block2_link',$homePost[0]->ID);?>" data-text="<?php _e('read more');?>" class="btn btn-primary"><?php _e('read more');?></a></div>
            </div>
        </div>            
    </div>
</div>
  
  
<div class="half-image-text-section text-center bg-black ">  
	<div class="row">
	<?php
	$query = new WP_Query('post_type=vip&posts_per_page=2');
	while ($query->have_posts()):$query->the_post();
	$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'partnership-img');
	?>
    <div class="col-sm-6">
	<div class="row row-vertical">
    	<div class="col-xs-6"><a href="<?php the_field('article_link');?>" ><img src=" <?php echo $image[0]; ?>" class="img-responsivex" style="width:100%"></a></div>
    	<div class="col-xs-6 teaser wow animated fadeInDown">
        	<img src="<?php the_field('vip_sponsor_logo');?>" class="img-responsivex logox hidden-sm"  style="width:auto; max-width:100%">
            <?php the_title('<h1>','</h1>')?>
            <p><?php the_field('vip_subtitle');?></p>
			<a href="<?php the_field('article_link');?>"  class="btn-arrow"></a>
        </div>        
    </div>
    </div>
    <?php endwhile; wp_reset_postdata();?>
    </div>
</div>

<div class="news-section-container">
<div class="news-section">
<?php 
$query = new WP_Query('posts_per_page=2');
$count = 1;
while($query->have_posts()): $query->the_post();
	if (has_post_thumbnail()):
	$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	$content = get_the_content();
	$newsDateLabel = '';
	$dateFrom = get_field('news_from'); $dateTo = get_field('news_to');
	
	if(!empty($dateFrom)){
		$dateFrom = DateTime::createFromFormat('Ymd', $dateFrom);
		$newsDateLabel = __("From").' '.$dateFrom->format('M. d');
	}

	if(!empty($dateTo)){
		$dateTo = DateTime::createFromFormat('Ymd', $dateTo);
		if($newsDateLabel != '') $newsDateLabel .= ' ';
		$newsDateLabel .= __("to").' '.$dateTo->format('M. d');
	}
	
	if($count%2==0):
?>
    <div class="row row-vertical">
        <figure class="col-sm-6 col-xs-6">
        	<a href="<?php the_permalink();?>" >
        	<img src="<?php echo $full_image_url[0] ?>" class="img-responsive"></a></figure>    
        <div class="col-sm-6 col-xs-6 text-center wow animated fadeInDown">
            <h4 class="wow animated fadeInDown"><?=$newsDateLabel?> <span>News </span> </h4> <?php the_title('<h3>','</h3>');?>
            <?php echo '<p>'.get_field('short_description').' ...</p>';?>
            <a href="<?php the_permalink();?>"  class="btn-arrow"></a>
            <!--<img src="<?php echo get_template_directory_uri(); ?>/images/news-arrow-right.png" class="news-arrow-right">-->
        </div>
    </div>
<?php
	else:
?>
    <div class="row row-vertical">
        <div class="col-sm-6 col-xs-6 text-center wow animated fadeInDown">
            <h4 class="wow animated fadeInDown"><?=$newsDateLabel?> <span>News </span></h4><?php the_title('<h3>','</h3>');?>
            <?php //echo '<p>'.truncate($content, 20, 150).' ...</p>'; ?>	
			<?php echo '<p>'.get_field('short_description').' ...</p>';?>
            <a href="<?php the_permalink();?>"  class="btn-arrow"></a>
            <!--<img src="<?php echo get_template_directory_uri(); ?>/images/news-arrow-right.png" class="news-arrow-right">-->
        </div>
        <figure class="col-sm-6 col-xs-6"><a href="<?php the_permalink();?>" ><img src="<?php echo $full_image_url[0] ?>" class="img-responsive"></a></figure>
    </div>
<?php
	endif;
?>
<?php $count++;  endif; endwhile;?>
</div>
</div>

<?php
    echo $cache->captureEnd($cacheHomeKey);
endif;
?>


<!-- social -->
<div id="socialHome" class="social text-center" data-stellar-background-ratio="0.5">
    <div class="wrapper">
      <?php /*<h4><?php _e("CONNECT WITH US & share");?></h4>*/ ?>
      <h2><?php _e("CONNECT & SHARE");?></h2>
    </div>
	<div class="social-nav">
  <ul>
    <li> <span class="nav-label"><a href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819" target="_blank"><i class="fa fa-facebook-square"></i></a></span> </li>
    <li> <span class="nav-label"><a href="https://twitter.com/edoxwatches" target="_blank"><i class="fa fa-twitter"></i></a></span> </li>
    <li> <span class="nav-label"><a href="https://instagram.com/edoxswisswatches/" target="_blank"><i class="fa fa-instagram"></i></a></span> </li>
    <?php /*
    <li> <span class="nav-label"><a href="https://www.tumblr.com/tagged/edox" target="_blank"><i class="fa fa-tumblr-square"></i></a></span> </li>
	*/ ?>
    <li> <span class="nav-label"><a href="https://www.youtube.com/user/EdoxWatches" target="_blank"><i class="fa fa-youtube"></i></a></span> </li>
    <li> <span class="nav-label"><a href="https://www.pinterest.com/edoxwatches/" target="_blank"><i class="fa fa-pinterest-square"></i></a></span> </li>
  </ul>
</div>    
	<section class="social-contr social-home section-para">
	<div class="social-col-one">
		<div id="tumble_section"></div>
        <section style="clear:both; position:relative; margin-top:38px;">
        
        	<?php
        	$cacheInstaKey = 'insta';
        	$cacheInstaData = $cache->restore($cacheInstaKey, 'static');
        	if($cacheInstaData): echo $cacheInstaData;
        	else:
        	   $cache->captureStart($cacheInstaKey, 'static', 259200); //seconds
        	?>
	        <div id="instagram_section" style='z-index:1;' class="dcsns"><a class="feed-icon feed-insta"></a>

<?php $instaItems = getInstagramFeeds(1);
function getInstaHtml($instaItem){
	return '<li class="dcsns-li dcsns-instagram dcsns-feed-0"><div class="inner"><span class="section-thumb"><a href="'.$instaItem[0].'"  target="_blank"><img src="'.$instaItem[1].'" alt=""></a></span><span class="clear"></span></div></li>';
}
 ?>
 <div class="dcsns-content">
 <ul class="feed-ul">
    <li class="first_colmn" data-type="sprite" data-speed="0.6">
	    <ul class="stream">
        	<?php if($instaItems[0]) echo getInstaHtml($instaItems[0]); ?>
	    </ul>
    </li>
</ul>
</div>
            </div>
	        <?php
	           echo $cache->captureEnd($cacheInstaKey);
            endif;
	        ?>
	        
	        <div id="facebook_section">
            	<div id="fb-content"></div>
            	<a class="feed-icon feed-fb"></a>
            </div>
		</section>
	</div>
    <div class="social-col-two">
		<div id="twitter_section"><a class="feed-icon feed-twitter"></a></div>    
		<div id="youtube_section"  data-type='sprite' data-speed='0.2'><div id="youtube-content"></div><a class="feed-icon feed-ytube"></a></div>
		<div id="pinterest_section"><div id='pinterest-content' data-type='sprite' data-speed='0.6'></div></div>
	</div>
</section>    
</div>



<script>
//remove collection gallery for mobile
if(screen.width < 768){
(function($){
	$('.collection-section .collect-wrap > a').each(function(index, element) { var $self = $(this);	$self.appendTo($self.parent().parent().attr('class','text-center'))  });
	$('.collection-section li div, .collection-section li img').remove();
})(jQuery);
}
</script>
<style>
.bg-black { background-color:#25262a; color:white;}

.news-section-container{background:#e4e4e4; position:relative;/*z-index:2*/}
.news-section figure{position:relative}
.news-section figure:after{
	content: '';
	display: block;
	position: absolute;
	background:url(<?php echo get_template_directory_uri(); ?>/images/news-arrow-right.png);
	width:31px;
	height:62px;
	left:0;
	z-index: 1;


	top: 50%;
	margin-top:-31px;
}

.news-section .row-vertical:last-child figure:after{
	background:url(<?php echo get_template_directory_uri(); ?>/images/news-arrow-left.png);
	right:0; left:auto;
}

</style>
<?php /*<link rel='stylesheet' id='history-css'  href='<?php echo get_template_directory_uri();  ?>/assets/social.css' type='text/css' media='all' />*/ ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/social/jquery.social.stream.wall.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/assets/social/jquery.social.stream.1.5.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/js/social-parallax.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/js/social-feeds.js?ver=1.3.4"></script>

<script src="<?php echo get_template_directory_uri();  ?>/js/yt_video.js"></script>


<script>
var para = new parallax();
//loadFbFeed($('#fb-content'), 1);
$('#fb-content').addClass('dcsns').load(themeUrl+'/libs/fb-feed.php');
loadTwitterFeed($('#twitter_section'), 1);
//loadInstaFeed($('#instagram_section'), 1);
loadYouTubeFeed($('#youtube-content'));
loadPinterestFeed($('#pinterest-content'));
</script>

<div class="visible-xs social-mobile">
<h2><a href="<?php echo site_url('contact') ?>">Contact Us</a></h2>
<div class="twitter-mobile"><a target="_blank" href="https://twitter.com/edoxwatches"><i class="fa fa-twitter"></i></a></div>
<div class="instagram-mobile"><a target="_blank" href="http://instagram.com/edoxswisswatches"><i class="fa fa-instagram"></i></a></div>
 <div class="youtube-mobile"><a target="_blank" href="https://www.youtube.com/user/EdoxWatches" ><i class="fa fa-youtube"></i></a></div>
 <?php /*
  <div class="tumblr-mobile"><a href="https://www.tumblr.com/tagged/edox"><i class="fa fa-tumblr-square"></i></a></div>
  */ ?>
 <div class="fb-mobile"><a target="_blank" href="https://www.facebook.com/pages/Edox-Swiss-Watches/156587182819"><i class="fa fa-facebook-square"></i></a></div>
 <div class="pinterest-mobile"><a target="_blank" href="http://www.pinterest.com/edoxwatches/"><i class="fa fa-pinterest-square"></i></a></div>
</div>

<?php /*
<script>
(function(){
	if(isiOS || window.screen.width <= 767) return;
//automate video play
var $video = $('#video');
var $window = $(window);
function checkVideoVisible(){
	var _videoTop = $video.position().top/$body.height();
	if(isScrolledIntoView($video)){
		$video.find('.play').click();
		$window.unbind('scroll', checkVideoVisible);
		$video = null;
		$window = null;		
	}
	//console.log('serachasfdasdf')
}
function isScrolledIntoView($elem){
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();
    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();
    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}
$window.bind('scroll', checkVideoVisible);
})();
</script>
*/ 

