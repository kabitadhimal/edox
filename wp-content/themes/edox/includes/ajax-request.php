<?php

/*
 * http://192.168.0.213/edox/wp-admin/admin-ajax.php?action=edox_menu&lang=fr
 */
add_action('wp_ajax_edox_menu', 'edox_menu_callback');           // for logged in user
add_action('wp_ajax_nopriv_edox_menu', 'edox_menu_callback');   // if user is not logged in


add_action('wp_ajax_edox_footer', 'edox_footer_callback');
add_action('wp_ajax_nopriv_edox_footer', 'edox_footer_callback');

//news
add_action('wp_ajax_edox_news', 'edox_news_callback');
add_action('wp_ajax_nopriv_edox_news', 'edox_news_callback');

add_action('wp_ajax_edox_vipnews', 'edox_vipnews_callback');
add_action('wp_ajax_nopriv_edox_vipnews', 'edox_vipnews_callback');


function edox_vipnews_callback()
{

	$lang = getLangFromQuery();
	global $sitepress;
	$sitepress->switch_lang($lang );
	$posts = get_posts(array(
		'post_type' => 'post',
		'posts_per_page' => 1,
    	'meta_query' => array(
	        array(
            	'key' => 'visible', // name of custom field
            	'value' => '"yes"', // matches exactly "red"
            	'compare' => 'LIKE'
        	)
    	)
	));
	if (empty($posts))
	{	
		$recentPost = get_posts(array(
			'post_type' => 'post',
			'posts_per_page' => 1,
		));
		$displayPost = $recentPost[0];
	}
	else
	{
		$displayPost = $posts[0];
	}
	setup_postdata( $GLOBALS['post'] =& $displayPost );
	$full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'newsimage' ); ?>

	<div class="dashboardblock ecclatestnews whitebg coverbg" style="background-image:url('<?php echo $full_image_url[0] ?>');">
    	<div class="align-wrapper">
			<div class="align-bottom">			
               	<img src="<?php echo get_template_directory_uri() ?>/images/latestnews-icon.png" />
               	<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
          </div>
        </div>
    </div>	
	<?php
	wp_reset_postdata(); 
	die();
}


function edox_menu_callback(){
	//echo "I am lang";
	$lang = getLangFromQuery();
	global $sitepress;
	$sitepress->switch_lang($lang);
	
	$menuArgs = array(
		'theme_location'  => 'primary',
		'menu'            => '',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '<span class="menu-span">',
		'link_after'      => '</span>',
		'items_wrap'      => '%3$s',//<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		);
	echo wp_nav_menu($menuArgs);
	
		$menuArgs = array(
		'theme_location'  => 'top-menu',
		'menu'            => '',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'side-nav list-inline menu-block',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		);
		echo wp_nav_menu($menuArgs);
	die();
}

function edox_footer_callback() {
	$lang = getLangFromQuery();
	global $sitepress;
	$sitepress->switch_lang($lang);
	global $post;
	//include 'footer_content_magento.php';
	include 'footer_content.php';
	die();
}


function edox_news_callback(){
	$lang = getLangFromQuery();
	global $sitepress;
	$sitepress->switch_lang($lang);
	
	$offset = $_GET['offset'];
	$newsItems = getNews(11, $offset);
	foreach($newsItems as $news):
		setup_postdata( $GLOBALS['post'] =& $news );
		get_template_part('partial/news');
	endforeach;
	wp_reset_postdata();
	die();
}


//point of sale ajax
add_action('wp_ajax_sale_country', 'sale_country_callbck');
add_action('wp_ajax_nopriv_sale_country', 'sale_country_callbck'); 
			
function  sale_country_callbck() {
	extract($_GET);		
	$lang = getLangFromQuery();
	global $sitepress;
	$sitepress->switch_lang('en');//$lang);
		
	if($action == 'sale_country'){		
		//get cities
		$args = array(
			'parent' 	           => $locationId,
			'orderby'                  => 'name',
			'order'                    => 'ASC',
			'hide_empty'               => 0,
			'hierarchical'             => 1,
			'taxonomy'                 => 'countries'
				
		);
		$categoriesT = get_categories( $args );
		$categories = sortCategories($categoriesT);
		
		echo '<div id="ajx-cities">';
		echo '<option value="-1">'.__('select city').'</option>';
		foreach($categories as $category):	?>
			<option value="<?=$category->term_id?>"><?=$category->name?></option>
		<?php endforeach;
		echo '</div>';
	}
	
	include __DIR__.'/../partial/stores-by-location.php';
	die();
}

add_action('wp_ajax_sale_city','sale_country_callbck');
add_action('wp_ajax_nopriv_sale_city', 'sale_country_callbck'); 

//text city search
add_action('wp_ajax_sale_search','sale_country_callbck');
add_action('wp_ajax_nopriv_sale_search', 'sale_country_callbck'); 