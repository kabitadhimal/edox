(function() {
    tinymce.create('tinymce.plugins.wp_procab', {
    	init : function(ed, url) {
  
    		
    		 ed.addButton('header', {
                 title : 'Add Header',
                 cmd : 'header',
                 text:'Header'//,
                 //image : url + '/imagepath.jpg'
             });	
    		 
    		  ed.addButton('teaser', {
                  title : 'Add Teaser',
                  cmd : 'teaser',
                  text:'Teaser'//,
                  //image : url + '/imagepath.jpg'
              });	
    		  
    		  
            ed.addButton('introduction', {
                title : 'Add Introduction',
                cmd : 'introduction',
                text:'Introduction'//,
                //image : url + '/imagepath.jpg'
            });	
            
          
            
            ed.addButton('btn-box',{
            	title : 'Button Box',
            	cmd	: 'btn-box',
            	text : 'Button Box'
            });
            
            
            ed.addButton('btn-arrow',{
            	title : 'button arrow',
            	cmd	: 'btn-arrow',
            	text : 'Button Transparent'
            });
            
            ed.addButton('btn-transparent',{
            	title : 'Add Button Transparent',
            	cmd	: 'btn-transparent',
            	text : 'Button Arrow'
            });
            
            
            ed.addButton('one-two-column-image',{
            	title : 'Two images in a row',
            	cmd	: 'one-two-column-image',
            	text : 'Column Images'
            });
           
            ed.addButton('sliders', {
                title : 'Select Slider to Display',
                cmd : 'slider-click-handler',
                text:'Slider'//,
                //image : url + '/imagepath.jpg'
            });
			
            
            
            ed.addButton('banner', {
                title : 'Add Banner',
                cmd : 'banner',
                text:'Banner'//,
                //image : url + '/imagepath.jpg'
            });			
            
            ed.addButton('add-image-text', {
                title : 'Add image and text in column',
                cmd : 'add-image-text',
                text:'Image and text'//,
                //image : url + '/imagepath.jpg'
            });
            
            ed.addButton('one-two-column',{
            	title : 'Add Column 1/2',
            	cmd	: 'one-two-column',
            	text : 'Column 1/2'
            });
            
            //quotation
            ed.addButton('quotation',{
            	title : 'Add Quotation',
            	cmd	: 'quotation',
            	text : 'Quotation'
            });
			

			
			//Slider
			  ed.addCommand('slider-click-handler', function() {
	                //var selected_text = ed.selection.getContent();
					//ed.execCommand('mceInsertContent', 0, '<br />[fw-wrapper]<br />[fw-column]---add content here---[/fw-column]<br />[fw-column]---add content here---[/fw-column]<br />[/fw-wrapper]');
					//var range = ed.selection.getRng();
					ed.windowManager.open({
						file : url + '/../../includes/slider-object-shortcode-form.php', //This line
						width : 450 + parseInt(ed.getLang('example.delta_width', 0)),
						height : 450 + parseInt(ed.getLang('example.delta_height', 0)),
						inline : 1
					}, {
						plugin_url : url
					});
	            });
			  
            //Page title
            ed.addCommand('header', function() {
				ed.execCommand('mceInsertContent', 0, '[header]--add text here--[/header]<br />');
            });
            
            //Introduction
			ed.addCommand('introduction', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[introduction]<br/>---add text here--<br/>[/introduction]');
            });
			
			
			//Teaser
			ed.addCommand('teaser', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[teaser]--add teaser here --[/teaser]');
            });
			
			//button
            ed.addCommand('btn-arrow', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[btn type="arrow" href="--link--" target=""][/btn]');
            });
			
			
            //Button Box
            ed.addCommand('btn-box', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[btn type="box" href="--link--" target="" ]-- add Label here--[/btn]');
            });	
            
            ed.addCommand('btn-transparent', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[btn type="transparent" href="--link--" target=""][/btn]');
            });
            
           
			//Header seperator
			ed.addCommand('banner', function() {
				ed.execCommand('mceInsertContent', 0, "<br/>[banner]--add banner image here-- [/banner]");
            });
			
			ed.addCommand('add-image-text', function() {
				ed.execCommand("mceInsertContent", 0, "<br/>[row-img]<br />[col-img img='--add image here --']--add text here--[/col-img]<br />[col-img img='--add image here --' ]---add text here---[/col-img]<br />[/row-img]");
            });
			
			ed.addCommand('one-two-column', function() {
				//ed.execCommand('mceInsertContent', 0, '<br/>[row-2]<br />[column-1]---add content here---[/column-1]<br />[column-1]---add content here---[/column-1]<br />[/row-2]<br />');
				ed.execCommand('mceInsertContent', 0, '<br/>[row class="half-text-section content-section"]<br />[col]---add content here---[/col]<br />[col]---add content here---[/col]<br />[/row]<br />');
            });
			
			ed.addCommand('one-two-column-image', function() {
				//ed.execCommand('mceInsertContent', 0, '<br/>[row-3]<br />[column-1]---add image here---[/column-1]<br />[column-1]---add image here---[/column-1]<br/>[/row-3]');
				ed.execCommand('mceInsertContent', 0, '<br/>[row class="full-width-extend"]<br />[col]---add content here---[/col]<br />[col]---add content here---[/col]<br />[/row]<br />');
            });
			
			ed.addCommand('quotation', function() {
				ed.execCommand('mceInsertContent', 0, '<br/>[quote]<br />--add quotation here--<br/>[/quote]');
            });
			
        },
 
        createControl : function(n, cm) {
            return null;
        },
 
        getInfo : function() {
            return {
                longname : 'Wp Procab Buttons'
            };
        }
    });
 
    tinymce.PluginManager.add( 'wp_procab', tinymce.plugins.wp_procab );
})();