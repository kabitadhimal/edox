<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 12/18/2017
 * Time: 6:56 PM
 */


/**
 * @param $lang
 * @param $location
 * @return string
 * http://www.nirsoft.net/countryip/fr.html
 */
function getStoreByLangAndLocation($lang, $location){
    $location = strtolower($location);
    $lang = strtolower($lang);

    //var_dump("search store for lang:{$lang} - location:{$location}");
    /**
     * 'lang' => [
     *              'location1' => 'store_id',
     *              'location2' => 'store_id',
     *              'location3' => 'store_id',
     * ]
     */
    $defaultStoreCode = 'en';
    $stores = [
        /*
        'au' => [
            'au' => 'au_au',
            'default' => 'au_au',
        ],
        */
        'bs' => [
            'bs' => 'bs_ba',
            'default' => 'bs_ba'
        ],
        'zh-hant' => [
            'cn' => 'cn_zh',
            'default' => 'cn_zh'
        ],
        'en' => [
            'cn' => 'cn_en',
            'fr' => 'fr_en',
            'de' => 'de_en',
            'at' => 'au_au',
            'es' => 'es_en',
            'ch' => 'ch_en',
            'default' => 'en',
        ],
        'es' => [
            'es' => 'es_es',
            'default' => 'es_es',
        ],
        'fr' => [
            'fr' => 'fr_fr',
            'ch' => 'ch_fr',
            'default' => 'fr_fr',
        ],
        'de' => [
            'de' => 'de_de',
            'ch' => 'ch_de',
            'default' => 'de_de'
        ]
    ];

    if(array_key_exists($lang, $stores)){
        $storeData = $stores[$lang];
        if(array_key_exists($location, $storeData)){
            return $storeData[$location];
        }else{
            //echo "store found for the lang {$lang} but couldnot found for {$location}";
            return $storeData['default'];
        }
    }else{
        //echo 'default language';
        return $defaultStoreCode;
    }return $store;
}


function getStoreCollectionUrl($storeCode){
    return "shop/index.php/{$storeCode}/categoryattribute/index/getMenuCollection/lang/{$storeCode}";
}

function discoverCollectionUrl($storeCode) {
    return "shop/index.php/{$storeCode}/categoryattribute/index/getHomePageCollection/lang/{$storeCode}";
    //return "shop/index.php/{$storeCode}/categoryattribute/index/getMenuCollection/lang/{$storeCode}";
}
function getStoreSliderUrl($storeCode){
    return "shop/index.php/{$storeCode}/categoryattribute/index/getHomeCollection/lang/{$storeCode}";
}


