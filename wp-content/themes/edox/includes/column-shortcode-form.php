<?php //require_once('../../../../wp-load.php'); ?>
<!DOCTYPE html><head>
	<title>Add Column for Image or text</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>


<link rel='stylesheet' href='http://192.168.0.213/gilles/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load=dashicons,admin-bar,buttons,media-views,wp-admin,wp-auth-check,wp-color-picker&amp;ver=4.0' type='text/css' media='all' />    
    
	<script type="text/javascript">
		//More JS Here Later
	</script>
</head>
<body>

<div style="padding:20px;">

<div class="widget-content">
    <p><input id="column_image_type" name="column_image_type" checked type="checkbox">&nbsp;<label for="column_image_type">Is this image ?</label></p>
    <p id="column_image">
    	<label for="column_image_path">Image:</label><input class="widefat" id="column_image_path" name="column_image_path" type="url">
        <a href="#" id="column_image_picker" >select image</a>
	    <img src="" id="column_image_preview" style="width:70px; height:auto;" />
    </p>
    <p id="column_image_content_grp"><textarea class="widefat" rows="4" cols="20" id="column_image_content" name="column_image_content"></textarea></p>

</div>
	<div><input type="submit" name="savewidget" id="submit" class="button button-primary" onClick="javascript:Column.insert(Column.e)" value="update editor"></div>
<form id="Shortcode"></form>
</div>


<script>
	var custom_uploader;
	var $imgPicker = $('#column_image_picker');
	var $imgInput = $('#column_image_path');
	var $imgPreview = $('#column_image_preview');
	var $imgContent = $('#column_image_content');
	
	/*
	$('#image_type').toggle(function(){
		
		}, function(){
		
	});
	*/
	$('#column_image_type').click(function() {
	    var $this = $(this);
	    // $this will contain a reference to the checkbox   
	    if ($this.is(':checked')) {
	        $('#column_image').show();	
	        $('#column_image_content_grp').hide();			        
	    } else {
			$('#column_image').hide();
			$('#column_image_content_grp').show();
	    }
	});
		
	$imgPicker.click(function(e) {
	    //If the uploader object has already been created, reopen the dialog
	    if (custom_uploader) {
	        custom_uploader.open();
	        return;
	    }
	
	    //Extend the wp.media object
	    custom_uploader = parent.wp.media.frames.file_frame = parent.wp.media({
	        title: 'Choose Image',
	        button: {
	            text: 'Choose Image'
	        },
	        multiple: false
	    });
	    
	    //When a file is selected, grab the URL and set it as the text field's value
	    custom_uploader.on('select', function() {
	        attachment = custom_uploader.state().get('selection').first().toJSON();
			$imgPreview.attr('src', attachment.url)
			$imgInput.val(attachment.url);
			
	    });
	    custom_uploader.open();
	});
</script>
<script type="text/javascript">
var Column = {
	e: '',
	init: function(e) {
		Column.e = e;
		tinyMCEPopup.resizeToInnerSize();
	},
	insert: function createMapShortcode(e) {
		var output = '';
		if($('#column_image_type').is(':checked')){
			//output = "[square img='"+$imgInput.val()+"' ][/square]"
			output = "[column]";
			output += $imgInput.val();
			output += "[/column]<br />";
		}else{
			output = "[column]"+$imgContent.val()+"[/column]<br />"
		}
		tinyMCEPopup.execCommand('mceReplaceContent', false, output);
		tinyMCEPopup.close();
	}
}
tinyMCEPopup.onInit.add(Column.init, Column);
</script>
</body>
</div>