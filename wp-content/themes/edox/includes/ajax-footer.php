<?php

/*
 * http://192.168.0.213/edox/wp-admin/admin-ajax.php?action=edox_footer&lang=fr
 */

add_action('wp_ajax_edox_footer', 'edox_footer_callback');
add_action('wp_ajax_nopriv_edox_footer', 'edox_footer_callback');

function edox_footer_callback() {
	
	$langs = array('en','zh-hant','fr','de','es','ar','ko','pl');
	$lang = 'en';
	if(isset($_GET['lang'])) {
		$lang= strtolower('lang');
		if(!in_array($lang, $langs)) $lang= 'en';
	}
	
	global $sitepress;
	$sitepress->switch_lang($lang);
	
	$footerPostId = my_icl_object_id(50, 'page');
	$footerPost = get_posts('page_id='.$footerPostId.'&post_per_page=1&post_type=page');
	
	var_dump($footerPost);
	//$footerPost = $footerPost[0];
	?>
	<!-- special -->
	<div class="special">
	<div class="wrapper">
	  <div class="row">
	    <?php
		foreach( $footerPost as $post ): setup_postdata($post);	?>
	    <div class="col-sm-3 text-left"><img src="<?php the_field('picto_1'); ?>"> <?php the_field('picto_text_1'); ?> </div>
	    <div class="col-sm-3 text-center"><img src="<?php the_field('picto_2'); ?>"> <?php the_field('picto_text_2'); ?></div>
	    <div class="col-sm-3 text-center"><img src="<?php the_field('picto_3') ?>"> <?php the_field('picto_text_3'); ?></div>
	    <div class="col-sm-3 text-center"><img src="<?php the_field('picto_4') ?>"> <?php the_field('picto_text_4'); ?></div>
	    <?php endforeach;?>
	  </div>
	</div>
	</div>
	<!-- special -->
	<!-- footer -->
	<div class="footer">
	<div class="wrapper">
	  <div class="row">
	    <div class="col-sm-3">
		<?php foreach( $footerPost as $post ): setup_postdata($post);	?>
	      <h5><?php the_field('home_contact_title');?></h5>
	     <?php the_field('home_contact_details');?>
	    <?php endforeach;?>
	     
	    </div>
	    <div class="col-sm-3"><h5>Our Collection</h5>
	    <ul class="list-unstyled" id="footer-coll">
	    </ul>
	    
	    <script>
		jQuery('#menu-menu .collection .sub-menu:first > li > a').each(function(index){
			if(index < 3){
				var $list = jQuery('<li />').appendTo(jQuery('#footer-coll'));
				jQuery(this).clone().appendTo($list);
			}
		});
		</script>
	    </div>
		<?php foreach( $footerPost as $post ): setup_postdata($post);
			if($postItems = get_field('block_a_page_listing')): ?>
			    <div class="col-sm-3"><h5><?php the_field('block_a_title');?></h5>
			     <ul class="list-unstyled">
			    <?php 	$count=1; foreach( $postItems as $postItem): // variable must be called $post (IMPORTANT) ?>
			       <?php if($count==4) break; ?>
			        <li><a href="<?=get_permalink($postItem); ?>"><?=get_the_title($postItem);?></a></li>
			    <?php $count++; endforeach; ?>
			    </ul>
			    </div>
		<?php endif; endforeach;?>
		
		
		<?php foreach( $footerPost as $post ): setup_postdata($post);
			if($postItems = get_field('block_b_page_listing')): ?>
			    <div class="col-sm-3"><h5><?php the_field('block_b_title');?></h5>
			     <ul class="list-unstyled">
			    <?php 	$count=1; foreach( $postItems as $postItem): // variable must be called $post (IMPORTANT) ?>
			       <?php if($count==4) break;?>
			        <li><a href="<?=get_permalink($postItem); ?>"><?=get_the_title($postItem);?></a></li>
			    <?php $count++; endforeach; ?>
			    </ul>
			    </div>
		<?php endif; endforeach;?>
	  </div>
	  <div class="row">
	    <div class="col-sm-6 footer-map">
	      	
	    	<h4><a href="#" data-toggle="modal" data-target="#languageModal"><?php the_field('map_title', $footerPost[0]->ID);?></a></h4>
	
	    	<!-- popup -->
	    	<div class="modal fade pop-map" id="languageModal" tabindex="-1" role="dialog">
	    		<div class="modal-dialog">
	    		<div class="modal-content">
	    		<h4><?php the_field('country_lang_title', $footerPost[0]->ID);?></h4>
	    		<div class="row map-section">
	                <div role="tabpanel">                       
	                         <div class="col-sm-6 list-small">
	                            <!-- Nav tabs -->
	                            <ul class="list-unstyled" role="tablist">
	                             <?php foreach( $footerPost as $post ): setup_postdata($post);
		                            	if(get_field('country_and_language')):  
		                            	$count=1;
											while(has_sub_field('country_and_language')): 
				                            if(get_field('country_and_language')):				                            
				                            	$country = get_sub_field('country');
												$spaceRemoved = str_replace(" ","",$country);
												$backRemoved = str_replace("/","",$spaceRemoved);
		                            			$countryLower = strtolower($country);
		                            			$class = ($count==1 ? 'class="active"' : '');
		                            		?>
		                            		 <li role="presentation" <?php echo $class; ?>><a href="#<?php echo $backRemoved;?>" role="tab" data-toggle="tab"><?php echo $country; ?></a></li>
		                            		<?php 
				                            endif;
				                            $count++;
		                            	endwhile;
									endif; endforeach;?>
	                            </ul>
	                        </div>
	                       
	                        <div class="col-sm-6 list-big">
	                            <!-- Tab panes -->
	                            <div class="tab-content">
									<?php foreach( $footerPost as $post ): setup_postdata($post);
		                            	if(get_field('country_and_language')):  
		                            	$count=1;
											while(has_sub_field('country_and_language')): 
				                            if(get_field('country_and_language')):   
				                            	$country = get_sub_field('country');
		                            			$spaceRemoved = str_replace(" ","",$country);
												$backRemoved = str_replace("/","",$spaceRemoved);
		                            			$countryLower = strtolower($country);
		                            			
		                            			$class = ($count==1 ? 'active' : '');
		                            			?>
		                            			
		                            			<div role="tabpanel" class="tab-pane <?php echo $class;?>" id="<?php echo $backRemoved; ?>">
	                                    			<ul class="list-unstyled" role="tablist">
			                            			<?php
				                            			$field = get_sub_field_object('checkbox');
				                            			$countryLang = get_sub_field('checkbox'); // array of selected color values
				                            				foreach($countryLang as $langCode){
															?>
														 <li role="presentation"><a href="<?php echo home_url('/').$langCode; ?>" role="tab" data-toggle="tab"><?php echo $field['choices'][ $langCode ]; ?></a></li>
			                            			<?php } ?>
		                            				</ul>
		                            			</div>
		                            		<?php 
				                            endif;
				                            $count++;
		                            	endwhile;
		                           	 endif; endforeach;?>             
	                            </div>
	                        </div>
	                </div>
	    		</div>
	    		</div>
	    		</div>
	    	</div>
	    	<!-- popup -->
	    </div>  
	    <div class="col-sm-3 point-de"><h5><?php the_field('block_point_title', $footerPost[0]->ID)?></h5>
	    <ul class="list-unstyled">
	      <li><a href="<?php the_field('block_d_page_1_link', $footerPost[0]->ID)?>"><?php the_field('block_d_page_1', $footerPost[0]->ID);?></a></li>
	      <li><a href="<?php the_field('block_d_page_2_link', $footerPost[0]->ID)?>"><?php the_field('block_d_page_2', $footerPost[0]->ID);?></a></li>      
	    </ul>
	    
	    <div class="footer-social"><a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-instagram"></i></a> <a href="#"><i class="fa fa-tumblr"></i></a></div>
	    </div>
	    
	    <div class="col-sm-3 forms">
			<div class="input-group">
				<div id="newsletter">
	            	<div id="nl-mess"></div>
	                    	<form method="post" id="form_mailchimp">
		                    	<input type="email" name='email' placeholder="votre e-mail" class="form-control emailid">
		                    	<span class="input-group-btn">
	                            <input type="hidden" name='lang' value='<?=ICL_LANGUAGE_CODE?>' class="btn btn-default">
		                        <input type="submit" name='submit' value="OK" class="btn btn-default">
		                        </span>
	                        </form>
	                </div>
	                
	            </div>
	        
	        
			<div class="button-click">
				<button class="btn btn-default demande"><?php the_field('catalogue_request_title', $footerPost[0]->ID);?></button>
				<button class="btn btn-default"><?php the_field('general_information_title', $footerPost[0]->ID);?></button>
			</div>
			</div>
			
	    </div>
	  </div>
	</div>
	<!-- footer -->
	
	<!-- copyright -->
	<div class="text-center copyright"><?php _e("Copyright")?> <?php date("Y");?> <?php _e("Edox - Ma�tre Horloger - Les Genevez depuis 1884 / Website by Procabstudio");?> </div>
	<!-- copyright -->
	</div>
	<?php 
	
	die();
}