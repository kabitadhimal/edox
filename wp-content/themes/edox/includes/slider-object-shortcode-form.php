<?php require_once('../../../../wp-load.php'); ?>
<!DOCTYPE html><head>
	<title>Select slider to display</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>



<link rel='stylesheet' href='<?=get_site_url();?>/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load=dashicons,admin-bar,buttons,media-views,wp-admin,wp-auth-check,wp-color-picker&amp;ver=4.0' type='text/css' media='all' />    
    
	<script type="text/javascript">
		//More JS Here Later
	</script>
</head>
<body>

<div style="padding:20px;">
	<div class="widget-content">
	
		<p><div>
        <select id="selectType">
            <option value="LeftTextImageRight">Left Text Image Right</option>
            <option value="leftImageTextRight">Left Image Text Right</option>
            <option value="ImageLeftImageSliding">Image Left Image Sliding</option>
            <option value="ImageRightImageSliding">Image Right Image Sliding</option>
            <option value="normal">Normal Slider</option>
        </select>
    </div></p>
    
    
	    <div class="picture box">
	    		 <fieldset id="checkArray">
	    		 <?php 
					$args = array( 'post_type' =>'slider','posts_per_page' => -1 );
					$slider = get_posts( $args );
					foreach ($slider as $s) {
					?>
		                <label><input type="radio" name="slider[]" value="<?php echo $s->ID; ?>"><?php echo  $s->post_title; ?></label><br/>
					<?php 
					} ?>
				</fieldset>
	    </div>
    <div><input type="submit" name="savewidget" id="submit" class="button button-primary" onClick="javascript:SquareBox.insert(SquareBox.e)" value="update editor"></div>
  </div>
 </div>
  
<script type="text/javascript">
var SquareBox = {
	e: '',
	init: function(e) {
		SquareBox.e = e;
		tinyMCEPopup.resizeToInnerSize();
	},
	insert: function createMapShortcode(e) {
		
		var sliderIds = [];

		var selected = $("select#selectType option").filter(":selected");
		
		$('#checkArray input:checked').each(function(index, element) {
			sliderIds.push($(this).val());
        });
		if(sliderIds.length > 0){
	        var output = "<br/>[slider id='"+ sliderIds.join() +"' type='"+selected.val()+"'][/slider]<br />"    		
			tinyMCEPopup.execCommand('mceReplaceContent', false, output);
			tinyMCEPopup.close();
		}
	}
}
tinyMCEPopup.onInit.add(SquareBox.init, SquareBox);
</script>
</body>
</div>