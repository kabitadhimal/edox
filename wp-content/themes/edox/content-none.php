<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<header class="page-header">
	<h1 class="page-title"><?php _e( "rien n'a été trouvé", 'faei' ); ?></h1>
</header>
<div class="page-content">
	<p><?php //_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Please try to search using another keyword.', 'faei' ); ?>
	<?php _e("Il semble que nous ne pouvons pas trouver ce que vous cherchez. Se il vous plaît essayer de chercher en utilisant un autre mot clé.")?>
	</p>
	<?php //get_search_form(); ?>
</div><!-- .page-content -->
