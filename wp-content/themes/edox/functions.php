<?php
define ('TEMP_DIR_URI', get_template_directory_uri() );
define ('TEMP_DIR', get_template_directory() );
define('CATALOG_URL', get_site_url().'/Catalog/');
include __DIR__.'/includes/stores.php';


/**
 * Edox functions and definitions

 *

 * Set up the theme and provides some helper functions, which are used in the

 * theme as custom template tags. Others are attached to action and filter

 * hooks in WordPress to change core functionality.

 *

 * When using a child theme you can override certain functions (those wrapped

 * in a function_exists() call) by defining them first in your child theme's

 * functions.php file. The child theme's functions.php file is included before

 * the parent theme's file, so the child theme functions would be used.

 *

 * @link http://codex.wordpress.org/Theme_Development

 * @link http://codex.wordpress.org/Child_Themes

 *

 * Functions that are not pluggable (not wrapped in function_exists()) are

 * instead attached to a filter or action hook.

 *

 * For more information on hooks, actions, and filters,

 * @link http://codex.wordpress.org/Plugin_API

 *

 * @package WordPress

 * @subpackage Edox

 * @since Edox 1.0

 */

/**

 * Set up the content width value based on the theme's design.

 *

 * @see kd_content_width()

 *

 * @since Edox 1.0

 */

if ( ! function_exists( 'kd_setup' ) ) :

    /**
     * Edox setup.
     *
     * Set up theme defaults and registers support for various WordPress features.
     *

     * Note that this function is hooked into the after_setup_theme hook, which

     * runs before the init hook. The init hook is too late for some features, such

     * as indicating support post thumbnails.

     *

     * @since Edox 1.0

     */

    function kd_setup() {
        /*

         * Make Edox available for translation.

         *

         * Translations can be added to the /languages/ directory.

         * If you're building a theme based on Edox, use a find and

         * replace to change 'kd' to the name of your theme in all

         * template files.

         */

        load_theme_textdomain( 'edox' );
        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 300, 300, true );
        add_image_size( 'bannerimage', 1900, 850, true );
        add_image_size('partnership-img',500,500,true);
        add_image_size('partnership-thumbnail-img',283,283,true);
        add_image_size('vip-img',400,400,true);
        add_image_size('press-img',300,403,true);
        add_image_size('newsimage',530, 540, true);
        add_image_size('news-image',589, 288, true);
        add_image_size('cms-image',1918,1019,true);
        add_image_size('normal-slider',450,271,true);
        add_image_size('large-slider',799,592,true);
        add_image_size('text-img',396,392,true);
        add_image_size('catolog-img',451,271,true);
        add_image_size('flims-img',1600,720,true);
        register_nav_menus(
            array(
                'primary' => __( 'Primary Menu' ),
                'top-menu' => __( 'Top Menu' )
            ));
    }

endif; // kd_setup

add_action( 'after_setup_theme', 'kd_setup' );

/*

 * Change the length of the Excerpt

 */

/*

function the_excerpt_max_charlength($charlength) {

	$excerpt = get_the_excerpt();

	$charlength++;



	if ( mb_strlen( $excerpt ) > $charlength ) {

		$subex = mb_substr( $excerpt, 0, $charlength - 5 );

		$exwords = explode( ' ', $subex );

		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );

		if ( $excut < 0 ) {

			echo mb_substr( $subex, 0, $excut );

		} else {

			echo $subex;

		}

		echo '[...]';

	} else {

		echo $excerpt;

	}

}

*/
/**

 * Register three Edox widget areas.

 *

 * @since Edox 1.0

 */
function kd_widgets_init() {
    register_sidebar ( array (
        'name' => __ ( 'Email Subscription', 'edox' ),
        'id' => 'sidebar-1',
        'description' => __ ( 'An optional widget area for your site footer', 'edox' ),
        'before_widget' => ' <div class="newsletter--cont">',
        'after_widget' => "</div>",
        'before_title' => '<p>',
        'after_title' => '</p>'
    ) );

}

add_action ( 'widgets_init', 'kd_widgets_init' );

/**

 * Create a nicely formatted and more specific title element text for output

 * in head of document, based on current view.

 *

 * @since Edox 1.0

 *

 * @global int $paged WordPress archive pagination page count.

 * @global int $page  WordPress paginated post page count.

 *

 * @param string $title Default title text for current view.

 * @param string $sep Optional separator.

 * @return string The filtered title.

 */

function kd_wp_title( $title, $sep ) {

    global $paged, $page;

    if ( is_feed() ) {

        return $title;

    }

    // Add the site name.

    $title .= get_bloginfo( 'name', 'display' );
    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );

    if ( $site_description && ( is_home() || is_front_page() ) ) {

        $title = "$title $sep $site_description";

    }
    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {

        $title = "$title $sep " . sprintf( __( 'Page %s', 'kd' ), max( $paged, $page ) );

    }
    return $title;

}
add_filter( 'wp_title', 'kd_wp_title', 10, 2 );

/*

require TEMP_DIR . '/functions/kd_list_posts/widget.php';

require TEMP_DIR . '/functions/aboutus.php';

*/

//require TEMP_DIR . '/functions/kd-custom-posts.php';

//shortcodes

use App\Psr4AutoloaderClass;

if(!class_exists('App\Psr4AutoloaderClass')) {
    require __DIR__ . '/src/autoloader.php';
    Psr4AutoloaderClass::getInstance()->addNamespace('Procab', __DIR__.'/src/Procab');
    Psr4AutoloaderClass::getInstance()->addNamespace('App', __DIR__.'/src/App');
}




include 'functions/home_functions.php';
require TEMP_DIR . '/functions/kd-custom-posts.php';
include 'includes/shortcodes.php';
include 'functions/shortcode-functions.php';

include 'src/helpers.php';


add_post_type ( 'slider', array (
    'name' => 'Slider ',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Slider",

));

add_post_type ( 'home-slider', array (
    'name' => 'Home Slider ',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Home Slider",
));

add_post_type ( 'press-reviews', array (
    'name' => 'Publicity ',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Publicity"
));

add_post_type ( 'publicity', array (
    'name' => 'Press Reviews',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Press Reviews",
));

add_post_type ( 'vintage', array (
    'name' => 'Vintage',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Vintage",
));

add_post_type ( 'partnership', array (
    'name' => 'Partnership ',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Partnership",
));

add_post_type ( 'vip', array (
    'name' => 'VIP ',
    'menu_icon' => 'dashicons-calendar',
    'label' => "VIP",
));

add_post_type ( 'films', array (
    'name' => 'Films',
    'menu_icon' => 'dashicons-calendar',
    'label' => " Films",
));

add_post_type ( 'catalog', array (
    'name' => 'Catalog',
    'menu_icon' => 'dashicons-calendar',
    'label' => "Catlog",
));

add_post_type('stores',array(
    'name'=> 'Stores',
    'menu_icon' => 'dashicons-calendar',
    'Label' => 'Stores',
));

add_action( 'init', 'kd_create_stores_tax' );
function kd_create_stores_tax() {
    register_taxonomy(
        'category-stores',
        'stores',
        array(
            'label' => __( 'Category' ),
            'rewrite' => array( 'slug' => 'category-stores' ),
            'show_admin_column' => true,
            'hierarchical' => true,
        ));

    register_taxonomy(
        'category-year',
        'films',
        array(
            'label' => __( 'Year' ),
            'rewrite' => array( 'slug' => 'year' ),
            'hierarchical' => true,
            'show_admin_column' => true
        ));


    register_taxonomy(
        'countries',
        'stores',
        array(
            'label' => __( 'countries' ),
            'rewrite' => array( 'slug' => 'countries-stores' ),
            'show_admin_column' => true,
            'hierarchical' => true,
        ));

    register_taxonomy(
        'countries-publicity',
        'publicity',
        array(
            'label' => __( 'Countries' ),
            'rewrite' => array( 'slug' => 'countries-publicity' ),
            'show_admin_column' => true,
            'hierarchical' => true,
        ));

} // End of kd_create_stores_tax


/*

 * Allows extra HTML items in to the_excerpt instead of stripping them like WordPress does

*/

function theme_t_wp_improved_trim_excerpt($text) {

    global $post;

    if ( '' == $text ) {

        $text = get_the_content('');

        $text = apply_filters('the_content', $text);

        $text = str_replace(']]>', ']]&gt;', $text);

        $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);

        $text = strip_tags($text, '<p>,<ul>,<li>,<ol>');

        $excerpt_length = 10;

        $words = explode(' ', $text, $excerpt_length + 1);

        if (count($words)> $excerpt_length) {

            array_pop($words);

            array_push($words, '');

            $text = implode(' ', $words);

        }

    }

    return $text;

}



remove_filter('get_the_excerpt', 'wp_trim_excerpt');

add_filter('get_the_excerpt', 'theme_t_wp_improved_trim_excerpt');



// Adding JQuery

function procab_scripts_method() {
    wp_enqueue_style ( 'font-awesome', TEMP_DIR_URI . '/assets/font-awesome-4.3.0/css/font-awesome.min.css', array(), '1.2.2' );
    wp_enqueue_style ( 'allcss', TEMP_DIR_URI . '/css/all.min.css', array(), '1.2.2' );
    wp_enqueue_style ( 'extracss', TEMP_DIR_URI . '/css/extracss.css', array(), '1.2.2' );


    wp_deregister_script('jquery');
    wp_register_script ( 'jquery', TEMP_DIR_URI . '/assets/jquery.js', true, '1.0');
    wp_enqueue_script('jquery');

    if(is_page_template('page-template/edox-contact-template.php' ) || (is_page_template( 'page-template/point-of-sale.php') )) {

        $googleMapKey = get_field('google_map_key', 'option');
        wp_register_script ( 'googemap','https://maps.googleapis.com/maps/api/js?key='.$googleMapKey, array(), '1.1');
    }

    wp_register_script ( 'meanmenujs', TEMP_DIR_URI . '/assets/meanmenu/jquery.meanmenu.js', array(), '1.0');
    wp_register_script ( 'waypoints', TEMP_DIR_URI . '/assets/waypoint/js/waypoints.min.js', true, '1.2', true);
    wp_register_script ( 'swiperjs', TEMP_DIR_URI . '/assets/swiper/swiper.min.js', true, '1.2', true);
    wp_register_script ( 'globalScripts', TEMP_DIR_URI . '/js/globalScripts.js', true, '1.2', true);
    wp_register_script ( 'select', TEMP_DIR_URI . '/assets/select.js', true, '1.2', true);
    wp_register_script ( 'script', TEMP_DIR_URI . '/assets/script.js', true, '1.2', true);
    wp_register_script ( 'rollover', TEMP_DIR_URI . '/assets/rollover/rollover.js', array(), '1.2' );
    wp_register_script ( 'fancy-box-js', TEMP_DIR_URI . '/assets/rollover/jquery.fancybox.js', array(), '1.0' );
    wp_enqueue_script ( array (
        'meanmenujs',
        'googemap',
        'waypoints',
        'globalScripts',
        'swiperjs',
        'select',
        'script',
        'rollover',
        'fancy-box-js'

    ));



} // procab_scripts_method

add_action ( 'wp_enqueue_scripts', 'procab_scripts_method' );



// Display page

function kd_display_page($id) {
    $query = new wp_query('page_id='.$id);
    while ($query->have_posts()):$query->the_post();
        the_content();
    endwhile;
    wp_reset_query();
}


//remove stress characters, stress characters

add_filter('wp_handle_upload_prefilter', 'procab_upload_filter',1,1 );
function procab_upload_filter( $file ){
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/','-', $file['name']);
    return $file;
}



remove_filter( 'the_content', 'wpautop' );

add_filter( 'the_content', 'wpautop' , 12);



function last_words($amount, $string)

{

    $amount+=1;

    $string_array = explode(' ', $string);

    $totalwords= str_word_count($string, 1, '????3');

    if($totalwords > $amount){

        $words= implode(' ',array_slice($string_array, count($string_array) - $amount));

    }else{

        $words= implode(' ',array_slice($string_array, count($string_array) - $totalwords));

    }



    return $words;

}

//  Custom post type navigation

function kd_custom_post_nav() {

    global $wp_query;



    if ( $wp_query->max_num_pages > 1 ) :

        echo '<nav id="nyb_post_pagi">';

        $previous = (ICL_LANGUAGE_CODE == 'fr') ? '« articles plus anciens' : 'older articles';

        $new = (ICL_LANGUAGE_CODE == 'fr') ? '« nouveaux articles' : '« new articles';

        next_posts_link( $previous );

        previous_posts_link($new);

        echo '</nav>';

    endif;

}



// Adding class to next and previous link

add_filter('next_posts_link_attributes', 'kd_posts_link_attributes_1');

add_filter('previous_posts_link_attributes', 'kd_posts_link_attributes_2');



function kd_posts_link_attributes_1() {

    return 'class="back-btn"';

}

function kd_posts_link_attributes_2() {

    return 'class="back-btn"';

}

/**

 * usess tim thumb

 * returns image url

 * <img data-src="" src="<?php echo kd_resize_url($imageUrl, array('w'=>1920, 'h'=>700)); ?>" alt="<?php the_title();?>">

 */

function kd_resize_url($image, $args = null) {

    $image = explode ( 'wp-content', $image );

    $image = '/wp-content' . $image [1];

    $query = '';

    if ($args === null)

        $args = array (

            'w' => 100

        );

    foreach ( $args as $key => $val ) {

        $query .= '&' . $key . '=' . $val;

    }

    $str = get_template_directory_uri () . '/timthumb.php?src=' . $image . '' . $query;

    return $str;

}



/**

 * usess tim thumb

 * returns image tag

 * echo kd_resize($imageUrl, array('w'=>1920, 'h'=>700), 'test-css-for-iamge')

 */

function kd_resize($image, $args = null, $cssClass = '') {

    // $imageUrl = wp_get_attachment_url( get_post_thumbnail_id(), 'full');

    $image = explode ( 'wp-content', $image );

    $image = '/wp-content' . $image [1];



    $query = '';

    if ($args === null)

        $args = array (

            'w' => 100

        );

    foreach ( $args as $key => $val ) {

        $query .= '&' . $key . '=' . $val;

    }

    $str = get_template_directory_uri () . '/timthumb.php?src=' . $image . '' . $query;

    $cssClass = ($cssClass != '') ? "class='" . $cssClass . "'" : '';

    return "<img src='" . $str . "' " . $cssClass . " />";

}



remove_filter( 'the_content', 'wpautop' );

add_filter( 'the_content', 'wpautop' , 12);



// For MailChimp

add_action('wp_ajax_mailchimp_subscribe', 'mailchimp_subscribe_callbck');           // for logged in user
add_action('wp_ajax_nopriv_mailchimp_subscribe', 'mailchimp_subscribe_callbck');   // if user is not logged in



function mailchimp_subscribe_callbck(){
    include 'includes/mail-chimp-subscriber.php';
    die();

}

function sendAddressToMailChimp($email, $lang = en){

    require_once(__DIR__.'/libs/mailchimp/MCAPI.class.php');
    define('MC_API_KEY', '1e7fe41d3cf13341986c2fe7dafd5125-us10');
    if($lang == 'en'){
        $listID = '2152f34cb6';
    }else{
        $listID = '2152f34cb6';
    }
    $api = new MCAPI(MC_API_KEY);

    return ($api->listSubscribe($listID, $email, '',null,'html',false));
}




//  Take Content based on Maxwords and Max chars

function truncate($input, $maxWords, $maxChars){
    //$input = do_shortcode($input);

    $words = preg_split('/\s+/', $input);

    $words = array_slice($words, 0, $maxWords);

    $words = array_reverse($words);

    $chars = 0;

    $truncated = array();

    while(count($words) > 0) {

        $fragment = trim(array_pop($words));

        $chars += strlen($fragment);

        if($chars > $maxChars) break;

        $truncated[] = $fragment;

    }



    $result = implode($truncated, ' ');

    $res = do_shortcode($result);

    //return $result.' ...';

    return $result;

}



function the_slug($echo=true){

    $slug = basename(get_permalink());

    do_action('before_slug', $slug);

    $slug = apply_filters('slug_filter', $slug);

    if( $echo ) echo $slug;

    do_action('after_slug', $slug);

    return $slug;

}



remove_filter( 'the_content', 'wpautop' );



remove_filter( 'the_excerpt', 'wpautop' );



function kd_get_excerpt($count){

    $permalink = get_permalink($post->ID);

    $excerpt = get_the_content();

    $excerpt = strip_tags($excerpt);

    $excerpt = substr($excerpt, 0, $count);

    $excerpt = $excerpt;

    return $excerpt;

}





/**

 * Display descriptions in main navigation.

 *

 * @since Twenty Fifteen 1.0

 *

 * @param string  $item_output The menu item output.

 * @param WP_Post $item        Menu item object.

 * @param int     $depth       Depth of the menu.

 * @param array   $args        wp_nav_menu() arguments.

 * @return string Menu item with possible description.

 */
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {

    if ( 'primary' == $args->theme_location && $item->description ) {

        if(trim($item->description) !=''){

            $item_output = str_replace( $args->link_after . '</a>', $args->link_after.'<div class="menu-item-title">'.$item->title.'</div><div class="menu-item-description">' . nl2br($item->description) . '</div></a><a href="'.$item->url.'" class="btn-menu"></a>', $item_output	 );

        }else{

            //$item_output = str_replace( $args->link_after . '</a>', $args->link_after.'</a><a href="'.$item->url.'" class="btn-menu"></a>', $item_output	 );
            $item_output = str_replace( $args->link_after . '</a>', $args->link_after.'</a><a href="'.$item->url.'"></a>', $item_output	 );

        }

    }else if('top-menu' == $args->theme_location){

        $item_output = str_replace( $args->link_after . '</a>', $args->link_after.'<span>'.$item->title.'</span></a>', $item_output	 );

    }

    return $item_output;

}

add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );





//enable html tags on the menu. this will let $item->post_content to have html tags

remove_filter('nav_menu_description', 'strip_tags');



//add filter for $item->description to have html tags on the front end and on the backend

add_filter( 'wp_setup_nav_menu_item', 'edox_wp_setup_nav_menu_item' );

function edox_wp_setup_nav_menu_item( $menu_item ) {

    if ( isset( $menu_item->post_type ) ) {

        if ( 'nav_menu_item' == $menu_item->post_type ) {

            $menu_item->description = apply_filters( 'nav_menu_description', $menu_item->post_content );

        }

    }

    return $menu_item;

}

/**/
//menus for magento

include 'includes/ajax-request.php';

//include 'includes/ajax-footer.php';
//User : Edoxswisswatches MDP : 2714Edox$



/*

 * Send mail based on Country selected

 */

function wpcf7_before_send_mail_attachReceiver ($wpcf7) {

    //var_dump($_POST);

    if(isset($_POST['subscribed']) && $_POST['subscribed']==true){

        //$properties = $wpcf7->get_properties();

        //var_dump($properties);

        $userEmail = $_POST['email'];

        //ues mailchimp api

        if($userEmail != '' && is_email($userEmail)) sendAddressToMailChimp($userEmail, ICL_LANGUAGE_CODE);

    }



    if(isset($_POST['change-email'])){

        $properties = $wpcf7->get_properties();
        //get receiver by country
        $countrySelected =  strtolower($_POST['country']);
        if($countrySelected == 'france'){

            $properties['mail']['recipient'] = $properties['mail']['recipient'];

        }

        $wpcf7->set_properties($properties);

    }

}

add_action("wpcf7_before_send_mail", "wpcf7_before_send_mail_attachReceiver");



/*

 *

 */



function my_icl_object_id($postId, $type){

    if(function_exists('icl_object_id')) {

        return icl_object_id($postId, $type, false);

    }else{

        return $postId;

    }

}



function getLangFromQuery(){

    $langs = array('en','zh-hant','fr','de','es','ar','ko','pl');

    $lang = 'en';

    //get lang code

    if(isset($_GET['lang'])){

        $lang = strtolower($_GET['lang']);

        if(!in_array($lang, $langs)) $lang = 'en';

    }

    return $lang;

}



/*
 * News Page
 */

function getNews($postPerPage=11, $offset=0){

    $args = array('posts_per_page'=>$postPerPage, 'offset'=>$offset , 'post_status'=>'publish', 'suppress_filters'=>0);

    return get_posts($args);

}



function getNews2($postPerPage=11, $offset=0){

    $args = array('posts_per_page'=>$postPerPage, 'offset'=>$offset , 'post_status'=>'publish', 'suppress_filters'=>0);

    $query = new WP_Query($args);

    //echo '<pre>';

    //var_dump($query->found_posts);

    return array($query->posts, $query->found_posts);

    //found_posts

//	return get_posts($args);

}

/*
 * Function for instagram feed
 */



function getInstagramFeeds($limit = 1){
    $api_url = "https://api.instagram.com/v1/users/1271365307/media/recent?access_token=1271365307.244da64.e58a3758dd264a40b47b65d640732395&count=".$limit;
    $connection_c = curl_init(); // initializing
    curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
    curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
    curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
    $json_return = curl_exec( $connection_c ); // connect and get json data
    curl_close( $connection_c ); // close connection

    $items = [];
    //$json_return = file_get_contents("/home/edox/public_html/insta.php");
    $insta = json_decode( $json_return ); // decode and return
    if($insta){
        $counter = 0;
        if(!empty($insta->data)):
            foreach($insta->data as $feed){
                //if($counter < 6){
                $items[] = array($feed->link, $feed->images->standard_resolution->url);
                //}
                $counter++;
            }
        endif;
    }
    return $items;
}

function getLat($latLon=null) {

    if(!$latLon) return null;

    $lat = explode(',', $latLon);

    return $lat[0];

}

function getLong($latLon=null) {

    if(!$latLon) return null;
    $long = explode(',', $latLon);
    if($long[1]){
        return $long[1];
    }else{
        return null;
    }
}

//edox string translation

function edox_e($str, $lang='en', $defaultLang = 'en'){
    if($lang=='en'){
        return __($str);
    }else{
        global $sitepress;
        $currentLang = $sitepress->get_current_language();
        $sitepress->switch_lang($lang);
        $str = __($str);
        $sitepress->switch_lang($currentLang);
        return $str;
    }
}

function edox_get_translated_term($termId, $taxonomy, $lang){
    $translatedTermId = icl_object_id($termId, $taxonomy, true, $lang);

    //remove wpml filter
    global $sitepress;
    remove_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1 );
    $translatedTerm = get_term_by('id', $translatedTermId, $taxonomy, false);

    //restore wpml filter to adjust ID
    add_filter( 'get_term', array( $sitepress, 'get_term_adjust_id' ), 1, 1 );
    return $translatedTerm;
}

function edox_get_translated_term_name($term, $lang){
    global $sitepress;
    if($sitepress->get_current_language() == $lang){
        return $term->name;
    }
    $translatedTerm = edox_get_translated_term($term->term_id, $term->taxonomy, $lang);
    return $translatedTerm->name;
}



function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) ) $src = remove_query_arg( 'ver', $src );
    return $src;
}

//add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/**
 *
 * @return boolean | string
 * country code eg. US
 */
function kd_detect_country(){
    include(__DIR__."/libs/geo_ip/src/geoip.inc");
    $gi = geoip_open(__DIR__."/libs/geo_ip/data/GeoIP.dat", GEOIP_STANDARD);

    $ip = (isset($_GET['ip'])) ? $_GET['ip'] : get_client_ip();
    //$ip = get_client_ip();
    //$ip = '90.65.208.157';
    return geoip_country_code_by_addr($gi, $ip);
}

function kd_get_detected_ip_code() {
    require_once 'libs/geoplugin/geoplugin.class.php';

    $cache = get_procab_file_cache();
    $ip = get_client_ip();
    $cacheKeyLocation = 'ip-'.$ip;
    $cacheLocationObj =  $cache->restore($cacheKeyLocation, 'static');

    //if in cache return cache obj
    if($cacheLocationObj):
        //echo 'got everythinfsfd from cache';
        return $cacheLocationObj;
    else:
        //echo 'not foudn in cache';
        $geoplugin = new geoPlugin();
        $geoplugin->locate($ip);
        //$countryCode = $geoplugin->countryCode;
        //  test
        // $countryCode = "CH";
        //return $countryCode;
        $cache->store($cacheKeyLocation, $geoplugin, 'static');
        return $geoplugin;
    endif;
}

/*
 *  Get your current ip
 */

function get_client_ip(){

    if(isset($_SERVER['HTTP_REMOTE_IP'])){
        $ip = $_SERVER['HTTP_REMOTE_IP'];
    }else if (isset($_SERVER['HTTP_X_REMOTE_IP'])) {
        $ip = $_SERVER['HTTP_X_REMOTE_IP'];
    }else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_X_FORWARDED'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_FORWARDED_FOR'];
    }
    else if(isset($_SERVER['HTTP_FORWARDED'])) {
        $ip = $_SERVER['HTTP_FORWARDED'];
    }
    else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }


    //check for multiple ip address
    if($ipIndex = strpos($ip, ',')){
        $ip = substr($ip,0, $ipIndex);
    }
    return $ip;
}

/**
 * Send wordPress Update email to hosting@procab.ch
 */

function procab_change_wp_update_email_to($email, $type, $core_update, $result){
    $email['to'] = 'hosting@procab.ch';
    return $email;
}
add_filter( 'auto_core_update_email', 'procab_change_wp_update_email_to', 10, 4 );


/*
 * Function for duplicating post
 */

add_action('save_post', 'wpml_duplicate_on_publish');

function wpml_duplicate_on_publish ( $post_id ) {
    global $sitepress, $iclTranslationManagement;

    // don't save for autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
    // don't save for revisions
    if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
        return $post_id;
    }
    // Check permissions
    if ( 'page' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) ) {
            return $post_id;
        }
    } else {
        if ( !current_user_can( 'edit_post', $post_id ) ) {
            return $post_id;
        }
    }

    $master_post_id = $post_id;
    $master_post = get_post($master_post_id);

    $language_details_original = $sitepress->get_element_language_details($master_post_id, 'post_' . $master_post->post_type);

    //$master_post->post_type;


    $languageCode = get_field('language_code', 'option');
    $postType = get_field('post_type_slug', 'option');


    // unhook this function so it doesn't loop infinitely
    remove_action('save_post', 'wpml_duplicate_on_publish');
    foreach($sitepress->get_active_languages() as $lang => $details){

        // if($lang != $language_details_original->language_code && ( $lang == 'es') && ($master_post->post_type=="page") ) {
        if($lang != $language_details_original->language_code && ( $lang == $languageCode ) && ($master_post->post_type==$postType) ) {


            $iclTranslationManagement->make_duplicate($master_post_id, $lang);
        }
    }

    // re-hook this function
    add_action('save_post', 'wpml_duplicate_on_publish');

}

/**
 * Create the options page that we will later set "global"
 */
if(function_exists('acf_add_options_page')){
    acf_add_options_page(array(
        'page_title'  => 'Duplicate Content',
        'menu_title'  => 'Duplicate'
    ));
}

//had to manually sort categories with alphabetically, due to Category Order and Taxonomy Terms Order....
function sortCategories($items){
    $placeHolder= [];
    foreach($items as $item){
        $placeHolder[$item->name] = $item;
    }
    ksort($placeHolder);
    return $placeHolder;
}

/*
 * Translate arraays of id
 *
 */
function lang_object_ids($ids_array, $type) {
    if(function_exists('icl_object_id')) {
        $res = array();
        foreach ($ids_array as $id) {
            $xlat = icl_object_id($id,$type,false);
            if(!is_null($xlat)) $res[] = $xlat;
        }
        return $res;
    } else {
        return $ids_array;
    }
}

/*
 * Creating featured image for translated content
 */

function otgs_duplicate_featured_image( $post_id ) {

    if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return;

    $capability = ( 'page' == $_POST['post_type'] ) ? 'edit_page' : 'edit_post';

    if( is_user_logged_in() && current_user_can( $capability, $post_id ) ) {

        $is_duplicate = get_post_meta( $post_id, '_icl_lang_duplicate_of', true );

        if( !empty( $is_duplicate ) && is_numeric($is_duplicate) ){
            $thumbnail_id = get_post_meta( $is_duplicate, '_thumbnail_id', true );
            if($thumbnail_id != '')  update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
        }
    }

}
add_action( 'save_post', 'otgs_duplicate_featured_image' );

function file_get_contents_curl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $data = curl_exec($ch);
    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($retcode == 200) {
        return $data;
    } else {
        return null;
    }
}

/*
 * Options Page
 */

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'General Settings',
        'menu_title'	=> 'General Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
}
/*
 * Register an API key Map
 */

function procab_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyAaiHtyT_EdIONgueNZcydOSr5eIKBdyFY');
}
add_action('acf/init', 'procab_acf_init');

/*
 * New Mailchimp API
 */

function edox_mailchimp_subscriber_status( $email, $status, $list_id, $api_key, $merge_fields = array('FNAME' => '','LNAME' => '') ){
    $data = array(
        'apikey'        => $api_key,
        'email_address' => $email,
        'status'        => $status,
        'merge_fields'  => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection
    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json

    $result = curl_exec($mch_api);
    return $result;
}


function edox_mch_subscribe(){
    $list_id = '2152f34cb6';
    $api_key = '05e73f8ad71a1dab7743d7d852eb2dde-us10';
    $response = array();
    $successMsg ='';
    $result = json_decode( edox_mailchimp_subscriber_status($_POST['email'], 'subscribed', $list_id, $api_key, array('FNAME' =>'' ,'LNAME' => '') ) );
    // print_r( $result );
    if( $result->status == 400 ){
        foreach( $result->errors as $error ) {
            //  echo '<p>Error: ' . $error->message . '</p>';
            $response['error'] = '<p>Error: ' . $error->message . '</p>';
        }
    } elseif( $result->status == 'subscribed' ){
        // echo 'Thank you, ' . $result->merge_fields->FNAME . '. You have subscribed successfully';

        if(ICL_LANGUAGE_CODE=="es") {
            $successMsg = "Gracias. ha sido agregado a nuestra lista de correo.";

        }elseif (ICL_LANGUAGE_CODE=='fr') {
            $successMsg = "Merci. vous avez été ajouté à notre liste de diffusion.";

        }elseif (ICL_LANGUAGE_CODE=='bs') {
            $successMsg = "Hvala. dodali ste nam na našu mailing listu.";

        }elseif (ICL_LANGUAGE_CODE=='zh-hant') {
            $successMsg = "谢谢。 您已被添加到我们的邮件列表中。。";

        }elseif (ICL_LANGUAGE_CODE=='de') {
            $successMsg ="Danke. Sie wurden unserer Mailingliste hinzugefügt.";

        }else {
            $successMsg ="Thank you. you have been added to our mailing list.";
        }

        $response['success'] = $successMsg;
    }
    // $result['id'] - Subscription ID
    // $result['ip_opt'] - Subscriber IP address
    $response = array_map('utf8_encode', $response);
    echo json_encode($response);
    die;
}

add_action('wp_ajax_mailchimpsubscribe','edox_mch_subscribe');
add_action('wp_ajax_nopriv_mailchimpsubscribe','edox_mch_subscribe');

/*
 * Featured image on Admin column
 */

// ALL POST TYPES: posts AND custom post types
add_filter('manage_posts_columns', 'kd_columns_head');
function kd_columns_head($columns) {
    $columns['image_type'] = 'Image Type';
    return $columns;
}

add_action('manage_posts_custom_column', 'kd_image_type_custom_columns', 10, 2);
function kd_image_type_custom_columns($column, $post_id){
    global $post;
    switch ($column) {
        case 'image_type' :
            the_post_thumbnail('thumbnail');
            break;
    }
}



/*
 *Change The Password Protected Text
 */
add_filter('the_content', 'wpfilter_password_protected_message');
function wpfilter_password_protected_message($content) {
    global $post;

    if (!empty($post->post_password)) {
        if(ICL_LANGUAGE_CODE=="en") {
            $content = str_replace('This content is password protected. To view it please enter your password below:','This content is password protected. To view it enter your password.If you want to obtain the password, subscribe to our Newsletter.',$content);
        }
        if(ICL_LANGUAGE_CODE=="fr") {
            $content = str_replace('This content is password protected. To view it please enter your password below:',"Ce contenu est protégé par mot de passe. Pour le consulter, entrez votre mot de passe. Si vous souhaitez obtenir le mot de passe, abonnez-vous à notre newsletter.",$content);
        }
        if(ICL_LANGUAGE_CODE=="zh-hant") {
            $content = str_replace('This content is password protected. To view it please enter your password below:',"此內容受密碼保護。 要查看密碼，請輸入密碼。如果要獲取密碼，請訂閱我們的時事通訊。",$content);
        }
        if(ICL_LANGUAGE_CODE=="de") {
            $content = str_replace('This content is password protected. To view it please enter your password below:',"Dieser Inhalt ist passwortgeschützt. Um es anzuzeigen, geben Sie Ihr Passwort ein. Wenn Sie das Passwort erhalten möchten, abonnieren Sie unseren Newsletter.",$content);
        }
        if(ICL_LANGUAGE_CODE=="es") {
            $content = str_replace('This content is password protected. To view it please enter your password below:',"Este contenido está protegido por contraseña. Para verlo ingrese su contraseña. Si desea obtener la contraseña, suscríbase a nuestro boletín.",$content);
        }
        if(ICL_LANGUAGE_CODE=="bs") {
            $content = str_replace('This content is password protected. To view it please enter your password below:',"Ce contenu est protégé par mot de passe. Pour le consulter, entrez votre mot de passe. Si vous souhaitez obtenir le mot de passe, abonnez-vous à notre newsletter.",$content);
        }
    }
    return $content;
}

/**
 * Advanced Custom Fields Options function
 * Always fetch an Options field value from the default language
 */
function app_acf_set_language() {
    return acf_get_setting('default_language');
}
function get_global_option($name) {
    add_filter('acf/settings/current_language', 'app_acf_set_language', 100);
    $option = get_field($name, 'option');
    remove_filter('acf/settings/current_language', 'app_acf_set_language', 100);
    return $option;
}