<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage EDOX
 * @since EDOX 1.0
 */
get_header();
?>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55b078a961e25d4d" async="async"></script>


<?php
$cache = get_procab_file_cache();
$cachesingleEdoxUniverseKey = $cache->buildKey('singleEdoxUniverse');
$cachesingleEdoxUniverseData = $cache->restore($cachesingleEdoxUniverseKey);
if($cachesingleEdoxUniverseData): echo $cachesingleEdoxUniverseData;
else:
    $cache->captureStart($cachesingleEdoxUniverseKey);
?>


<div class="main-wrapper">
<div class="container">
  <div class="newdetailpage">

    <?php

    while (have_posts()): the_post(); ?>
    <div class="header-section bg-blue">
      <?php the_title('<h1>','</h1>'); 	?>
      <h2><?php the_field('news_subtitle'); ?></h2>
    </div>
    <div id="banner"> <img src="<?php the_field('banner_image'); ?>" alt="<?php the_title(); ?>" class="img-responsive"></div>
    <div id="col-text" class="news-wrapper cms-content">
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
	<div class="addthis_native_toolbox"></div>
      <div class="blockspace"><?php the_field('column_text'); ?></div>
    </div>
    <div>
      <?php //Slider text ?>


      <?php
      $i=1;
    // check if the repeater field has rows of data
    if( have_rows('slider_text') ):

      ?>
        <div id="slideText" class="career-achievements-section">
            <div class="blockspace">
                <h2><?php the_field('slider_text_title'); ?></h2>
                <div id="carousel-example-generic" class="carousel carousel1 slide news-wrapper cms-content" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner" role="listbox">

                    <?php
		    		while ( have_rows('slider_text') ) : the_row();
		    		$active = ($i==1 ? 'active' : '');
		    		?>
			              <div class="item <?=$active?>">
			                  <?php the_sub_field('text');?>
			              </div>
              	<?php 
		    		   $i++;
		    		endwhile;

		    		?>
                    </div>
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>

          </div>
            </div>
        </div>
        <?php	endif; ?>


    <div class="news-wrapper cms-content">
	<br /><br />
		<h2><?php the_field('Image_block_title',$post->ID); ?></h2>
		<div class="swiper-container ">
			<div class="swiper-wrapper">
			
			<?php 
				while ( have_rows('image_block') ) : the_row();
	    		// display a sub field value
				$slideImage = get_sub_field('upload_image');
				if(!empty($slideImage)):
	    		?>
            	<div class="swiper-slide"><img src="<?=$slideImage?>" class="img-responsive"></div>
            <?php endif; endwhile; ?>

			</div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>

		</div>
	</div>
    <!-- Swiper JS -->
       
      </div>
      <div class=" clearfix"></div>
      <div class=" news-wrapper cms-content newsPage-gallery">
	  <div class="blockspace">
        <h2><?php the_field('gallery_title',$post->ID)?></h2>
        <?php 
    		
    		
    		$images = get_field('images_for_gallery');
    		
    		if( $images ): ?>
        <div class=" gallery">
		<ul>
          <?php foreach( $images as $image ): ?>
           <li class='gallery-item col-md-3 col-sm-6 col-xs-6'>
			  <span class='gallery-icon'>
				  <a href="<?php echo $image['url']; ?>" rel='gallery'>
					<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"  />
				  </a>
			  </span>
			</li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
		</div>
        </div>
      </div>
      
      <div class="pull-right blockspace btn-wrap">
      		<a href="<?php echo site_url('/news');?>" class="btn-cms"><?php _e("BACK TO EDOX UNIVERSE"); ?></a>
	   </div>
	   
      <?php //For video
    	$videoLink = get_field('home_video_link');
    	if(!empty($videoLink)):
    ?>
      <div class="video" id="video"> <img src="<?php the_field('background_image'); ?>" class="img-responsive">
        <div class="overlay">
          <div class="caption">
            <div class="caption-text">
              <h3 class="wow animated fadeInDown">
                <?php the_field('home_video_title');?>
              </h3>
              <h2 class="wow animated fadeInDown">
                <?php the_field('home_video_subtitle');?>
              </h2>
              <p class="wow animated fadeInDown">
                <?php the_field('home_video_play_button_title');?>
              </p>
              <a href="#video" data-video-href="<?php the_field('home_video_link');?>" target="_blank"  onclick="loadVideo('<?php the_field('home_video_link');?>')" class="play wow animated fadeInDown animated scroll hidden-xs"><i class="fa fa-play"></i></a>
			  <a href="<?php the_field('home_video_link');?>" target="_blank" class="play hidden-sm hidden-md hidden-lg"><i class="fa fa-play"></i></a>
		  
			</div>
          </div>
        </div>
        <!-- video popup -->
        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <button type="button" class="close-btn pull-right" data-dismiss="modal" aria-label="Close"><img src="<?php echo get_template_directory_uri();?>/images/cross.png"></button>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" src="<?php the_field('home_video_link');?>" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <!-- video popup -->
        <div class="closeVideo" id="ytplayerClose" onclick="closeVideo();" style="display:none">Close Video</div>
        <div id="ytplayer"></div>
      </div>
      <?php endif;?>
      <?php endwhile;
      	wp_reset_postdata();
      ?>
	   
    </div>
  </div>
</div>
  <?php 
	  echo $cache->captureEnd($cachesingleEdoxUniverseKey);
	endif;
 ?>
<script src="<?php echo get_template_directory_uri();  ?>/js/yt_video.js"></script>
<?php get_footer();?>
