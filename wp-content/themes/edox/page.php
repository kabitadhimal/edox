<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage EDOX
 * @since EDOX 1.0
 */

get_header();
?>
  <div class="main-wrapper">
    <div class="cms-content">
	<?php
	$cache = get_procab_file_cache();
	$cachePageKey = $cache->buildKey('page');
	$cachePageData = $cache->restore($cachePageKey);
	if($cachePageData): echo $cachePageData;
	else:
	   $cache->captureStart($cachePageKey);
	?>
  <?php while (have_posts()): the_post();?>

    <?php if(has_post_thumbnail()):
    		$imgArray =  wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'cms-image');
    		echo ' <div class="full-width-extend"><img src="'.$imgArray[0].'" class="img-responsive"></div>';
    	endif;
    	the_content();
    ?>
  <?php endwhile;?>
  <?php
        echo $cache->captureEnd($cachePageKey);
    endif;
  ?>

    </div>
  </div>
 <?php get_footer();