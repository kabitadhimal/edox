<?php
/*
Plugin Name: Procab Object Cache
Plugin URI: http://procab.com
Description: This is a brief description of my plugin
Version: 1.0
Author: Procab
Author URI: Laxman Thapa
Text Domain: procab-file-cache
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! defined( 'OBJECT_CACHE_ENABLED' ) ) {
    define('OBJECT_CACHE_ENABLED', true);
}
    



class Procab_File_Cache_Plugin{
    protected static $_instance = null;
    
    
    protected $cache;
    /*
    public static function init(){
        $class = __CLASS__;
        $class = new $class;
        
        add_filter('pro_file_cache', function() use ($class){
            return $class->getCache();
        });
        
        //ajax callbacks
        add_action( 'wp_ajax_procab_view_cache', array($class, 'viewCache'));
        add_action( 'wp_ajax_nopriv_procab_view_cache', array($class, 'viewCache'));
    }
    */
    public function viewCache(){
        $key = $_GET['key'];
        $group = $_GET['group'];
        $data =  $this->getCache()->restore($key, $group, false);
        
        if(!$data){
            echo 'cache not found!';
            wp_die();
        }
        
        echo '<div>';
        $ttl = $data[0];
        
        if(($ttl - time()) < 0){
            echo '<strong>The cache expired '. human_time_diff($ttl, time()).' ago </strong>';
        }else{
            echo '<strong>The cache will expire on '. human_time_diff($ttl, time()).'</strong>';
        }
        
        var_dump($data[1]);
        echo '</div>';
        ?>
		<style>
            pre {
             white-space: pre-wrap;       /* css-3 */
             white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
             white-space: -pre-wrap;      /* Opera 4-6 */
             white-space: -o-pre-wrap;    /* Opera 7 */
             word-wrap: break-word;       /* Internet Explorer 5.5+ */
            }
            </style>
        <?php
        wp_die();
    }
    
    public function __construct(){
        if(!self::$_instance){
            register_activation_hook(__FILE__, array($this, 'checkCompatibility'));
            add_action('admin_menu', array($this, 'createMenu'));
            self::$_instance = $this;
        }
        return self::$_instance;
   }
   
    
   public function getCache($isEnabled = true){
       if(!$this->cache){
           require 'src/FileCache.php';
           require 'src/FileCacheContainer.php';
           
       }
       $this->cache = new Procab_FileCache($isEnabled);
       return $this->cache;
   }
    
    public function checkCompatibility(){
        global $wp_version;
        if ( version_compare( $wp_version, '4.1', '<' ) ) {
            wp_die( 'This plugin requires WordPress version 4.1 or higher.' );
        }   
    }
    
    public function createMenu(){
        //add menu
        add_menu_page('Procab File Cache',
            'File Cache',
            'manage_options',
            'procab_file_cache',
            array($this, 'settingsPage'));
        
        //call register settings function
        //add_action('admin_init', array($this, 'registerSettings'));
    }
    
    public function settingsPage(){
        //get cache files
        
        $this->handleDeletion();
        
        /*
        
        
        if(isset($_POST['action']) && $_POST['action'] = 'delete-selected'){
            //delete the cache file
            $filesForDeletion = $_POST['checked'];
            if(is_array($filesForDeletion) && !empty($filesForDeletion)){
                foreach ($filesForDeletion as $file){
                    var_dump('delete '. $this->getCache()->getFileName($file));
                }
            }
        }
        */
        $files = $this->getCache()->getCacheFiles();
        include 'src/views/list.php';
    }
    
    public function deleteAllFlatCache(){
        $this->getCache()->deleteAllFlatCache();
    }
    
    protected function handleDeletion(){
        if(isset($_POST['action'])){
            if(isset($_POST['delete-selected'])){
                if(!empty($_POST['checked'])){
                    echo 'delete selected files';
                    $filesForDeletion = $_POST['checked'];
                    foreach ($filesForDeletion as $file){
                        //var_dump('delete '. $this->getCache()->getFilePath($file));
                        //$this->getCache()->delete($file);
                        $index = strpos($file, '_'); 
                        if($index !== false){
                            $group = substr($file, 0, $index);
                            $key = substr($file, $index+1 );
                            $this->getCache()->delete($key, $group);
                        }else{
                            $this->getCache()->delete($file);
                        }
                    }
                }
            }elseif(isset($_POST['delete-all'])){
                $this->getCache()->deleteAll();
                //$this->getCache()->deleteAllFlatCache();
            }
            //$url = admin_url('admin.php?page=procab_file_cache&message=-1');
            //wp_redirect($url);
        }
        
        
    }
    
}

//add_action( 'plugins_loaded', array( 'Procab_File_Cache_Plugin', 'init' ));
$procab_file_cache_plug = new Procab_File_Cache_Plugin();

global $procab_file_cache;
$procab_file_cache = $procab_file_cache_plug->getCache(OBJECT_CACHE_ENABLED);

/**
 * @return Procab_FileCache
 */
add_filter('procab_file_cache', function() use ($procab_file_cache){
    return $procab_file_cache;
});

 //ajax callbacks
add_action( 'wp_ajax_procab_view_cache', array($procab_file_cache_plug, 'viewCache'));
add_action( 'wp_ajax_nopriv_procab_view_cache', array($procab_file_cache_plug, 'viewCache'));

add_action( 'save_post', array($procab_file_cache_plug, 'deleteAllFlatCache'), 13, 2 );


/**
 *
 * @return Procab_FileCache
 */
function get_procab_file_cache(){
global $procab_file_cache;
return $procab_file_cache;
   //return apply_filters('procab_file_cache', null);
}

 
/*
 * $url = admin_url('admin.php?page=customlinks');
wp_redirect($url);
*/