<?php
//http://www.cowburn.info/2010/04/30/glob-patterns/
class Procab_FileCache{
    protected $isCacheEnabled = true;
    protected $cacheContainers = [];
    
    //const DEFAULT_TTL = 432000;
    const DEFAULT_TTL = 518400;    //10mins
    
    public function __construct($isCacheEnabled){
        $this->isCacheEnabled = $isCacheEnabled;
    }
    
    //86400 seconds - 1 day
    //432000 => 5 day
    public function store($key, $data, $group='', $ttl = self::DEFAULT_TTL){
        if(!$this->isCacheEnabled) return false;
        //return false;
        $h = fopen($this->getFilePath($key, $group), 'a+');
        if(!$h) throw new Exception('Couldnot open cache file to write!');
        
        //exclusive lock, will get released when the file is closed
        flock($h, LOCK_EX);
        //set pointer to the begining of the file
        fseek($h, 0);
        //clear the file content
        ftruncate($h, 0);
        
        //time to live
        $ttl = (int) $ttl;
        $ttl = time() + $ttl;
        
        $data = serialize([$ttl, $data]);
        if(fwrite($h, $data) === false) throw new Exception('Coudnot write to cache!');
        
        fclose($h);
    }
    
    /*
    public function storeCss($key, $data){
        $h = fopen($this->getFilePath($key), 'a+');
        if(!$h) throw new Exception('Couldnot open cache file to write!');
        //exclusive lock, will get released when the file is closed
        flock($h, LOCK_EX);
        //set pointer to the begining of the file
        fseek($h, 0);
        //clear the file content
        ftruncate($h, 0);
        
        if(fwrite($h, $data) === false) throw new Exception('Coudnot write to cache!');
        fclose($h);
    }
    */
    
    
    
    public function exists($key, $group = ''){
        $filename = $this->getFilePath($key, $group);
        if(!file_exists($filename)) return false;
        return true;
    }

    
    public function restore($key, $group = '', $getDataOnly = true){
        if(!$this->isCacheEnabled) return false;
        $filename = $this->getFilePath($key, $group);
        if(!file_exists($filename)) return false;
        
        $h = fopen($filename, 'r');
        if(!$h) return false;
        
        //shared read lock
        flock($h, LOCK_SH);
        
        $fileContent = file_get_contents($filename);
        fclose($h);
        
        $data = @unserialize($fileContent);
        if(!$data){
            unlink($filename);
            return false;
        }
        
        if($getDataOnly){
            //check ttl
            if(time() > $data[0]){
                unlink($filename);
                return false;
            }
            
            return $data[1];
        }else{
            return $data;
        }
    }
    
    public function getFileName($key){
        $key = trim($key);
        
        if(strlen($key) < 2 || strlen($key) > 255){
            throw new Exception('Key must be between 2 - 255 characters');
        }
        
        $key = $this->sanitizeKey($key);// preg_replace('/[^a-zA-Z0-9\']/', '-', $key);
        return plugins_url('procab-file-cache').'/tmp/'.$key.'.txt';
    }
    
    public function getFilePath($key, $group = ''){///, $ext = 'txt'){
        $key = trim($key);
        
        if(strlen($key) < 2 || strlen($key) > 255){
            throw new Exception('Key must be between 2 - 255 characters');
        }
        
        $filePath = __DIR__.'/../tmp/';
        if($group != ''){
            $group = $this->sanitizeKey($group);
            $filePath .= $group.'_'; 
        }
        
        $key = $this->sanitizeKey($key);// preg_replace('/[^a-zA-Z0-9\']/', '-', $key);
        
        return $filePath.$key.'.txt';
        //return __DIR__.'/../tmp/'.$key.'.txt';
    }
    
    protected function sanitizeKey($key){
        return preg_replace('/[^a-zA-Z0-9\']/', '-', $key);
    }
    
    
    
    public function captureStart($key, $group = '', $ttl = self::DEFAULT_TTL){  //dayds of ttl
        $container = $this->setContainer($key, $ttl, $group);
        $container->captureStart();
    }
    
    public function captureEnd($key){
        $container = $this->getContainer($key);
        if($container == null) return '';
        $data = $container->captureEnd();
        $this->store($key, $data, $container->getGroup(), $container->getTTL());
        return $data;
    }
    
    /**
     * 
     * @param string $key
     * @return FileCacheContainer|NULL
     */
    protected function getContainer($key){
        if(array_key_exists($key, $this->cacheContainers)){
            return $this->cacheContainers[$key];
        }
        return null;
    }
    
    /**
     * 
     * @param string $key
     * @return FileCacheContainer
     */
    protected function setContainer($key, $ttl, $group=''){
        $container = new Procab_FileCacheContainer($ttl, $group);
        $this->cacheContainers[$key] = $container;
        return $container;
    }
    
    static public function getCacheFiles(){
        $files = glob(__DIR__.'/../tmp/*.txt');
        return $files;
    }
    
    public function deleteAllFlatCache(){
        //$files = glob(__DIR__.'/../tmp/*.txt');
        foreach (glob(__DIR__.'/../tmp/*.txt') as $file){
            if((strpos(basename($file), '_')) === false){
                @unlink($file);
                //var_dump($file);
            }
        }
        //array_map('unlink', glob(__DIR__.'/../tmp/*.txt'));
        //array_map('var_dump', glob(__DIR__.'/../tmp/*.*'));
    }
    
    public function deleteAll(){
        array_map('unlink', glob(__DIR__.'/../tmp/*.*'));
    }
    
    public function getCacheDir(){
        return __DIR__.'/../tmp/';
    }
    
    public function delete($key, $group = ''){
        $filename = $this->getFilePath($key, $group);
        if(!file_exists($filename)) return false;
        unlink($filename);
    }
    
    public function buildKey($str = ''){
        $key = $str.'-'.$_SERVER['REQUEST_URI'];
        return $this->sanitizeKey($key);//
    }
    
    
}