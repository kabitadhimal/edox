<?php add_thickbox(); ?>
<!-- active inactive update -->
<div class="wrap">
	<h2>Cache Files</h2>
	<div id="message" class="updated">
		<p>
			Plugin <strong>activated</strong>.
		</p>
	</div>


<form method="post" action="">
	<div class="alignleft actions bulkactions">
		<input type='hidden' name='action' />
		<input type="submit" name="delete-selected"  class="button delete-all" value="delete selected" />
		<input type="submit" name="delete-all"  class="button  button-cancel delete-all" value="delete all cache" />
	</div>

	<table class="wp-list-table widefat plugins">
		<thead>
			<tr>
				<th scope='col' id='cb' class='manage-column column-cb check-column'
					style=""><label class="screen-reader-text" for="cb-select-all-1">Select
						All</label><input id="cb-select-all-1" type="checkbox" /></th>
				<th scope='col' id='name' class='manage-column column-name' style="">Cache</th>
				<th scope='col' id='description'
					class='manage-columndelete column-description' style="">Description</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<th scope='col' class='manage-column column-cb check-column'
					style=""><label class="screen-reader-text" for="cb-select-all-2">Select
						All</label><input id="cb-select-all-2" type="checkbox" /></th>
				<th scope='col' class='manage-column column-name' style="">Cache</th>
				<th scope='col' class='manage-column column-description' style="">Description</th>
			</tr>
		</tfoot>
<tbody id="the-list">
		
			<?php
$groups = [];
$groups['flat'] = [];
foreach ($files as $file){
    $fileInfo = pathinfo($file);
    $index = strpos($fileInfo['filename'], '_'); 
    if($index !== false){
        $groupName = substr($fileInfo['filename'], 0, $index);
        $fileName = substr($fileInfo['filename'], $index+1 );
        
        if(!array_key_exists($groupName, $groups)) $groups[$groupName] = [];
        
        //$fileInfo['filename'] = $fileName;
        $fileInfo['key'] = $fileName;
        $groups[$groupName][] = $fileInfo; 
    }else{
        $fileInfo['key'] = $fileInfo['filename'];
        $groups['flat'][] = $fileInfo; 
    }
}


foreach ($groups as $groupName=>$group) :
    $fileGroup = '';
    if($groupName != 'flat') $fileGroup = $groupName;
    if(count($group) > 0):
    ?>
    <thead><tr><th></th><th scope="col" id="name" colspan='2' class="manage-column column-name" style=""><strong><?=$groupName?> (<?=count($group)?>)</strong></th></tr></thead>
    <?php
    foreach ($group as $fileInfo):
        $file = $fileInfo['dirname'].'/'.$fileInfo['basename'];
        
        ?>
        <tr id='a-courtiers-promotion-importer' class='active'>
				<th scope='row' class='check-column'><input type='checkbox'
					name='checked[]' value='<?php echo $fileInfo['filename']; ?>' /></th>
				<td class='plugin-title'><strong><?php echo $fileInfo['key']; ?></strong>
					<div class="row-actions visible">
						<span class='deactivate'>
							<a href="<?php echo site_url('wp-admin/admin-ajax.php') ?>?key=<?php echo $fileInfo['key']; ?>&group=<?=$fileGroup?>&action=procab_view_cache&amp;TB_iframe=true&amp;width=772&amp;height=336" 
						class="thickbox" aria-label="More information about Advanced Custom Fields" data-title="Advanced Custom Fields">View details</a>
						</span>
					</div></td>
				<td class='column-description desc'>
					<div class='plugin-description'>
						<p>created <?php echo human_time_diff(time(), filemtime($file)); ?> ago
						</p>
					</div>

				</td>
			</tr>
        <?php
    endforeach;
    endif;
?>
	
<?php
endforeach;
?>
	</tbody>	
	</table>
	</form>
